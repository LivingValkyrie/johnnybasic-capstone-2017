﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Author: Mike Dobson
/// Created: 9-23-16
/// 
/// Description: CompanionBaseBehavior is the base script that the companion AI will use for behaviors.
/// This script will be used for companions only to define specific behaviors.
/// </summary>

public class CompanionBaseBehaviors : AIBaseBehavior {

    #region Variables
    bool playerHasControl = false; //does the player currently have control of this character
    bool scriptedBehavior = false; //Is the character being acted upon by a scripted behavior
    float acceptanceDistance = .3f; //The Distance off acceptance to complete a jump behavior

    #region Object Variables
    Player playerComponent; //The Player component for this character
    PlayerControllerInput playerControllerComponent; //The PlayercontrollerInput component for this character
    GameObject companion; //The companion for this character, either player or non
    CharacterStats myCharacterStats; //The data that defines my characters behaviors and attributes
    float playerHitboxSizeX; //The x value for the players collision box size
    //bool isKO = false; //Is this companion KOed
    #endregion
    #region Distance Variables
    float sprintDistance = 3.5f; //The distance from the player that we start to sprint
    float runDistance = 2.5f;//The distance from the player that we start to run
    float walkDistance = 2f;//The distance from the player that we start to walk
    float distanceBetween; //The distance between the character and the player
    #endregion
    #endregion

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
        myCharacterStats = GetComponent<CharacterStats>();
        playerComponent = GetComponent<Player>();
        playerControllerComponent = GetComponent<PlayerControllerInput>();
        if (this.gameObject.tag == "Player")
        {
            companion = GameObject.FindGameObjectWithTag("Companion");
            playerHasControl = true;
        }
        else
        {
            companion = GameObject.FindGameObjectWithTag("Player");
            playerComponent.enabled = false;
            playerControllerComponent.enabled = false;
        }
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    //Fixed update is called at a set frame rate
    protected override void FixedUpdate()
    {
        if(!myCharacterStats.isKOd)
        {
            if(!playerHasControl)
            {
                base.FixedUpdate();
                FollowPlayer();
            }
        }
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if(!scriptedBehavior)
        {
            if(other.gameObject.tag == "BehaviorNode")
            {
                BehaviorNode script = other.GetComponent<BehaviorNode>();
                BehaviorType nodeType = script.thisType;
                if(nodeType == BehaviorType.JumpZone)
                {
                    //JumpZone(script);
                }
            }
        }
    }

    /// <summary>
    /// FollowPlayer is used to determine the direction that the companion needs to move and the speed that it
    /// needs to move at in order to follow the player
    /// </summary>
    void FollowPlayer()
    {
        //determine the distance between the player and the character
        distanceBetween = transform.position.x - companion.transform.position.x;
        //determine if the player is left or rigth of the character
        //character is right of the player
        if (transform.position.x - companion.transform.position.x > 0)
        {
            //set the follow direction for this frame to left
            moveLeft = true;
        }
        //character is left of the player
        else if (transform.position.x - companion.transform.position.x < 0)
        {
            //set the follow direction for this frame to right
            moveLeft = false;
        }

        //determine how close the character is to the player
        //if the distance is greater than sprint distance then sprint
        if (Mathf.Abs(distanceBetween) > sprintDistance)
        {
            //Debug.Log("I need to sprint");
            moveSpeed = sprintVelocity;
        }

        //if the distance is between run and sprint then run
        if (Mathf.Abs(distanceBetween) > runDistance &&
            Mathf.Abs(distanceBetween) < sprintDistance)
        {
            //Debug.Log("I need to run");
            moveSpeed = runVelocity;
        }

        //if the distance is between walk and run then run
        if (Mathf.Abs(distanceBetween) > walkDistance &&
            Mathf.Abs(distanceBetween) < runDistance)
        {
            //Debug.Log("I need to walk");
            moveSpeed = walkVelocity;
        }

        //if the distance is acceptable then stop
        if (Mathf.Abs(distanceBetween) < walkDistance)
        {
            //Debug.Log("I need to stop");
            moveSpeed = 0;
        }
    }

    /// <summary>
    /// CycleControllerToPlayer will be used to change the controller of the companion to the player
    /// </summary>
    public void CycleController()
    {
        if (playerHasControl)
        {
            //if the player currently has control then turn off the components for the player and tell the 
            //companion that the player does not have control any longer
            //playerComponent.enabled = true;
            playerControllerComponent.enabled = false;
            playerHasControl = false;
        }
        else
        {
            //if the player currently does not have control then turn on the components for the player
            //and tell the companion that the player has control
            //playerComponent.enabled = false;
            playerControllerComponent.enabled = true;
            playerHasControl = true;
        }       
    }

    ///// <summary>
    ///// This is used to create a jump behavior of a companion
    ///// </summary>
    ///// <param name="jumpInfo"></param>
    //void JumpZone(BehaviorNode jumpInfo)
    //{
    //    if(!playerHasControl)
    //    {
    //        if (jumpInfo.leftEntry == true)
    //        {
    //            if(transform.position.x - jumpInfo.gameObject.transform.position.x < 0)
    //            {
    //                StartCoroutine(JumpMovement(jumpInfo));
    //            }
    //        }
    //        else
    //        {
    //            if(transform.position.x - jumpInfo.gameObject.transform.position.x > 0)
    //            {
    //                StartCoroutine(JumpMovement(jumpInfo));
    //            }
    //        }
    //    }
    //}

    /// <summary>
    /// Helper script for making a jump happen for companions
    /// </summary>
    /// <param name="info"></param>
    ///// <returns></returns>
    //IEnumerator JumpMovement(BehaviorNode info)
    //{
    //    //Vector3 landingPadPos = info.jumpEnd.gameObject.transform.position;
    //    //Vector3 curvePos = info.curvePoint.gameObject.transform.position;
    //    //float jumpTime = info.jumpTime;

    //    moveSpeed = 0;
    //    scriptedBehavior = true;

    //    //Debug.Log("Starting Jump Coroutine");
    //    //yield return StartCoroutine(movementBezier(landingPadPos, curvePos, jumpTime));
    //    //Debug.Log("Completed Jump Coroutine");

    //    scriptedBehavior = false;
    //    moveSpeed = walkVelocity;
    //}

    /// <summary>
    /// Used to move along a bezier curve to create a faked jump action, used internally
    /// </summary>
    /// <param name="target"></param>
    /// <param name="curve"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    IEnumerator movementBezier(Vector3 target, Vector3 curve, float time)
    {
        //get the start of the curve
        Vector3 startCurve = transform.position;
        //elapsed time 
        float elapsedTime = 0f;

        //While time is lest than the end time
        //while (Time.time < endTime)
        //while(Vector3.Distance(transform.position, target) >= acceptanceDistance)
        while (Mathf.Abs(transform.position.x - target.x) >= acceptanceDistance)
        {
            //Debug.Log("Doing the curve loop");
            //track the current elapsed time after each frame
            elapsedTime += Time.deltaTime;
            float curTime = elapsedTime / time;

            //move along the curve
            transform.position = GetPoint(startCurve, target, curve, curTime);
            yield return null;
        }
    }

    //@reference Tiffany Fisher
    public Vector3 GetPoint(Vector3 start, Vector3 end, Vector3 curve, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        return oneMinusT * oneMinusT * start + 2f * oneMinusT * t * curve + t * t * end;
    }
}
