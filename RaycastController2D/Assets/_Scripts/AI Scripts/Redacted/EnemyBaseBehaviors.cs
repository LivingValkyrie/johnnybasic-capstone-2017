﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Author: Mike Dobson
/// Created: 10-11-16
/// 
/// Description: EnemyBaseBehaviors is the specific script that will be used to define specific behaviors that the enemies will
/// use to navigate and interact with the environment.
/// </summary>

[RequireComponent(typeof(Rigidbody2D))]
public class EnemyBaseBehaviors : AIBaseBehavior {

    #region Variables
    //Variables for changing direction
    //float lastPositionX; //The last recorded position of this enemy
    //int cycleCounter = 0; //The counter that stores how many cycles have been run with the same results
    //int maxCycles = 10; //The maximum amount of cycles that are acceptable to be stuck

    //Variables for jumping
    bool scriptedBehavior = false; //Is this character being acted upon by a scripted behavior
    float acceptanceDistance = .3f; //The Distance off acceptance to complete a jump behavior

    //Variables for Attacking
    bool isAttacking = false; //Is the character attacking the player
    public float frontDetectionDistance = 7; //The distance that the enemy can detect players in front of him
    public float rearDetectionDistance = 3; //The distance that the enemy can detect players behind him
    public float chaseDetectionDistance = 9; //The distance that the enemy will chase the player
    public LayerMask DetectionLayer; //The layer that we are detecting collisions on
    RaycastHit2D hitFront; //Detection of collisions in the front
    RaycastHit2D hitRear; //Detection of collisions in the rear
    GameObject chaseTarget; //The target that we are chasing to attack
    float distanceBetween; //The distance between the enemy and the target
    public float attackDistance = 4; //The distance that the enemy can hit a target
    public float chaseSpeed = 5; //The speed the enemy will chase the target
    Ability myAttack; //The attack ability that this enemy will use
    bool canAttack = true; //Can the enemy attack this frame
    float attackRecharge = 2; //The time it takes between the enemy attacking
    float attackDirection = 1; //1 is right -1 left
    #endregion

    //Use this for early initialization
    protected override void Awake()
    {
        base.Awake();
    }

	// Use this for initialization
	void Start () {
        myAttack = GetComponent<CharacterStats>().DataAsEnemy.basicAttack;
        myAttack.Init();
        
	}
	
	// Update is called once per frame
	protected override void Update () {
        base.Update();
	}

    //Fixed update is called at a set frame rate
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if(!isAttacking)
        {
            CastRays();
        }
        if(isAttacking)
        {
            Chase();
            CastRayAttack();
        }
    }

    /// <summary>
    /// OnTriggerEnter2D will activate when a new collision is detected on 2D colliders that are triggers
    /// </summary>
    /// <param name="other">The collider that we are colliding with</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        //if the character is not being scripted
        if(!scriptedBehavior)
        {
            //and is not attacking
            if(!isAttacking)
            {
                //check that the collision is on a behavior node
                if(other.gameObject.tag == "BehaviorNode")
                {
                    //Debug.Log("Activating a behavior");
                    //determine the type of behavior node
                    BehaviorNode script = other.GetComponent<BehaviorNode>();
                    BehaviorType nodeType = script.thisType;
                    //Switch
                    switch(nodeType)
                    {
                        case BehaviorType.JumpZone:
                            CompareJumpZone(script);
                            break;
                        case BehaviorType.StopContinue:
                            StartCoroutine(StopAndContinue(script.waitTime));
                            break;
                        case BehaviorType.StopTurn:
                            StartCoroutine(StopAndTurn(script.waitTime));
                            break;
                    }
                }
            }
            //if the character is attacking
            else
            {
                //check that the collision is on a behavior node
                if(other.gameObject.tag == "BehaviorNode")
                {
                    //ignore all behaviors except jumps
                    if (other.GetComponent<BehaviorNode>().thisType == BehaviorType.JumpZone)
                    {
                        CompareJumpZone(other.gameObject.GetComponent<BehaviorNode>());
                    }
                }
            }
        }

        //This is just a backup tool used only in development time to try to prevent enemies from getting stuck
        if (other.gameObject.tag == "Wall")
        {
            if (controller.collisions.left)
            {
                moveLeft = false;
            }
            else if (controller.collisions.right)
            {
                moveLeft = true;
            }
        }

    }

    /// <summary>
    /// StopAndContinue is a behavior node type that will allow the character to stop at the location,
    /// wait for a specific amount of time, and then continue on in the same direction as it entered
    /// the node.
    /// </summary>
    /// <param name="waitTime">The time that the character will wait at this node before continuing</param>
    /// <returns></returns>
    IEnumerator StopAndContinue(float waitTime)
    {
        //Debug.Log("Starting StopAndContinue");
        //Set the move speed to 0
        moveSpeed = 0;
        //Wait for a set amount of time
        yield return StartCoroutine(Wait(waitTime));
        //Return to walking speed
        moveSpeed = walkVelocity;
        //Debug.Log("Finished StopAndContinue");
    }

    IEnumerator StopAndTurn(float waitTime)
    {
        //Set the move speed to 0
        moveSpeed = 0;
        //Wait for a set amount of time
        yield return StartCoroutine(Wait(waitTime));
        //Return to walk speed and change direction
        moveLeft = !moveLeft;
        moveSpeed = walkVelocity;
    }

    //IEnumerator JumpMovement(BehaviorNode info)
    //{
    //    Vector3 landingPadPos = info.jumpEnd.gameObject.transform.position;
    //    Vector3 curvePos = info.curvePoint.gameObject.transform.position;
    //    float jumpTime = info.jumpTime;

    //    moveSpeed = 0;
    //    scriptedBehavior = true;

    //    //Debug.Log("Starting Jump Coroutine");
    //    yield return StartCoroutine(movementBezier(landingPadPos, curvePos, jumpTime));
    //    //Debug.Log("Completed Jump Coroutine");

    //    scriptedBehavior = false;
    //    moveSpeed = walkVelocity;
    //}

    /// <summary>
    /// This is a helper method that will only be used as a countdown timer.
    /// </summary>
    /// <param name="waitTime">The amount of time to yield the return</param>
    /// <returns></returns>
    IEnumerator Wait(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
    }

    /// <summary>
    /// This is used to determine the time between enemy attacks
    /// </summary>
    /// <param name="waitTime"></param>
    /// <returns></returns>
    IEnumerator AttackWait(float waitTime)
    {
        canAttack = false;
        yield return new WaitForSeconds(waitTime);
        canAttack = true;
    }

    /// <summary>
    /// CompareJumpZone is used to determine if the enemy meets the qualifications to complete the jump zone.
    /// If the enemy qualifies for the jump the jump will be run, if the enemy does not qualify for the jump 
    /// zone they will turn around.
    /// </summary>
    /// <param name="jumpZone">This is the JumpZone object that we are testing on to determine if we qualify for the jump</param>
    void CompareJumpZone(BehaviorNode jumpInfo)
    {
        //If this enemy meets the qualifier for this jump zone
        if(jumpInfo.nodeRequirement < jumpQualifier)
        {
            //If the jumpzone is registered as a left entry
            if(jumpInfo.leftEntry == true)
            {
                
                if(transform.position.x - jumpInfo.gameObject.transform.position.x < 0)
                {
                    //Do the jump coroutine
                    //StartCoroutine(JumpMovement(jumpInfo));
                }
            }
            //else if the jumpzone is registered as a right entry
            else
            {
                //And the enemy is entering from the right, creating a left collision on the enemy
                if(transform.position.x - jumpInfo.gameObject.transform.position.x > 0)
                {
                    //Do the jump coroutine
                    //StartCoroutine(JumpMovement(jumpInfo));
                }
            }
        }
        else
        { 
            moveLeft = !moveLeft;
        }
    }

    /// <summary>
    /// CheckPreviousLocation will check the current position against the previous position of this character
    /// If the character has remained stationary for a number of cycles then it will turn around to move in
    /// the opposite direction.
    /// </summary>
    //void CheckPreviousLocation()
    //{
    //    //if the position is the same as the last recorded position
    //    if(lastPositionX == transform.position.x)
    //    {
    //        //and we have maxed out our cycle counter
    //        if(cycleCounter >= maxCycles)
    //        {
    //            moveLeft = !moveLeft;
    //        }
    //        else
    //        {
    //            cycleCounter++;
    //        }
    //    }
    //    else
    //    {
    //        cycleCounter = 0;
    //        lastPositionX = transform.position.x;
    //    }
    //}

    /// <summary>
    /// CastRays is used to cast the rays that will determine if the enemy should be in the combat state
    /// </summary>
    void CastRays()
    {
        //Debug.Log("Casting Rays: " + this.gameObject.name);
        //Cast a ray in front of and behind the enemy based on which direction the enemy is moving
        switch(moveLeft)
        {
            case true:
                hitFront = Physics2D.Raycast(transform.position, Vector2.left, frontDetectionDistance, DetectionLayer.value);
                hitRear = Physics2D.Raycast(transform.position, Vector2.right, rearDetectionDistance, DetectionLayer.value);
                break;
            case false:
                hitFront = Physics2D.Raycast(transform.position, Vector2.right, frontDetectionDistance, DetectionLayer.value);
                hitRear = Physics2D.Raycast(transform.position, Vector2.left, rearDetectionDistance, DetectionLayer.value);
                break;
        }
        //if there is a collision then activate the combat state
        if(hitFront.collider != null)
        {
            //Debug.Log("Collision detected in front: " + this.gameObject.name + " " + hitFront.collider.gameObject.name);   
            isAttacking = true;
            chaseTarget = hitFront.collider.gameObject;
        }
        if(hitRear.collider != null)
        {
            //Debug.Log("Collision detected in rear " + this.gameObject.name + " " + hitRear.collider.gameObject.name);
            isAttacking = true;
            chaseTarget = hitRear.collider.gameObject;
        }
    }
    
    /// <summary>
    /// Cast a ray to determine if there is a target that I can attack infront of me
    /// </summary>
    void CastRayAttack()
    {
        //Check on a specific layer if I am detecting a player in front of me
        hitFront = Physics2D.Raycast(transform.position, Vector2.left, attackDistance, DetectionLayer.value);
        if(hitFront.collider != null)
        {
            Attack();
        }
    }

    /// <summary>
    /// This is used for chasing the player in order to attack them if the enemy is in the attack state
    /// </summary>
    void Chase()
    {
        //Find what direction I need to chase
        distanceBetween = transform.position.x - chaseTarget.transform.position.x;
        if (transform.position.x - chaseTarget.transform.position.x > 0)
        {
            attackDirection = -1;
            moveLeft = true;
        }
        else if(transform.position.x - chaseTarget.transform.position.x < 0)
        {
            attackDirection = 1;
            moveLeft = false;
        }
        //change my speed if I am further away from the player than what I am able to attack from
        if(Mathf.Abs(distanceBetween) > attackDistance)
        {
            moveSpeed = chaseSpeed;
        }
        if(Mathf.Abs(distanceBetween) < attackDistance)
        {
            moveSpeed = 0;
        }
    }

    /// <summary>
    /// Used to create the attack from the enemy
    /// </summary>
    void Attack()
    {
        //if i can attack this frame
        if(canAttack)
        {
            //cast the known ability
            myAttack.OnCast(attackDirection, transform.position);
            //then start my wait timer
            StartCoroutine(AttackWait(attackRecharge));
        }
            
    }

    /// <summary>
    /// Used to move along a bezier curve to create a faked jump action, used internally
    /// </summary>
    /// <param name="target"></param>
    /// <param name="curve"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    IEnumerator movementBezier(Vector3 target, Vector3 curve, float time)
    {
        //get the start of the curve
        Vector3 startCurve = transform.position;
        //elapsed time 
        float elapsedTime = 0f;

        //While time is lest than the end time
        //while (Time.time < endTime)
        //while(Vector3.Distance(transform.position, target) >= acceptanceDistance)
        while(Mathf.Abs(transform.position.x - target.x) >= acceptanceDistance)
        {
            //Debug.Log("Doing the curve loop");
            //track the current elapsed time after each frame
            elapsedTime += Time.deltaTime;
            float curTime = elapsedTime / time;

            //move along the curve
            transform.position = GetPoint(startCurve, target, curve, curTime);
            yield return null;
        }
    }

    //@reference Tiffany Fisher
    public Vector3 GetPoint(Vector3 start, Vector3 end, Vector3 curve, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        return oneMinusT * oneMinusT * start + 2f * oneMinusT * t * curve + t * t * end;
    }
}
