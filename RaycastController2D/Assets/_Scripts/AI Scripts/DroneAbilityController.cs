﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneAbilityController : MonoBehaviour {

    public GameObject player;
    public GameObject target;
    public Ability droneShoot;

    [HideInInspector] bool canAttack = true;
    public float attackCooldown = .75f;

    public float existanceTime = 30f;

    public float acceptanceDistance = 1f;

    [Tooltip("The speed at which the enemy will seek the players position")]
    public float speed = 10f; //The speed this enemy moves
    [Tooltip("The radius at which the enemy will stop seeking the player")]
    public float radiusOfSatisfaction = 1.5f; //My radius of satisfaction

    Steering steering;

    //attacking variables
    public List<GameObject> targetList = new List<GameObject>();
    public GameObject myTarget;
    public LayerMask targetDetectIgnoreMask;
    private RaycastHit2D targetDetection;

    // Use this for initialization
    void Start () {
        droneShoot.Init();
        steering = GetComponent<Steering>();
        player = GameController.instance.ActivePlayerGameObject;
        target = player.GetComponentInChildren<droneSeekTarget>().gameObject;
        StartCoroutine(DestroySelf(existanceTime));
        targetDetectIgnoreMask = ~targetDetectIgnoreMask;
	}
	
	// Update is called once per frame
	void Update () {
        FollowTarget();
		CheckTargets();
	}

    void FollowTarget()
    {
        transform.Translate((steering.getSteering(transform.position, target.transform.position, speed, radiusOfSatisfaction)) * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            AddEnemyToList(other.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            RemoveEnemyFromList(other.gameObject);
        }
    }

    public void AddEnemyToList(GameObject Enemy)
    {
        //Debug.Log("Parent Received Tracking information for " + Enemy.name);
        targetList.Add(Enemy);
    }

	public void RemoveEnemyFromList(GameObject Enemy) {
		if (Enemy != null) {


			//Debug.Log("Parent Removing Tracking information for " + Enemy.name);
			for (int i = 0; i < targetList.Count; i++) {
				if (targetList[i] == Enemy) {
					targetList.RemoveAt(i);
				}
			}
		}
	}

	public void CheckTargets()
    {
        //Debug.Log("Checking Targets");
        //Debug.Log("My Target is currently: " + myTarget.gameObject);
        if (targetList.Count > 0)
        {
            if (myTarget != null)
            {
                //Debug.Log("Target Detected");
                try
                {
                    targetDetection = Physics2D.Raycast(transform.position, myTarget.transform.position - transform.position,
                                                        GetComponent<CircleCollider2D>().radius, targetDetectIgnoreMask);
                    //Debug.Log("I hit: " + targetDetection.collider.gameObject + " while trying to reach " + myTarget.gameObject);
                    if (targetDetection.collider.gameObject == myTarget)
                    {
                        //Debug.Log("rawr");
                        //Debug.Log("Attacking Target");
                        if (canAttack)
                        {
                            //Debug.Log("meep");
                            ShootAtTarget(myTarget);
                        }
                    }
                    else
                    {
                        //Debug.Log("meep rawr");
						myTarget = null;
					}
                }
                catch
                {
                    //Debug.Log("pop");
                    RemoveEnemyFromList(myTarget);
                    myTarget = null;
                }
            }
            else
            {
				//Debug.Log("Finding New Target");
                //myTarget = null;

                #region matt added
                //converted to a for instead of a foreach. during a swap the list would be changed, which would cause .net errors for changing a collection during a foreach.
                for (int index = 0; index < targetList.Count; index++)
                {
                    GameObject enemy = targetList[index];
                    try
                    {
                        targetDetection = Physics2D.Raycast(transform.position,
                                                            enemy.transform.position - transform.position,
                                                            GetComponentInChildren<CircleCollider2D>().radius,
                                                            targetDetectIgnoreMask);
                        //Debug.Log("I hit: " + targetDetection.collider.gameObject + " while trying to reach " + targetList[index].gameObject);
                        if (targetDetection.collider.gameObject == enemy)
                        {
                            //Debug.Log("New target Aquired " + targetDetection.collider.gameObject);
                            //Debug.Break();
                            myTarget = (GameObject)targetDetection.collider.gameObject;
							//print("myTarget: " + myTarget);
							//Debug.Break();
                            break;
                        }
                    }
                    catch
                    {
                        RemoveEnemyFromList(enemy);
                    }
                }

                #endregion


                if (myTarget == null)
                {
                    //print("my target still null");
                }
            }
        }
        else
        {
           //print("target list empty"); 
        }
    }
    public void ShootAtTarget(GameObject Target)
    {
        //Debug.Log("fizz");
        //attackMain.OnCast(attackDirection, transform.position);
        droneShoot.OnCast(myTarget.transform.position - transform.position, transform.position, this.gameObject);
        //Debug.Log("buzz");
        AttackCooldown();
    }

    public void AttackCooldown()
    {
        StartCoroutine(AttackCooldownTimer(attackCooldown));
    }

    IEnumerator AttackCooldownTimer(float waitTime)
    {
        canAttack = false;
        yield return new WaitForSeconds(waitTime);
        canAttack = true;
    }

    void AttackWait(float waitTime)
    {
        StartCoroutine(AttackWaitCo(waitTime));
    }

    IEnumerator AttackWaitCo(float waitTime)
    {
        canAttack = false;
        yield return new WaitForSeconds(waitTime);
        canAttack = true;
    }

    IEnumerator DestroySelf(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(this.gameObject);
    }
}
