﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Author: Mike Dobson
/// Created: 10-09-16
/// 
/// Description: JumpZone is the basic script that all jump zones will use to create artificial jumping
/// behaviors for non-player characters.
/// </summary>

[RequireComponent(typeof(BoxCollider2D))]
public class JumpZone : MonoBehaviour {

    #region Variables
    public GameObject jumpEnd;
    public int jumpRequirement = 5;
    #endregion

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnDrawGizmos()
    {
        //get the box component from this object
        BoxCollider2D tempCollider = GetComponent<BoxCollider2D>();
        Gizmos.color = new Color(0,1,1,.25f);
        //Draw the gizmo that defines the location of the jump zone
        Gizmos.DrawCube(transform.position + new Vector3(tempCollider.offset.x, tempCollider.offset.y), new Vector3(tempCollider.size.x,tempCollider.size.y));
        Gizmos.color = new Color(1, 0, 0, .5f);
        Gizmos.DrawLine(transform.position + new Vector3(tempCollider.offset.x, tempCollider.offset.y),
            jumpEnd.transform.position + new Vector3(jumpEnd.GetComponent<BoxCollider2D>().offset.x, jumpEnd.GetComponent<BoxCollider2D>().offset.y));
    }
}
