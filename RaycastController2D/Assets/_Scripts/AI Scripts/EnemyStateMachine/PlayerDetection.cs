﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// @author: Mike Dobson
/// </summary>
public class PlayerDetection : MonoBehaviour {

    StateMachineEnemy myParentStateMachine;

	// Use this for initialization
	void Start () {
        myParentStateMachine = GetComponentInParent<StateMachineEnemy>();
	}
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "PlayerTwo")
        {
            //Debug.Log("Collision: " + other.gameObject.name);
            myParentStateMachine.AddTargetToList(other.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if(other.tag == "Player" || other.tag == "PlayerTwo")
        {
            myParentStateMachine.RemoveTargetFromList(other.gameObject);
        }
    }
}
