﻿using UnityEngine;
using System.Collections;
/// <summary>
/// @author Michael Dobson
/// </summary>
public class PatrolState : IEnemyState {

    private readonly StateMachineEnemy myStateMachine;
    private RaycastHit2D targetDetection;
    //Variables for detecting players
    //public float frontDetectionDistance = 7;
    //public float rearDetectionDistance = 4;
    //RaycastHit2D hitFront;
    //RaycastHit2D hitRear;

    public PatrolState (StateMachineEnemy stateMachineEnemy)
    {
        myStateMachine = stateMachineEnemy;
    }

    public void UpdateState()
    {
        if(myStateMachine.stationaryPatrol)
        {
            myStateMachine.moveSpeed = 0;
        }
        //CastDetectionRays();
        CheckTargets();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "BehaviorNode")
        {
            BehaviorNode script = other.gameObject.GetComponent<BehaviorNode>();
            switch(myStateMachine.myType)
            {
                case EnemyTypes.podCrawler:
                    if(script.thisType == BehaviorType.JumpZone)
                    {
                        //Debug.Log("Changing Direction");
                        myStateMachine.moveLeft = !myStateMachine.moveLeft;
                        //myStateMachine.moveSpeed = 0;                 
                    }                  
                    break;
                case EnemyTypes.swordSproutling:
                    if(script.thisType == BehaviorType.JumpZone)
                    {
                        myStateMachine.CompareJumpZone(script);
                    }
                    break;
                case EnemyTypes.spearSproutling:
                    break;
            }
        }
        //if(other.gameObject.tag == "BehaviorNode")
        //{
        //    if(!myStateMachine.scriptedBehavior)
        //    {
        //        BehaviorNode script = other.GetComponent<BehaviorNode>();
        //        switch(script.thisType)
        //        {
        //            case BehaviorType.JumpZone:
        //                CompareJumpZone(script);
        //                break;
        //            case BehaviorType.StopContinue:
        //                myStateMachine.StopAndContinue(script.waitTime);
        //                break;
        //            case BehaviorType.StopTurn:
        //                myStateMachine.StopAndTurn(script.waitTime);
        //                break;
        //        }
        //    }
        //}
    }

    #region SwitchStates
    public void ToPatrolState()
    {
        Debug.Log("Can not transfer from PatrolState to PatrolState");
    }

    public void ToAttackState()
    {
        //Debug.Log("Switching to attack state");
        myStateMachine.currentState = myStateMachine.attackState;
    }

    public void ToDeadState()
    {
        myStateMachine.currentState = myStateMachine.deadState;
    }

    public void ToStunState()
    {
        myStateMachine.lastState = myStateMachine.currentState;
        myStateMachine.StateKeepingSet();
        myStateMachine.currentState = myStateMachine.stunState;
    }
    #endregion

    public void CheckTargets()
    {
        foreach(GameObject target in myStateMachine.targetList)
        {
            //Debug.Log("Checking Target " + target.name);
            targetDetection = Physics2D.Raycast(myStateMachine.transform.position,
                                                target.transform.position - myStateMachine.transform.position,
                                                myStateMachine.GetComponentInChildren<CircleCollider2D>().radius,
                                                myStateMachine.targetDetectionIgnoreMask);
            if(targetDetection.collider.gameObject == target)
            {
                //Debug.Log("Collision Found");
                ToAttackState();
            }
        }
    }

    //void CastDetectionRays()
    //{
    //    switch(myStateMachine.moveLeft)
    //    {
    //        case true:
    //            hitFront = Physics2D.Raycast(myStateMachine.transform.position, Vector2.left, frontDetectionDistance, myStateMachine.ignoreMask);
    //            hitRear = Physics2D.Raycast(myStateMachine.transform.position, Vector2.right, rearDetectionDistance, myStateMachine.ignoreMask);
    //            break;
    //        case false:
    //            hitFront = Physics2D.Raycast(myStateMachine.transform.position, Vector2.right, frontDetectionDistance, myStateMachine.ignoreMask);
    //            hitRear = Physics2D.Raycast(myStateMachine.transform.position, Vector2.left, rearDetectionDistance, myStateMachine.ignoreMask);
    //            break;
    //    }
    //    if(hitFront.collider != null)
    //    {
    //        if (hitFront.collider.gameObject.tag == "Player")
    //        {
    //            //Debug.Log("Change to attack state");
    //            myStateMachine.chaseTarget = hitFront.collider.gameObject;
    //            myStateMachine.currentState = myStateMachine.attackState;
    //        }
    //    }
    //    if (hitRear.collider != null)
    //    {
    //        if (hitRear.collider.gameObject.tag == "Player")
    //        {
    //            //Debug.Log("Change to attack state");
    //            myStateMachine.chaseTarget = hitRear.collider.gameObject;
    //            myStateMachine.currentState = myStateMachine.attackState;
    //        }
    //    }
    //}


    /// <summary>
    /// CompareJumpZone is used to determine if the enemy meets the qualifications to complete the jump zone.
    /// If the enemy qualifies for the jump the jump will be run, if the enemy does not qualify for the jump 
    /// zone they will turn around.
    /// </summary>
    /// <param name="jumpZone">This is the JumpZone object that we are testing on to determine if we qualify for the jump</param>
    //void CompareJumpZone(BehaviorNode jumpInfo)
    //{
    //    //If this enemy meets the qualifier for this jump zone
    //    if (jumpInfo.nodeRequirement < myStateMachine.jumpQualifier)
    //    {
    //        //If the jumpzone is registered as a left entry
    //        if (jumpInfo.leftEntry == true)
    //        {

    //            if (myStateMachine.transform.position.x - jumpInfo.gameObject.transform.position.x < 0)
    //            {
    //                //Do the jump coroutine

    //                //myStateMachine.JumpMovement(jumpInfo);
    //            }
    //        }
    //        //else if the jumpzone is registered as a right entry
    //        else
    //        {
    //            //And the enemy is entering from the right, creating a left collision on the enemy
    //            if (myStateMachine.transform.position.x - jumpInfo.gameObject.transform.position.x > 0)
    //            {
    //                //Do the jump coroutine
    //                //myStateMachine.JumpMovement(jumpInfo);
    //            }
    //        }
    //    }
    //    else
    //    {
    //        myStateMachine.moveLeft = !myStateMachine.moveLeft;
    //    }
    //}
}
