﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunnedState : IEnemyState
{

    private readonly StateMachineEnemy myStateMachine;

    public StunnedState(StateMachineEnemy stateMachineEnemy)
    {
        myStateMachine = stateMachineEnemy;
    }

    public void UpdateState()
    {

    }

    public void OnTriggerEnter2D(Collider2D other)
    {

    }

    IEnumerator Stunned(float time)
    {
        yield return new WaitForSeconds(time);
        //Return to previous state here
        switch (myStateMachine.stateKeepingInt)
        {
            case 0: //Follow
                ToPatrolState();
                break;
            case 1: //Attack
                ToAttackState();
                break;
            case 2: //Dead
                Debug.LogWarning("Should not be able to return to dead state from stunned state");
                ToDeadState();
                break;

        }
    }

    public void ToPatrolState()
    {
        myStateMachine.currentState = myStateMachine.patrolState;
    }

    public void ToAttackState()
    {
        myStateMachine.currentState = myStateMachine.attackState;
    }

    public void ToDeadState()
    {
        myStateMachine.currentState = myStateMachine.deadState;
    }
    public void ToStunState()
    {
        Debug.Log("Can not trasnfer from stunState to stunState");
    }
}
