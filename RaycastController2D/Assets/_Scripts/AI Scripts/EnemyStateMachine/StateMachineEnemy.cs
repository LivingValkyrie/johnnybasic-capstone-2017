﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum EnemyTypes
{
    podCrawler,
    swordSproutling,
    spearSproutling
};

/// <summary>
/// @author Michael Dobson
/// </summary>
public class StateMachineEnemy : AIBaseBehavior {

    //states
    [HideInInspector] public IEnemyState currentState;
    [HideInInspector] public IEnemyState lastState;
    [HideInInspector] public PatrolState patrolState;
    [HideInInspector] public AttackState attackState;
    [HideInInspector] public DeadState deadState;
    [HideInInspector] public StunnedState stunState;
    [HideInInspector] public float stunTime;
    [HideInInspector] public int stateKeepingInt;
    
    //AI Keeping
    [HideInInspector] public bool scriptedBehavior; //boolean to turn off the behavior control
    [HideInInspector] public float acceptanceDistance = .3f; //acceptance distance for jump zones
    [HideInInspector] public EnemyTypes myType; //the general type of the enemy
    [HideInInspector] public CharacterStats myStats; //the place where my stats are stored
    [HideInInspector] public bool stationaryPatrol; //bool to determine a stationary patrol state
    [HideInInspector] public Animator myAnimator; //The animator i use to show movement

    //Attack values
    [HideInInspector] public List<GameObject> targetList = new List<GameObject>(); //list of avvailable targets
    [HideInInspector] public GameObject myTarget; //the target i'm currently attacking
    public float attackDistance; //the distance that i can attack from
    public float minAttackDistance; //the distance i want to be from the player when attacking (Spears)
    public bool canAttack = true; //Can attack tracker
    public float attackCooldown; //attack cooldown length
    [HideInInspector] public LayerMask targetDetectionIgnoreMask; 
    public Ability myAttack; //what my attack is
    [HideInInspector] public LayerMask ignoreMask;
    [HideInInspector] public GameObject chaseTarget; //the target i'm chasing
    [HideInInspector] public float chaseSpeed; //the speed i chase that target
    [HideInInspector] public Animator animationController; //The controller for this animator

    protected override void Awake()
    {
        patrolState = new PatrolState(this);
        attackState = new AttackState(this);
        deadState = new DeadState(this);
        stunState = new StunnedState(this);
        base.Awake();
        ignoreMask = ~ignoreMask;
        targetDetectionIgnoreMask = ~targetDetectionIgnoreMask;
    }

    void Start()
    {
        myStats = GetComponent<CharacterStats>();
        attackDistance = myStats.DataAsEnemy.attackDistance;
        attackCooldown = myStats.DataAsEnemy.attackCooldown;
        chaseSpeed = myStats.DataAsEnemy.chaseSpeed;
        myAttack = myStats.DataAsEnemy.basicAttack;
        myAnimator = myStats.DataAsEnemy.myAnimator;
        myType = myStats.DataAsEnemy.myType;
        stationaryPatrol = myStats.DataAsEnemy.stationaryPatrol;

        animationController = GetComponent<Animator>();
        animationController.runtimeAnimatorController = myStats.data.animatorController;

        currentState = patrolState;
        myAttack.Init(myStats.data);
    }

    protected override void Update()
    {
        base.Update();
        if(Mathf.Abs(moveSpeed) > 0)
        {
            SetRun();
        }
        else
        {
            SetIdle();
        }
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        currentState.UpdateState();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("Collision with: " + other.gameObject.name);
        currentState.OnTriggerEnter2D(other);
    }

    public void StopAndContinue(float waitTime)
    {
        StartCoroutine(StopAndContinueCo(waitTime));
    }

    /// <summary>
    /// StopAndContinue is a behavior node type that will allow the character to stop at the location,
    /// wait for a specific amount of time, and then continue on in the same direction as it entered
    /// the node.
    /// </summary>
    /// <param name="waitTime">The time that the character will wait at this node before continuing</param>
    /// <returns></returns>
    IEnumerator StopAndContinueCo(float waitTime)
    {
        //Set the move speed to 0
        moveSpeed = 0;
        //Wait for a set amount of time
        yield return StartCoroutine(Wait(waitTime));
        //Return to walking speed
        moveSpeed = walkVelocity;
        //Debug.Log("Finished StopAndContinue");
    }

    public void StopAndTurn(float waitTime)
    {
        StartCoroutine(StopAndTurnCo(waitTime));
    }

    IEnumerator StopAndTurnCo(float waitTime)
    {
        //Set the move speed to 0
        moveSpeed = 0;
        //Wait for a set amount of time
        yield return StartCoroutine(Wait(waitTime));
        //Return to walk speed and change direction
        moveLeft = !moveLeft;
        moveSpeed = walkVelocity;
    }

    //public void JumpMovement(BehaviorNode info)
    //{
    //    StartCoroutine(JumpMovementCo(info));
    //}

    ///// <summary>
    ///// JumpMovementCo is the coroutine that will define the jump behaviors for the enemies
    ///// </summary>
    ///// <param name="info">The infor from the BehaviorNode</param>
    ///// <returns></returns>
    //IEnumerator JumpMovementCo(BehaviorNode info)
    //{
    //    Vector3 landingPadPos = info.jumpEnd.gameObject.transform.position;
    //    Vector3 curvePos = info.curvePoint.gameObject.transform.position;
    //    float jumpTime = info.jumpTime;

    //    moveSpeed = 0;
    //    scriptedBehavior = true;

    //    //Debug.Log("Starting Jump Coroutine");
    //    yield return StartCoroutine(movementBezier(landingPadPos, curvePos, jumpTime));
    //    //Debug.Log("Completed Jump Coroutine");

    //    scriptedBehavior = false;
    //    moveSpeed = walkVelocity;
    //}

    /// <summary>
    /// This is a helper method that will only be used as a countdown timer.
    /// </summary>
    /// <param name="waitTime">The amount of time to yield the return</param>
    /// <returns></returns>
    IEnumerator Wait(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
    }

    public void AttackWait(float waitTime)
    {
        SetAttack();
        StartCoroutine(AttackWaitCo(waitTime));
    }

    public void AddTargetToList(GameObject Target)
    {
        targetList.Add(Target);
    }

    public void RemoveTargetFromList(GameObject Target)
    {
        for(int i = 0; i < targetList.Count; i++)
        {
            if(targetList[i] == Target)
            {
                targetList.RemoveAt(i);
            }
        }
    }
    /// <summary>
    /// This is used to determine the time between enemy attacks
    /// </summary>
    /// <param name="waitTime"></param>
    /// <returns></returns>
    IEnumerator AttackWaitCo(float waitTime)
    {
        canAttack = false;
        yield return new WaitForSeconds(waitTime);
        canAttack = true;
    }

    public void SetTarget(GameObject Target)
    {
        myTarget = Target;
    }

    public void CompareJumpZone(BehaviorNode jumpInfo)
    {
        //If this enemy meets the qualifier for this jump zone
        if (jumpInfo.nodeRequirement < jumpQualifier)
        {
            //If the jumpzone is registered as a left entry
            if (jumpInfo.leftEntry == true)
            {

                if (transform.position.x - jumpInfo.gameObject.transform.position.x < 0)
                {
                    //Do the jump coroutine

                    //myStateMachine.JumpMovement(jumpInfo);
                }
            }
            //else if the jumpzone is registered as a right entry
            else
            {
                //And the enemy is entering from the right, creating a left collision on the enemy
                if (transform.position.x - jumpInfo.gameObject.transform.position.x > 0)
                {
                    //Do the jump coroutine
                    //myStateMachine.JumpMovement(jumpInfo);
                }
            }
        }
        else
        {
            moveLeft = !moveLeft;
        }
    }

    /// <summary>
    /// Used to move along a bezier curve to create a faked jump action, used internally
    /// </summary>
    /// <param name="target"></param>
    /// <param name="curve"></param>
    /// <param name="time"></param>
    /// <returns></returns>
    IEnumerator movementBezier(Vector3 target, Vector3 curve, float time)
    { 
        //get the start of the curve
        Vector3 startCurve = transform.position;
        //elapsed time 
        float elapsedTime = 0f;

        //While time is lest than the end time
        //while (Time.time < endTime)
        //while(Vector3.Distance(transform.position, target) >= acceptanceDistance)
        while (Mathf.Abs(transform.position.x - target.x) >= acceptanceDistance)
        {
            //Debug.Log("Doing the curve loop");
            //track the current elapsed time after each frame
            elapsedTime += Time.deltaTime;
            float curTime = elapsedTime / time;

            //move along the curve
            transform.position = GetPoint(startCurve, target, curve, curTime);
            yield return null;
        }
    }

    //@reference Tiffany Fisher
    Vector3 GetPoint(Vector3 start, Vector3 end, Vector3 curve, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        return oneMinusT * oneMinusT * start + 2f * oneMinusT * t * curve + t * t * end;
    }

    void SetAttack()
    {
        animationController.ResetTrigger("idle");
        animationController.ResetTrigger("run");
        animationController.SetTrigger("attack");
    }

    void SetIdle()
    {
        animationController.ResetTrigger("run");
        animationController.ResetTrigger("attack");
        animationController.SetTrigger("idle");
    }

    void SetRun()
    {
        animationController.ResetTrigger("idle");
        animationController.ResetTrigger("attack");
        animationController.SetTrigger("run");
    }

    public void StunEnemy(float pStunTime)
    {
        stunTime = pStunTime;
        currentState.ToStunState();
    }

    public void StateKeepingSet()
    {
        if (lastState == patrolState)
        {
            stateKeepingInt = 0;
        }
        if (lastState == attackState)
        {
            stateKeepingInt = 1;
        }
        if (lastState == deadState)
        {
            stateKeepingInt = 2;
        }
    }
}
