﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class AttackState : IEnemyState {

    private readonly StateMachineEnemy myStateMachine;

    RaycastHit2D hitFront;
    RaycastHit2D hitRear;

    float attackMin;
    float attackMax;

    float distanceBetween;
    float attackDirection;

    RaycastHit2D targetDetection;

    //Constructor for a new attack state
    public AttackState (StateMachineEnemy stateMachineEnemy)
    {
        myStateMachine = stateMachineEnemy;
    }

    /// <summary>
    /// UpdateState will be used by the state machine to run this specific state
    /// </summary>
    public void UpdateState()
    {
        //Debug.Log("Attack Update");
        CheckTargets();
	    if (myStateMachine.myTarget) {
		    Chase();
	    }
        //CastRayAttack();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "BehaviorNode")
        {
            BehaviorNode script = other.gameObject.GetComponent<BehaviorNode>();
            switch (myStateMachine.myType)
            {
                case EnemyTypes.podCrawler:
                    if (script.thisType == BehaviorType.JumpZone)
                    {
                        //Debug.Log("Stop Moving");
                        myStateMachine.moveLeft = !myStateMachine.moveLeft;
                        myStateMachine.moveSpeed = 0;
                    }
                    break;
                case EnemyTypes.swordSproutling:
                    if (script.thisType == BehaviorType.JumpZone)
                    {
                        //myStateMachine.CompareJumpZone(script);
                    }
                    break;
                case EnemyTypes.spearSproutling:
                    break;
            }
        }

        //if (myStateMachine.myType == EnemyTypes.podCrawler)
        //{
        //    if(other.gameObject.tag == "BehaviorNode")
        //    {
        //        if(other.gameObject.GetComponent<BehaviorNode>().thisType == BehaviorType.JumpZone)
        //        {
        //            //myStateMachine.moveSpeed = 0;
        //            myStateMachine.moveLeft = !myStateMachine.moveLeft;
        //        }
        //    }
        //}
    }

    public void ToPatrolState()
    {
        //Debug.Log("Switching to patrol state");
        myStateMachine.currentState = myStateMachine.patrolState;
    }

    public void ToAttackState()
    {
        Debug.Log("Can not transfer from AttackState to AttackState");
    }

    public void ToDeadState()
    {
        myStateMachine.currentState = myStateMachine.deadState;
    }

    public void ToStunState()
    {
        myStateMachine.lastState = myStateMachine.currentState;
        myStateMachine.StateKeepingSet();
        myStateMachine.currentState = myStateMachine.stunState;
    }

    void CheckTargets()
    {
        //Debug.Log("Checking Targets");
        if(myStateMachine.targetList.Count > 0)
        {
            if(myStateMachine.myTarget != null)
            {
                if(myStateMachine.myTarget.tag != "Dead")
                {
                    targetDetection = Physics2D.Raycast(myStateMachine.transform.position, myStateMachine.myTarget.transform.position - myStateMachine.transform.position,
                                                        myStateMachine.GetComponentInChildren<CircleCollider2D>().radius, myStateMachine.targetDetectionIgnoreMask);

					#region Matt added
					//check to see if target exist before processing.
					if (targetDetection) {

		                if (targetDetection.collider.gameObject == myStateMachine.myTarget) {
			                if (myStateMachine.canAttack) {
				                //Debug.Log("Attacking");
				                ShootAtTarget(myStateMachine.myTarget);
			                }
		                } else {
			                myStateMachine.myTarget = null;
		                }
	                } else {
		                //Debug.Log("Target detection failed");
						//Debug.Break();
	                }

					#endregion
				} else
                {
                    myStateMachine.RemoveTargetFromList(myStateMachine.myTarget);
                    myStateMachine.myTarget = null;
                }
            }
            else
            {
                foreach(GameObject target in myStateMachine.targetList)
                {
                    targetDetection = Physics2D.Raycast(myStateMachine.transform.position, target.transform.position - myStateMachine.transform.position,
                                                        myStateMachine.GetComponentInChildren<CircleCollider2D>().radius, myStateMachine.targetDetectionIgnoreMask);
                    if(targetDetection.collider.gameObject == target)
                    {
                        myStateMachine.myTarget = targetDetection.collider.gameObject;
                        break;
                    }
                }
                if(myStateMachine.myTarget == null)
                {
                    ToPatrolState();
                }
            }
        }
        else
        {
            ToPatrolState();
        }
    }

    void ShootAtTarget(GameObject Target)
    {
        myStateMachine.myAttack.OnCast(attackDirection, myStateMachine.transform.position, myStateMachine.gameObject);
        myStateMachine.AttackWait(myStateMachine.attackCooldown);
    }

    ///// <summary>
    ///// Cast a ray to determine if there is a target that I can attack infront of me
    ///// </summary>
    //void CastRayAttack()
    //{
    //    //Check on a specific layer if I am detecting a player in front of me
    //    hitFront = Physics2D.Raycast(myStateMachine.transform.position, Vector2.left, myStateMachine.attackDistance, myStateMachine.ignoreMask);
    //    if (hitFront.collider != null)
    //    {
    //        Attack();
    //    }
    //}

    /// <summary>
    /// This is used for chasing the player in order to attack them if the enemy is in the attack state
    /// </summary>
    void Chase()
    {
        //Debug.Log("Chasing Target");
        //Find what direction I need to chase
        distanceBetween = myStateMachine.transform.position.x - myStateMachine.myTarget.transform.position.x;
        //determine if the player is left or right of the player
        if (distanceBetween > 0)
        {
            //attackdirection -1 is shooting to the left
            attackDirection = -1;
            //myStateMachine.moveLeft = true;
        }
        else if (distanceBetween < 0)
        {
            //attacck direction 1 is shooting to the right
            attackDirection = 1;
            //myStateMachine.moveLeft = false;
        }

        
        //change my speed if I am further away from the player than what I am able to attack from
        switch(myStateMachine.myType)
        {
            case EnemyTypes.podCrawler:
            case EnemyTypes.swordSproutling:
                if(distanceBetween > 0)
                {
                    myStateMachine.moveLeft = true;
                }
                else
                {
                    myStateMachine.moveLeft = false;
                }
                if (Mathf.Abs(distanceBetween) > myStateMachine.attackDistance)
                {
                    myStateMachine.moveSpeed = myStateMachine.chaseSpeed;
                }
                if (Mathf.Abs(distanceBetween) < myStateMachine.attackDistance)
                {
                    myStateMachine.moveSpeed = 0;
                }
                break;
            case EnemyTypes.spearSproutling:
                if(Mathf.Abs(distanceBetween) < myStateMachine.minAttackDistance)
                {
                    if(distanceBetween > 0)
                    {
                        myStateMachine.moveLeft = false;
                    }
                    else
                    {
                        myStateMachine.moveLeft = true;
                    }
                    //myStateMachine.moveLeft = !myStateMachine.moveLeft;
                    myStateMachine.moveSpeed = myStateMachine.chaseSpeed;
                }
                else if (Mathf.Abs(distanceBetween) > myStateMachine.attackDistance)
                {
                    if(distanceBetween > 0)
                    {
                        myStateMachine.moveLeft = true;
                    }
                    else
                    {
                        myStateMachine.moveLeft = false;
                    }
                    myStateMachine.moveSpeed = myStateMachine.chaseSpeed;
                }
                else if (Mathf.Abs(distanceBetween) < myStateMachine.attackDistance)
                {
                    myStateMachine.moveSpeed = 0;
                }
                break;
        }
        if (Mathf.Abs(distanceBetween) > myStateMachine.attackDistance)
        {
            myStateMachine.moveSpeed = myStateMachine.chaseSpeed;
        }
        if (Mathf.Abs(distanceBetween) < myStateMachine.attackDistance)
        {
            myStateMachine.moveSpeed = 0;
        }
    }

    ///// <summary>
    ///// Used to create the attack from the enemy
    ///// </summary>
    //void Attack()
    //{
    //    //if i can attack this frame
    //    if (myStateMachine.canAttack)
    //    {
    //        //cast the known ability
    //        myStateMachine.myAttack.OnCast(attackDirection, myStateMachine.transform.position);
    //        //then start my wait timer
    //        myStateMachine.AttackWait(myStateMachine.attackCooldown);
    //    }
    //}
}
