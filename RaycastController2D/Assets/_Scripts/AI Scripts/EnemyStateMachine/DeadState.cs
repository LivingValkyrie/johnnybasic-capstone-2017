﻿using UnityEngine;
using System.Collections;

public class DeadState : IEnemyState {

    private readonly StateMachineEnemy myStateMachine;

    public DeadState (StateMachineEnemy stateMachineEnemy)
    {
        myStateMachine = stateMachineEnemy;
    }

    public void UpdateState()
    {

    }

    public void OnTriggerEnter2D(Collider2D other)
    {

    }

    public void ToPatrolState()
    {
        myStateMachine.currentState = myStateMachine.patrolState;
    }

    public void ToAttackState()
    {
        myStateMachine.currentState = myStateMachine.attackState;
    }

    public void ToDeadState()
    {
        Debug.Log("Can not trasnfer from DeadState to DeadState");
    }

    public void ToStunState()
    {
        myStateMachine.lastState = myStateMachine.currentState;
        myStateMachine.StateKeepingSet();
        myStateMachine.currentState = myStateMachine.stunState;
    }
}
