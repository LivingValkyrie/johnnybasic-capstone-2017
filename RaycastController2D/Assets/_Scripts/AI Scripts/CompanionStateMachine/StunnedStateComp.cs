﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// @author Michael Dobson
/// </summary>

public class StunnedStateComp : ICompanionState
{

    private readonly CompStateMachine myStateMachine;

    public StunnedStateComp(CompStateMachine stateMachineComp)
    {
        myStateMachine = stateMachineComp;
    }

    public void UpdateState()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D other)
    {

    }

    IEnumerator Stunned(float time)
    {
        yield return new WaitForSeconds(time);
        //Return to previous state here
        switch(myStateMachine.stateKeepingInt)
        {
            case 0: //Follow
                ToFollowState();
                break;
            case 1: //Attack
                ToAttackState();
                break;
            case 2: //Dead
                Debug.LogWarning("Should not be able to return to dead state from stunned state");
                ToDeadState();
                break;

        }
    }

    public void ToFollowState()
    {
        myStateMachine.AliveTag();
        myStateMachine.currentStateComp = myStateMachine.followStateComp;
    }

    public void ToAttackState()
    {
        myStateMachine.AliveTag();
        myStateMachine.currentStateComp = myStateMachine.attackStateComp;
    }

    public void ToDeadState()
    {
        myStateMachine.DeadTag();
        myStateMachine.currentStateComp = myStateMachine.deadStateComp;
    }

    public void ToStunnedState()
    {
        Debug.LogError("Can not transition from stunned state to stunned state");
    }
}
