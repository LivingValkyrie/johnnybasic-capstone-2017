﻿using UnityEngine;
using System.Collections;

/// <summary>
/// @author Michael Dobson
/// </summary>

public class AttackStateComp : ICompanionState {

    private readonly CompStateMachine myStateMachine;

    private RaycastHit2D targetDetection;

    float distanceToEnemy;
    //float attackDirection;

    public AttackStateComp(CompStateMachine stateMachineComp)
    {
        myStateMachine = stateMachineComp;
    }

    public void UpdateState()
    {
        if (!myStateMachine.myStats.isKOd)
        {
            if (!myStateMachine.playerHasControl)
            {
                //,ChaseTarget(myStateMachine.myTarget);
                CheckTargets();
                Chase();
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {

    }

    public void CheckTargets()
    {
        //Debug.Log("Checking Targets");
        if(myStateMachine.enemyList.Count > 0)
        {
            if(myStateMachine.myTarget != null)
            {
                //Debug.Log("Target Detected");
                try
                {
                    targetDetection = Physics2D.Raycast(myStateMachine.transform.position, myStateMachine.myTarget.transform.position - myStateMachine.transform.position,
                                                        myStateMachine.GetComponentInChildren<CircleCollider2D>().radius, myStateMachine.targetDetectIgnoreMask);
                    if(targetDetection.collider.gameObject == myStateMachine.myTarget)
                    {
                        if(myStateMachine.canAbilityOne)
                        {
                            ShootAbilityAtTarget(myStateMachine.myTarget);
                        }
                        //Debug.Log("Attacking Target");
                        else if(myStateMachine.canAttack)
                        {
                            ShootAtTarget(myStateMachine.myTarget);
                        }
                    }
                    else
                    {
                        myStateMachine.myTarget = null;
                    }
                }
                catch
                {
                    myStateMachine.RemoveEnemyFromList(myStateMachine.myTarget);
                    myStateMachine.myTarget = null;
                }
            }
            else {
				//Debug.Log("Finding New Target");
				//myStateMachine.myTarget = null;

				#region matt added
				//converted to a for instead of a foreach. during a swap the list would be changed, which would cause .net errors for changing a collection during a foreach.
				for (int index = 0; index < myStateMachine.enemyList.Count; index++) {
		            GameObject enemy = myStateMachine.enemyList[index];
		            try {
			            targetDetection = Physics2D.Raycast(myStateMachine.transform.position,
			                                                enemy.transform.position - myStateMachine.transform.position,
			                                                myStateMachine.GetComponentInChildren<CircleCollider2D>().radius,
			                                                myStateMachine.targetDetectIgnoreMask);
			            if (targetDetection.collider.gameObject == enemy) {
				            //Debug.Log("New target Aquired");
				            myStateMachine.myTarget = targetDetection.collider.gameObject;
				            break;
			            }
		            } catch {
			            myStateMachine.RemoveEnemyFromList(enemy);
		            }
	            }

				#endregion


				if(myStateMachine.myTarget == null )
                {
                    //Debug.Log("No suitable targets, returning to follow state");
                    ToFollowState();
                }
            }
        }
        else
        {
            //Debug.Log("No Targets remaining, returning to follow state");
            ToFollowState();
        }
    }

    public void ShootAtTarget(GameObject Target)
    {
        //myStateMachine.attackMain.OnCast(attackDirection, myStateMachine.transform.position);
        myStateMachine.attackMain.OnCast(Target.transform.position - myStateMachine.transform.position, myStateMachine.transform.position, myStateMachine.gameObject);
        myStateMachine.AttackCooldown();
    }

    public void ShootAbilityAtTarget(GameObject Target)
    {
        myStateMachine.abilityOne.OnCast(Target.transform.position - myStateMachine.transform.position, myStateMachine.transform.position, myStateMachine.gameObject);
        myStateMachine.AbilityOneCooldown();
    }

    //public void ChaseTarget(GameObject Target)
    //{

    //}

    /// <summary>
    /// This is used for chasing the player in order to attack them if the enemy is in the attack state
    /// </summary>
    void Chase()
    {
        //Find what direction I need to chase
        try
        {
            distanceToEnemy = myStateMachine.transform.position.x - myStateMachine.myTarget.transform.position.x;
            //Debug.Log("me: " + myStateMachine.transform.position.x + " It: " + myStateMachine.myTarget.transform.position.x + " Combined: " + distanceToEnemy);
            //determine if the player is left or right of the player
            if (myStateMachine.transform.position.x - myStateMachine.myTarget.transform.position.x > 0)
            {
                //attackdirection -1 is shooting to the left
                //attackDirection = -1;
                myStateMachine.moveLeft = true;
            }
            else if (myStateMachine.transform.position.x - myStateMachine.myTarget.transform.position.x < 0)
            {
                //attacck direction 1 is shooting to the right
                //attackDirection = 1;
                myStateMachine.moveLeft = false;
            }
            //change my speed if I am further away from the player than what I am able to attack from
            if (Mathf.Abs(distanceToEnemy) > myStateMachine.attackDistance)
            {
                myStateMachine.moveSpeed = myStateMachine.runVelocity;
            }
            if(Mathf.Abs(distanceToEnemy) < myStateMachine.attackDistance)
            {
                myStateMachine.moveSpeed = 0;
            }
        }
        catch
        {
            
        }
    }

    public void ToFollowState()
    {
        myStateMachine.currentStateComp = myStateMachine.followStateComp;
    }

    public void ToAttackState()
    {
        Debug.LogWarning("cannot convert from attack state to attack state");
    }

    public void ToDeadState()
    {
        myStateMachine.DeadTag();
        myStateMachine.currentStateComp = myStateMachine.deadStateComp;
    }

    public void ToStunnedState()
    {
        myStateMachine.lastStateComp = myStateMachine.currentStateComp;
        myStateMachine.StateKeepingSet();
        myStateMachine.currentStateComp = myStateMachine.stunnedStateComp;
    }
}
