﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// @author Michael Dobson
/// </summary>

public class CompStateMachine : AIBaseBehavior {

    //States
    [HideInInspector] public ICompanionState currentStateComp;
    [HideInInspector] public ICompanionState lastStateComp;
    [HideInInspector] public FollowStateComp followStateComp;
    [HideInInspector] public AttackStateComp attackStateComp;
    [HideInInspector] public DeadStateComp deadStateComp;
    [HideInInspector] public StunnedStateComp stunnedStateComp;
    [HideInInspector] public int stateKeepingInt;
    [HideInInspector] public float stunTime;

    //Companion Keeping
    [HideInInspector] public bool scriptedBehavior = false; //Is the character being acted upon by a scripted behavior
    [HideInInspector] public CharacterStats myStats; //the values for my character
    [HideInInspector] protected string myTag; //the tag that i am currently using
    [HideInInspector] float distanceBetween; //the distance between me and the other PC
    [HideInInspector] GameObject companion; //The other PC
    [HideInInspector] public bool playerHasControl = false; //Do i have control or does the player
    [HideInInspector] Player playerComponent; //The player component on me
    [HideInInspector] PlayerControllerInput playerControllerComponent; //The player controler component on me
    [HideInInspector] Animator myAnimator; //The animator on me

    //Movement
    public bool catchup = false; //a bool to govern the ability for the companion to catch upto the player
    [HideInInspector] float sprintDistance; //The distance from the PC that i should start sprinting
    [HideInInspector] float runDistance; //The distance from the PC that i should statrt running
    [HideInInspector] float walkDistance; //The distance from the PC that i should start walking
    //[HideInInspector] float acceptanceDistance = .25f; //The Distance off acceptance to complete a jump behavior

    //CharacterStats myCharacterStats;
    public LayerMask targetDetectIgnoreMask;

    //Attack values
    [HideInInspector] public GameObject myTarget; //My current target
    [HideInInspector] public float engagementRadius; //The radius away from me that i recognize an enemy is in the area
    [HideInInspector] public float attackDistance; //The distance away that i can attack
    [HideInInspector] public bool canAttack = true; //the cooldown check for attacking
    public float attackCooldown; //the cooldown timer
    public bool canAbilityOne = true;
    public float abilityOneCooldown;
    public Ability attackMain; //Main attack
    public Ability attackSecondary; //Secondary Attack
    public Ability abilityOne; //Ability one

    //My colldiers
    //public CircleCollider2D myCircleCollider;
    //public BoxCollider2D myBoxCollider;

    [HideInInspector] public bool isAttacking = false; //Is the character currently in the attack state

    [HideInInspector] public List<GameObject> enemyList = new List<GameObject>();

    protected override void Awake()
    {
        followStateComp = new FollowStateComp(this);
        attackStateComp = new AttackStateComp(this);
        deadStateComp = new DeadStateComp(this);
        stunnedStateComp = new StunnedStateComp(this);
        targetDetectIgnoreMask = ~targetDetectIgnoreMask;
        base.Awake();
        //myCharacterStats = GetComponent<CharacterStats>();
        playerComponent = GetComponent<Player>();
        playerControllerComponent = GetComponent<PlayerControllerInput>();
        if (this.gameObject.tag == "Player")
        {
            companion = GameObject.FindGameObjectWithTag("PlayerTwo");
            playerHasControl = true;
        }
        else
        {
            companion = GameObject.FindGameObjectWithTag("Player");
            playerComponent.enabled = false;
            playerControllerComponent.enabled = false;
        }

		//following 4 lines commented out by matt to move to start
		//myStats = GetComponent<CharacterStats>();
		//sprintDistance = myStats.DataAsPlayer.sprintDistance;
		//runDistance = myStats.DataAsPlayer.runDistance;
		//walkDistance = myStats.DataAsPlayer.walkDistance;

        //myCircleCollider = GetComponent<CircleCollider2D>();
        //myBoxCollider = GetComponent<BoxCollider2D>();
        attackDistance = 8;
        attackCooldown = 1f;
        canAttack = true;
        //Debug.Log(sprintDistance + " " + runDistance + " " + walkDistance);
    }

    // Use this for initialization
    void Start () {
		#region  moved here from awake by matt
		myStats = GetComponent<CharacterStats>();
		sprintDistance = myStats.DataAsPlayer.sprintDistance;
		runDistance = myStats.DataAsPlayer.runDistance;
		walkDistance = myStats.DataAsPlayer.walkDistance;
        #endregion
        engagementRadius = myStats.DataAsPlayer.engagementRadius;
        attackMain = myStats.DataAsPlayer.attackMain;
        attackSecondary = myStats.DataAsPlayer.attackSecondary;
        abilityOne = myStats.DataAsPlayer.abilityOne;
        attackCooldown = myStats.DataAsPlayer.attackMain.Cooldown;
        abilityOneCooldown = myStats.DataAsPlayer.abilityOne.Cooldown;
        //TODO add the attack cooldowns and bools

        myAnimator = GetComponent<Animator>();
        myAnimator.runtimeAnimatorController = myStats.data.animatorController;

		currentStateComp = followStateComp;
        //attackMain = myStats.DataAsPlayer.abilityOne;
        myTag = gameObject.tag;
	}

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if(!playerHasControl)
        {
            if(scriptedBehavior)
            {
                moveSpeed = 0;
            }
        }
    }

    protected override void FixedUpdate()
    {
        if(!myStats.isKOd)
        {
            if(!playerHasControl)
            {
                base.FixedUpdate();
	            if (!scriptedBehavior) {
		            //Debug.Log("not scripted");
		            myAnimator.SetFloat("speed", moveSpeed);
					myAnimator.ResetTrigger("idle");
	            } else {
		            myAnimator.SetFloat("speed", 0f);
	            }

                //if (Mathf.Abs(moveSpeed) == 0)
                //{
                //    myAnimator.SetTrigger("idle");
                //}
                //else
                //{
                //    myAnimator.ResetTrigger("idle");
                //}
                currentStateComp.UpdateState();
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        currentStateComp.OnTriggerEnter2D(other);
    }

    public void AddEnemyToList(GameObject Enemy)
    {
        //Debug.Log("Parent Received Tracking information for " + Enemy.name);
        enemyList.Add(Enemy);
    }

    public void RemoveEnemyFromList(GameObject Enemy)
    {
        //Debug.Log("Parent Removing Tracking information for " + Enemy.name);
        for(int i = 0; i < enemyList.Count; i++)
        {
            if(enemyList[i] == Enemy)
            {
                enemyList.RemoveAt(i);
            }
        }
    }

    public void DeadTag()
    {
        gameObject.tag = "Dead";
    }

    public void AliveTag()
    {
        gameObject.tag = myTag;
    }

    /// <summary>
    /// FollowPlayer is used to determine the direction that the companion needs to move and the speed that it
    /// needs to move at in order to follow the player
    /// </summary>
    public void FollowPlayer()
    {
        //determine the distance between the player and the character
		//print(companion.name);
        distanceBetween = transform.position.x - companion.transform.position.x;
        //determine if the player is left or rigth of the character
        //character is right of the player
        if (transform.position.x - companion.transform.position.x > 0)
        {
            //set the follow direction for this frame to left
            moveLeft = true;
        }
        //character is left of the player
        else if (transform.position.x - companion.transform.position.x < 0)
        {
            //set the follow direction for this frame to right
            moveLeft = false;
        }

        if(!catchup)
        {
            //determine how close the character is to the player
            //if the distance is greater than sprint distance then sprint
            if (Mathf.Abs(distanceBetween) > sprintDistance)
            {
                //Debug.Log("I need to sprint");
                moveSpeed = sprintVelocity;
                catchup = true;
            }

            //if the distance is between run and sprint then run
            if (Mathf.Abs(distanceBetween) > runDistance &&
                Mathf.Abs(distanceBetween) < sprintDistance)
            {
                //Debug.Log("I need to run");
                moveSpeed = runVelocity;
            }

            //if the distance is between walk and run then run
            if (Mathf.Abs(distanceBetween) > walkDistance &&
                Mathf.Abs(distanceBetween) < runDistance)
            {
                //Debug.Log("I need to walk");
                moveSpeed = walkVelocity;
            }

            //if the distance is acceptable then stop
            if (Mathf.Abs(distanceBetween) < walkDistance)
            {
                //Debug.Log("I need to stop");
                moveSpeed = 0;
            }
        }
        else
        {
            moveSpeed = sprintVelocity + 3;
            if(Mathf.Abs(distanceBetween) < walkDistance)
            {
                moveSpeed = walkVelocity;
                catchup = false;
            }
        }
    }

    //public void DetectedEnemy(GameObject detected)
    //{
    //    //Debug.LogWarning("Enemy Detected: " + detected.name);
    //}

    public void AttackCooldown()
    {
        myAnimator.SetTrigger("attack");
        StartCoroutine(AttackCooldownTimer(attackCooldown));
    }

    IEnumerator AttackCooldownTimer(float waitTime)
    {
        canAttack = false;
        yield return new WaitForSeconds(waitTime);
        myAnimator.ResetTrigger("attack");
        canAttack = true;
    }

    public void AbilityOneCooldown()
    {
        myAnimator.SetTrigger("attack");
        StartCoroutine(AbilityOneCooldownTimer(abilityOneCooldown));
    }

    IEnumerator AbilityOneCooldownTimer(float waitTime)
    {
        canAbilityOne = false;
        yield return new WaitForSeconds(waitTime);
        myAnimator.ResetTrigger("attack");
        canAbilityOne = true;
    }

    /// <summary>
    /// CycleControllerToPlayer will be used to change the controller of the companion to the player
    /// </summary>
    public void CycleController()
    {
        if (playerHasControl)
        {
            //if the player currently has control then turn off the components for the player and tell the 
            //companion that the player does not have control any longer
            //playerComponent.enabled = true;
            playerControllerComponent.enabled = false;
            playerHasControl = false;
        }
        else
        {
            //if the player currently does not have control then turn on the components for the player
            //and tell the companion that the player has control
            //playerComponent.enabled = false;
            playerControllerComponent.enabled = true;
            playerHasControl = true;
        }
    }

    /// <summary>
    /// This is used to create a jump behavior of a companion
    /// </summary>
    /// <param name="jumpInfo"></param>
    public void JumpZone(BehaviorNode jumpInfo)
    {
        if (!playerHasControl)
        {
            if (jumpInfo.leftEntry == true)
            {
                if (transform.position.x - jumpInfo.gameObject.transform.position.x < 0)
                {
                    int jumpIndex = JumpClosestChecker(jumpInfo);
                    myAnimator.SetTrigger("jumping");
                    StartCoroutine(JumpMovement(jumpInfo.LandingPads[jumpIndex], jumpInfo.CurvePoints[jumpIndex], jumpInfo.JumpTimes[jumpIndex]));
                    myAnimator.ResetTrigger("hasLanded");
                }
            }
            else
            {
                if (transform.position.x - jumpInfo.gameObject.transform.position.x > 0)
                {
                    int jumpIndex = JumpClosestChecker(jumpInfo);
                    StartCoroutine(JumpMovement(jumpInfo.LandingPads[jumpIndex], jumpInfo.CurvePoints[jumpIndex], jumpInfo.JumpTimes[jumpIndex]));
                }
            }
        }
    }

    public int JumpClosestChecker(BehaviorNode jumpInfo)
    {
        List<GameObject> landingPads = jumpInfo.LandingPads;

        int returnValue = 0;

        for(int j = 0; j < landingPads.Count; j++ )
        {
            //if the pad we are checking is closer to the player than the pad that is currently stored in the target pad
            if(Vector3.Distance(landingPads[j].transform.position, companion.transform.position) < 
                Vector3.Distance(landingPads[returnValue].transform.position, companion.transform.position))
            {
                returnValue = j;
            } 
        }

        return returnValue;
    }

    /// <summary>
    /// Helper script for making a jump happen for companions
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    IEnumerator JumpMovement(GameObject LandingPad, GameObject CurvePoint, float JumpTime)
    {
        Vector3 landingPadPos = LandingPad.transform.position;
        Vector3 curvePos = CurvePoint.transform.position;
        float jumpTime = JumpTime;

        moveSpeed = 0;
        scriptedBehavior = true;
        myAnimator.SetTrigger("jumping");
        //myAnimator.speed = moveSpeed;

        //Debug.Log("Starting Jump Coroutine");
        yield return StartCoroutine(movementBezier(landingPadPos, curvePos, jumpTime));
        //Debug.Log("Completed Jump Coroutine");
        //myAnimator.SetFloat("verticalSpeed", 0);
        //myAnimator.ResetTrigger("hasLanded");
        myAnimator.ResetTrigger("jumping");
        myAnimator.SetTrigger("hasLanded");
        myAnimator.SetFloat("verticalSpeed", 0);
        //myAnimator.SetFloat("speed", walkVelocity);
        scriptedBehavior = false;
        //myAnimator.ResetTrigger("hasLanded");
        moveSpeed = walkVelocity;
    }

    ///// <summary>
    ///// Used to move along a bezier curve to create a faked jump action, used internally
    ///// </summary>
    ///// <param name="target"></param>
    ///// <param name="curve"></param>
    ///// <param name="time"></param>
    ///// <returns></returns>
    //IEnumerator movementBezier(Vector3 target, Vector3 curve, float time)
    //{
    //    //get the start of the curve
    //    Vector3 startCurve = transform.position;
    //    //elapsed time 
    //    float elapsedTime = 0f;

    //    //While time is lest than the end time
    //    //while (Time.time < endTime)
    //    //while(Vector3.Distance(transform.position, target) >= acceptanceDistance)
    //    while (Mathf.Abs(transform.position.x - target.x) >= acceptanceDistance)
    //    {
    //        //Debug.Log("Doing the curve loop");
    //        //track the current elapsed time after each frame
    //        elapsedTime += Time.deltaTime;
    //        float curTime = elapsedTime / time;
    //        if(elapsedTime < time / 2)
    //        {
    //            myAnimator.SetFloat("verticalSpeed", 1);
    //        }
    //        else
    //        {
    //            myAnimator.SetFloat("verticalSpeed", -1);
    //        }
    //        //move along the curve
    //        transform.position = GetPoint(startCurve, target, curve, curTime);
    //        yield return null;
    //    }

    //    //myAnimator.ResetTrigger("hasLanded");
    //}

    ////@reference Tiffany Fisher
    //public Vector3 GetPoint(Vector3 start, Vector3 end, Vector3 curve, float t)
    //{
    //    t = Mathf.Clamp01(t);
    //    float oneMinusT = 1f - t;
    //    return oneMinusT * oneMinusT * start + 2f * oneMinusT * t * curve + t * t * end;
    //}

    //@reference Tiffany Fisher
    IEnumerator movementBezier(Vector3 target, Vector3 curve, float time)
    {
        //Get the current time
        float startTime = Time.time;
        //find the time for the end of the movement
        float endTime = startTime + time;

        //get the start of the curve
        Vector3 startCurve = transform.position;
        //elapsed time 
        float elapsedTime = 0f;


		//While time is lest than the end time
		while ( Time.time < endTime)
        {

            //track the current elapsed time after each frame
            elapsedTime += Time.deltaTime;
            float curTime = elapsedTime / time;
		
			if ( elapsedTime < time / 2 ) {
				myAnimator.SetFloat( "verticalSpeed", 1 );
			} else {
				myAnimator.SetFloat( "verticalSpeed", -1 );
			}

            //move along the curve
            transform.position = GetPoint(startCurve, target, curve, curTime);
            yield return null;
        }

        //Snap the player to the target position at the end of the curve
        transform.position = target;

    }

    public Vector3 GetPoint(Vector3 start, Vector3 end, Vector3 curve, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        return oneMinusT * oneMinusT * start + 2f * oneMinusT * t * curve + t * t * end;
    }

    public void StateKeepingSet()
    {
        if(lastStateComp == followStateComp)
        {
            stateKeepingInt = 0;
        }
        if(lastStateComp == attackStateComp)
        {
            stateKeepingInt = 1;
        }
        if(lastStateComp == deadStateComp)
        {
            stateKeepingInt = 2;
        }
    }

    public void StunCompanion(float pStunTime)
    {
        stunTime = pStunTime;
        currentStateComp.ToStunnedState();
    }
}
