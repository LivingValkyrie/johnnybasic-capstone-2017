﻿using System;
using UnityEngine;
using System.Collections;

/// <summary>
/// @author Michael Dobson
/// 
/// </summary>

public class FollowStateComp : ICompanionState {

    private readonly CompStateMachine myStateMachine;
    private RaycastHit2D targetDetection;

    public FollowStateComp (CompStateMachine stateMachineComp)
    {
        myStateMachine = stateMachineComp;
    }

    public void UpdateState()
    {
        if(!myStateMachine.myStats.isKOd)
        {
            if(!myStateMachine.playerHasControl)
            {
                myStateMachine.FollowPlayer();
                CheckTargets();
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (!myStateMachine.scriptedBehavior)
        {
            if (other.gameObject.tag == "BehaviorNode")
            {
                BehaviorNode script = other.gameObject.GetComponent<BehaviorNode>();
                BehaviorType nodeType = script.thisType;
                if (nodeType == BehaviorType.JumpZone)
                {
                    myStateMachine.JumpZone(script);
                }
            }
        }
    }

    public void CheckTargets()
    {
        foreach(GameObject enemy in myStateMachine.enemyList)
        {
			#region matt added
			//added an enemy check as there was a error where if you swapped control during combat this could error, or if the enemy died during a frame
			if (enemy) {
				  targetDetection = Physics2D.Raycast(myStateMachine.transform.position,
				                                      enemy.transform.position - myStateMachine.transform.position,
				                                      myStateMachine.GetComponentInChildren<CircleCollider2D>().radius,
				                                      myStateMachine.targetDetectIgnoreMask);

				  //Debug.Log("Raycasthit collided with: " + targetDetection.collider.gameObject);
				  if(targetDetection.collider.gameObject == enemy)
				  {
				      //Debug.Log("Enemy is in line of sight");
				      ToAttackState();
				  }
	        }

			#endregion
		}

	    for (int i = 0; i < myStateMachine.enemyList.Count; i ++) 
		{
		    try {
			    targetDetection = Physics2D.Raycast(myStateMachine.transform.position,
			                                        myStateMachine.enemyList[i].transform.position - myStateMachine.transform.position,
			                                        myStateMachine.GetComponentInChildren<CircleCollider2D>().radius,
			                                        myStateMachine.targetDetectIgnoreMask);
			    if (targetDetection.collider.gameObject == myStateMachine.enemyList[i]) {
				    //Debug.Log("Enemy is in line of sight");
				    ToAttackState();
			    }
		    } catch {
			    myStateMachine.RemoveEnemyFromList(myStateMachine.enemyList[i]);
		    }
	    }
    }

    public void ToFollowState()
    {
        Debug.LogWarning("Can not transfer from Follow State to Follow State");
    }

    public void ToAttackState()
    {
        myStateMachine.currentStateComp = myStateMachine.attackStateComp;
    }

    public void ToDeadState()
    {
        myStateMachine.DeadTag();
        myStateMachine.currentStateComp = myStateMachine.deadStateComp;
    }

    public void ToStunnedState()
    {
        myStateMachine.lastStateComp = myStateMachine.currentStateComp;
        myStateMachine.StateKeepingSet();
        myStateMachine.currentStateComp = myStateMachine.stunnedStateComp;
    }
}
