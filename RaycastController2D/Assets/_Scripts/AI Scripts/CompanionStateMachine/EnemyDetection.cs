﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// @author Michael Dobson
/// </summary>

public class EnemyDetection : MonoBehaviour {

    //The state machine that the companion is running on
    CompStateMachine myParentStateMachine;

	// Use this for initialization
	void Start () {
        myParentStateMachine = GetComponentInParent<CompStateMachine>();
	}
    
    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("Enemy entering Radius");
        //Debug.Log("Adding " + other.name + " to tracking");
        if(other.tag == "Enemy")
        {
            //Debug.Log("Telling Parent to add to tracking");
            myParentStateMachine.AddEnemyToList(other.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        //Debug.Log("Enemy Leaving Radius.");
        //Debug.Log("Removing " + other.name + " from tracking");
        if(other.tag =="Enemy")
        {
            //Debug.Log("Telling Parent to remove from tracking");
            myParentStateMachine.RemoveEnemyFromList(other.gameObject);
        }
    }
}
