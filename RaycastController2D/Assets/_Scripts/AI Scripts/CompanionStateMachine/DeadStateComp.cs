﻿using UnityEngine;
using System.Collections;

/// <summary>
/// @author Michael Dobson
/// </summary>

public class DeadStateComp : ICompanionState {

    private readonly CompStateMachine myStateMachine;

    public DeadStateComp(CompStateMachine stateMachineComp)
    {
        myStateMachine = stateMachineComp;
    }

    public void UpdateState()
    {

    }

    public void OnTriggerEnter2D(Collider2D other)
    {

    }

    public void ToFollowState()
    {
        myStateMachine.AliveTag();
        myStateMachine.currentStateComp = myStateMachine.followStateComp;
    }

    public void ToAttackState()
    {
        myStateMachine.AliveTag();
        myStateMachine.currentStateComp = myStateMachine.attackStateComp;
    }

    public void ToDeadState()
    {
        Debug.LogWarning("Cannot transfer from dead state to dead state");
    }

    public void ToStunnedState()
    {
        myStateMachine.lastStateComp = myStateMachine.currentStateComp;
        myStateMachine.StateKeepingSet();
        myStateMachine.currentStateComp = myStateMachine.stunnedStateComp;
    }
}
