﻿using UnityEngine;
using System.Collections;

/// <summary>
/// @author Michael Dobson
/// </summary>

public interface ICompanionState {

    void UpdateState();

    void OnTriggerEnter2D(Collider2D other);

    void ToFollowState();

    void ToAttackState();

    void ToDeadState();

    void ToStunnedState();

}
