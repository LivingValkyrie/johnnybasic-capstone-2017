﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Mike Dobson
/// Created: 11-03-16
/// 
/// Description: BehaviorNode is the base for the behavior nodes that will define specific behaviors that enemies
/// and companions will use to navigate the environment
/// </summary>

public enum BehaviorType
{
    StopTurn,
    StopContinue,
    JumpZone,
    LandingPad
};

public class BehaviorNode : MonoBehaviour {

    #region Variables
    public BehaviorType thisType = BehaviorType.StopTurn; //The specific type for this behavior node

    public List<GameObject> LandingPads = new List<GameObject>(1);
    public List<GameObject> CurvePoints = new List<GameObject>(1);
    public List<float> JumpTimes = new List<float>(1);

    //public GameObject jumpEnd; //The place where the jump will end
    //public GameObject curvePoint; //The point used to define the curve of the jump
    //public float jumpTime; //the time it will take to complete the jump
    public int nodeRequirement = 5; //the requirement that will be used to define if a character qualifies for the node
    public float waitTime; //The time in seconds the character will wait at a stop node
    public bool leftEntry; //Defines if a jump zone is entered from the left or right
    #endregion

    void OnDrawGizmos()
    {
        //get the box component from this object
        BoxCollider2D tempCollider = GetComponent<BoxCollider2D>();
        //Set the color based on the type of behavior node that this node is
        switch(thisType)
        {
            case BehaviorType.StopTurn:
                Gizmos.color = new Color(1, 1, 0, .25f);
                break;
            case BehaviorType.StopContinue:
                Gizmos.color = new Color(0, 1, 0, .25f);
                break;
            case BehaviorType.JumpZone:
                //Draw a Line for the end zone then change the color for the box
                Gizmos.color = new Color(1, 0, 0, .5f);
                if(LandingPads != null)
                {
                    //Draws the line that connects the start end end of a jump
                    foreach(GameObject jumpEnd in LandingPads)
                    Gizmos.DrawLine(transform.position + new Vector3(tempCollider.offset.x, tempCollider.offset.y),
                        jumpEnd.transform.position + new Vector3(jumpEnd.GetComponent<BoxCollider2D>().offset.x, jumpEnd.GetComponent<BoxCollider2D>().offset.y));
                }

                if (JumpTimes != null)
                {
                    for(int j = 0; j < JumpTimes.Count; j++)
                    {
                        Gizmos.color = Color.green;
                        Vector3 bezierStart = transform.position;
                        Vector3 lineStart = bezierStart;
                        //@reference Tiffany Fisher
                        for (int i = 1; i <= 10; i++)
                        {
                            //Defines the line that the character will actually jump on
                            Vector3 lineEnd = GetPoint(bezierStart, LandingPads[j].transform.position, CurvePoints[j].transform.position, i / 10f);
                            Gizmos.DrawLine(lineStart, lineEnd);
                            lineStart = lineEnd;
                        }
                    }
                }

                Gizmos.color = new Color(0, 1, 1, .25f);
                break;
            case BehaviorType.LandingPad:
                Gizmos.color = new Color(1, 0, 0, .25f);
                break;
        }
        //Draw the gizmo that defines the location of the behavior node
        Gizmos.DrawCube(transform.position + new Vector3(tempCollider.offset.x, tempCollider.offset.y), new Vector3(tempCollider.size.x, tempCollider.size.y));
    }

    //@reference Tiffany Fisher
    public Vector3 GetPoint(Vector3 start, Vector3 end, Vector3 curve, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        return oneMinusT * oneMinusT * start + 2f * oneMinusT * t * curve + t * t * end;
    }
}
