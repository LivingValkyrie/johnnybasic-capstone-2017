﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Mike Dobson
/// Created: 9-23-16
/// 
/// Description: AIBaseBehavior is the base of the enemy AI behavioral tree.
/// This script will be used by both enemy and companion AI to navigate around
/// the environment.
/// </summary>

public class AIBaseBehavior : NonPlayerControllerInput {

    #region Variables
    Vector3 movementVector; //The vector that will be acted upon each fixed update to create the movement
    public float sprintVelocity = 6; //The velocity that the character moves while sprinting
    public float runVelocity = 4; //The velocity that the chracter moves while running
    public float walkVelocity = 2; //The velocity that the character moves while walking
    public bool moveLeft = true; //Is the character working in left movement
    public int jumpQualifier = 3; //The qualification that this entity will pass in order to make a jump
    #endregion

    // Use this for initialization
    protected override void Awake() {
        base.Awake();
        moveSpeed = walkVelocity; //Set the current movement speed to the walking velocity of the character
	}

	protected override Vector2 GetInput() {
		//print("Called get input");
		Vector2 targetVelocity;
		if (moveLeft) {
			targetVelocity = Vector2.left;
		} else {
			targetVelocity = Vector2.right;
		}

		return targetVelocity;
	}

	// Update is called once per frame
	protected override void Update () {
		base.Update();
    }

    //Update that is called based on a fixed frame rate
    protected virtual void FixedUpdate()
    {
        Controller.Move(velocity * Time.deltaTime);
    }
}
