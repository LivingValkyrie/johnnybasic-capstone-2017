﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: DamageTrigger
/// </summary>
public class DamageTrigger : MonoBehaviour {
	#region Fields

	public float damage;
	public bool doNotDestroy = false;

	#endregion

	void Start() {
		if (doNotDestroy) {
			return;
		}

		if (GetComponent<Animator>()) {
			Destroy(gameObject, GetComponent<Animator>().GetCurrentAnimatorClipInfo(0).Length);
			//print("Anim " + GetComponent<Animator>().GetCurrentAnimatorClipInfo(0).Length);
		} else if (GetComponent<Animation>()) {
			Destroy(gameObject, GetComponent<Animation>().clip.length);
		} else {
			Destroy(gameObject, 0.2f);
		}

		objects = new Dictionary<Collider2D, float>();
	}

	void OnTriggerEnter2D(Collider2D other) {
		//print("Triggered: " + other.tag);
		if (other.tag == "Enemy") {
			other.GetComponent<CharacterStats>().TakeDamage(damage);
		}
	}

	Dictionary<Collider2D, float> objects;
	void OnTriggerStay2D( Collider2D other ) {
		if (objects.ContainsKey(other)) {
			objects[other] += Time.deltaTime;

			if (other.tag == "Enemy" && objects[other] >= 1) {
				other.GetComponent<CharacterStats>().TakeDamage(damage);
				objects[other] = 0;
			}
		} else {
			objects.Add(other, 0);
		}
	}
}