﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: MassHealingPlantSummon
/// </summary>
public class MassHealingPlantSummon : MonoBehaviour {
	#region Fields

	public int amountToHeal;
	public int minPlants, maxPlants;
	public GameObject plantPrefab;

	#endregion

	void Start() {
		int count = Random.Range(minPlants, maxPlants);
		for (int i = 0; i < count; i++) {
			Vector3 spawnPos = Camera.main.ViewportToWorldPoint(Random.insideUnitCircle);
			spawnPos.z = 0;
			var temp = Instantiate(plantPrefab, spawnPos, Quaternion.identity);
			temp.GetComponent<HealingTrigger>().amountToHeal = amountToHeal;
		}
	}

}