﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SetTargetTrigger
/// </summary>
public class SetTargetTrigger : MonoBehaviour {
	#region Fields

	public GameObject newTarget;

	#endregion

	void Start() {
		if (GetComponent<Animation>()) {
			Destroy(gameObject, GetComponent<Animation>().clip.length);
			//Debug.Log("anim");
		} else {
			Destroy(gameObject, 0.2f);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		//print("Triggered: " + other.tag);
		if (other.tag == "Enemy") {
			other.GetComponent<StateMachineEnemy>().SetTarget(newTarget);
		}
	}
}