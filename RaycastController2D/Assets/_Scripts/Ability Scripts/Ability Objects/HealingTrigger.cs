﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: HealingTrigger
/// </summary>
public class HealingTrigger : MonoBehaviour {
	#region Fields

	public int amountToHeal;

	#endregion

	void OnTriggerEnter2D(Collider2D other) {
		//print("Triggered: " + other.tag);
		if (other.tag == "Player" || other.tag == "PlayerTwo") {
			other.GetComponent<CharacterStats>().TakeDamage(amountToHeal, CharacterStats.DamageType.Heal);
			Destroy(gameObject);
		}
	}
}