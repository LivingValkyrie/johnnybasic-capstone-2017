﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SpeedDartBuff
/// </summary>
public class SpeedDartBuff : MonoBehaviour {
	#region Fields

	public float duration = 10;
	float walkSpeed, sprintSpeed;
	public AudioClip buffSound;

	//todo apply buff to cooldowns aswell, maybe use a modifier value that is assigned to player that gets used along with ability CD on cooldown checks
	//todo make buff size variable (15% and 25%)

	#endregion

	void Start() {
		//print(name);
		//Debug.Break();
		if (GetComponent<SpeedDartBuff>()) {
			if (GetComponent<SpeedDartBuff>() != this) {
				GetComponent<SpeedDartBuff>().duration = duration;
				Destroy(this);
				return;
			}
		}

		CharacterStats stats = GetComponent<CharacterStats>();
		walkSpeed = stats.DataAsPlayer.WalkSpeed;
		stats.data.moveSpeed = walkSpeed * 1.25f;
		//print(walkSpeed + " walk");

		sprintSpeed = stats.DataAsPlayer.sprintSpeed;
		stats.DataAsPlayer.sprintSpeed = sprintSpeed * 1.25f;
		//print(sprintSpeed + " sprint");

		Destroy(this, duration);
		GameController.instance.PlaySoundEffect( buffSound );

	}

	void OnDestroy() {
		//print("On destroy");
		GetComponent<CharacterStats>().DataAsPlayer.moveSpeed = walkSpeed;
		GetComponent<CharacterStats>().DataAsPlayer.sprintSpeed = sprintSpeed;
	}
}