﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SpeedDartTrigger
/// </summary>
public class SpeedDartTrigger : MonoBehaviour {
	#region Fields

	#endregion

	void Start() {
		if ( GetComponent<Animation>() ) {
			Destroy( gameObject, GetComponent<Animation>().clip.length );
		} else {
			Destroy( gameObject, 0.2f );
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		//print("hit: " + other.name);
		if (other.tag == "Player" || other.tag == "PlayerTwo") {
			other.gameObject.AddComponent<SpeedDartBuff>();
		}
	}

}