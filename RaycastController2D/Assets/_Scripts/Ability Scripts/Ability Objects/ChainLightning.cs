﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.PlaymodeTestsRunner;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: ChainLightning
/// </summary>
public class ChainLightning : MonoBehaviour {
	#region Fields

	public GameObject origin;
	public int iterations;
	public int damage;

	#endregion

	void Start() {
		iterations -= 1;

		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
		List<GameObject> enemiesInView = new List<GameObject>();
		foreach (GameObject enemy in enemies) {
			if (enemy.GetComponent<Renderer>().isVisible) {
				enemiesInView.Add(enemy);
			}
		}

		GameObject closest = null;
		float magnitudeToClosest = 0;

		foreach (GameObject enemy in enemiesInView) {
			//get range from JB and what side they are on
			//if first or closer than closest make it closest and closest to second closest
			if (enemy == origin) {
				continue;
			}

			Vector3 temp = enemy.transform.position - origin.transform.position;

			//print(temp.magnitude + " for " + enemy.name);

			if (closest == null) {
				closest = enemy;
				magnitudeToClosest = temp.magnitude;
			} else {
				//if closer update values
				if (temp.magnitude < magnitudeToClosest) {
					closest = enemy;
					magnitudeToClosest = temp.magnitude;
				}
			}
		}

		if (closest) {
			transform.SetParent(closest.transform);
			transform.localPosition = Vector3.zero;
			name = origin.name + " " + closest.name;
		} else {
			//no enemy so destroy
			Destroy(gameObject);
			return;
		}

		GetComponent<LineRenderer>().SetPositions(new Vector3[] {origin.transform.position, transform.position});

		//Debug.Break();
		//spawn line renderers and damage closest and second closest
		if (iterations > 0) {
			var temp = Instantiate(gameObject, closest.transform);

			//temp.transform.localPosition = Vector3.zero;
			temp.GetComponent<ChainLightning>().iterations = iterations;
			temp.GetComponent<ChainLightning>().origin = closest;
		}

		Invoke("DoDamage", 0.15f);
	}

	void DoDamage() {
		if (GetComponentInParent<CharacterStats>()) {
			GetComponentInParent<CharacterStats>().TakeDamage(damage, CharacterStats.DamageType.Shock);
		}

		Destroy(gameObject);
	}

}