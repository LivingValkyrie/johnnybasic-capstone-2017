﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: StunTrigger
/// </summary>
public class StunTrigger : MonoBehaviour {
	#region Fields

	public float stunTime = 2.0f;

	#endregion

	void Start() {
		if (GetComponent<Animation>()) {
			Destroy(gameObject, GetComponent<Animation>().clip.length);
		} else {
			Destroy(gameObject, 0.2f);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		//print("Triggered: " + other.tag);
		if (other.tag == "Enemy") {
			other.GetComponent<StateMachineEnemy>().StunEnemy(stunTime);
		}
	}
}