﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Projectile is a damage dealing object that flies with speed and direction. can hurt anything with "CharacterStats" and destroy distructible objects.
/// </summary>
public class Projectile : MonoBehaviour {
	#region Fields

	//the direction to move
	public Vector2 direction;

	//the speed in which to move
	public float speed;

	//the amount of damage when hitting something not on "layers to ignore"
	public int damage;

	public bool usesGravity;
	public float fallOff = 0.01f;

	//how long the projectille will last in seconds
	public float dieTime = 5f;

	[Tooltip("Objects on the selected layers will cause the bullet to destroy itself.")]
	public LayerMask layersToDestroySelf;

	public bool playerBullet;
	public bool spawnObjOnDestroy;
	[Tooltip("This prefab needs to be set up in the editor, no initalization happens on spawn.")]
	public GameObject objToSpawnOnDestroy;
	public bool stunOnImpact;
	public float stunTime = 2.0f;
	SpriteRenderer sr;

	#endregion

	/// <summary>
	/// Invokes Destroy with a timer of dieTime
	/// </summary>
	void Start() {

		//Debug.LogWarning( "Projectile spawned" );
		//print("die in " + dieTime);

		Destroy(gameObject, dieTime);
		sr = GetComponent<SpriteRenderer>();
	}

	/// <summary>
	/// Updates position of object
	/// </summary>
	void Update() {
		transform.Translate(direction * speed * Time.deltaTime);
		if (usesGravity) direction.y -= fallOff;
		sr.flipX = direction.x < 0;

	}

	/// <summary>
	/// Called on 2D trigger. returns if [other] is on an ignored layer. does damage and destroys destructibles if not.
	/// </summary>
	/// <param name="other">The other.</param>
	void OnTriggerEnter2D(Collider2D other) {
		bool destroy = false;

		//print( "hit: " + other.tag );

		switch (other.transform.tag) {
			case "Destructible":
				other.gameObject.GetComponent<Destructible>().Destruct();
				destroy = true;
				break;
			case "Player":
			case "PlayerTwo":
				if (!playerBullet) {
					if (other.gameObject.GetComponent<CharacterStats>()) {
						other.gameObject.GetComponent<CharacterStats>().TakeDamage(damage);
						destroy = true;
					}
				}
				break;
			case "Enemy":
				if (playerBullet) {
					if (other.gameObject.GetComponent<CharacterStats>()) {
						other.gameObject.GetComponent<CharacterStats>().TakeDamage(damage);
						destroy = true;

						if (stunOnImpact) {
							other.GetComponent<StateMachineEnemy>().StunEnemy(stunTime);
						}
					}
				}
				break;
			default:
				if (GetComponent<Collider2D>().IsTouchingLayers(layersToDestroySelf)) {
					destroy = true;
				}
				break;
		}

		if (destroy) {
			if (spawnObjOnDestroy) {
				GameObject temp = Instantiate(objToSpawnOnDestroy, transform.position, Quaternion.identity);
				if (temp.GetComponent<DamageTrigger>()) {
					temp.GetComponent<DamageTrigger>().damage = damage;
				}
				if (temp.GetComponent<HealingTrigger>()) {
					temp.GetComponent<HealingTrigger>().amountToHeal = damage;
				}
				if (temp.GetComponent<GravityObject>()) {
					temp.transform.position = temp.transform.position + Vector3.up;
				}
			}
			Destroy(gameObject);
		}
	}

	void OnTriggerStay2D(Collider2D other) {
		OnTriggerEnter2D(other);
	}

	void OnTriggerExit2D( Collider2D other ) {
		OnTriggerEnter2D( other );
	}
}