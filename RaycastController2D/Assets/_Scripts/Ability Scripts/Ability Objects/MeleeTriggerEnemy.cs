﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: MeleeTriggerEnemy
/// </summary>
public class MeleeTriggerEnemy : MonoBehaviour {
	#region Fields

	public int damage;

	#endregion

	void Start() {
		if (GetComponent<Animation>()) {
			Destroy(gameObject, GetComponent<Animation>().clip.length);
		} else {
			Destroy(gameObject, 0.2f);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player" || other.tag == "PlayerTwo") {
			other.gameObject.GetComponent<CharacterStats>().TakeDamage(damage);
			//Debug.Log("HIT " + other.name + " for " + damage);
		} else if (other.tag == "Destructible") {
			other.GetComponent<Destructible>().Destruct();
		}
	}
}