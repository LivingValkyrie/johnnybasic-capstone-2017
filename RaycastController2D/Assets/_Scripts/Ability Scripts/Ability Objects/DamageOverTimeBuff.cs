﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: DamageOverTimeBuff
/// </summary>
public class DamageOverTimeBuff : MonoBehaviour {
	#region Fields

	public bool healProportionateAmount = true;
	public CharacterStats.DamageType type;
	public int healthPerTick;
	int ticks = 0;
	int totalTicks;

	#endregion

	public void Init(CharacterStats.DamageType type, int ticks, int dmgAmount, bool fixedDamage = true, float proportionalDamge = 0.3f) {
		this.totalTicks = ticks;
		this.type = type;

		if (fixedDamage) {
			healthPerTick = dmgAmount;
		} else {
			int totalDmg = (int) (GetComponent<CharacterStats>().data.maxHealth * proportionalDamge);
			healthPerTick = totalDmg / ticks;
		}

		InvokeRepeating("Tick", 0, 1);
	}

	void Tick() {
		if (GetComponent<CharacterStats>().isKOd) {
			Destroy( this );
			return;
		}
		ticks++;
		GetComponent<CharacterStats>().TakeDamage(healthPerTick, type);

		if (ticks >= totalTicks) {
			Destroy(this);
		}
	}
}