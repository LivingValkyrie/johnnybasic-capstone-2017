﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SpeedDart
/// </summary>
public class SpeedDart : MonoBehaviour {
	#region Fields

	public float speed;
	public float dieTime = 5f;
	public GameObject speedDartTrigger;

	#endregion

	void Start() {
		if (GetComponent<Animation>()) {
			Destroy(gameObject, GetComponent<Animation>().clip.length);
		} else {
			Destroy(gameObject, dieTime);
		}
	}

	void Update() {
		//transform.Translate(Vector3.up * speed * Time.deltaTime);
	}

	void OnDestroy() {
		Instantiate(speedDartTrigger, transform.position, Quaternion.identity);
	}
}