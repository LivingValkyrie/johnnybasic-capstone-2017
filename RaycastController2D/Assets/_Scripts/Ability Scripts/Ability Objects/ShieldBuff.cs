﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: ShieldBuff
/// </summary>
public class ShieldBuff : MonoBehaviour {
	#region Fields

	public float duration;
	public GameObject caster;
	public GameObject shieldSprite;
	public AudioClip buffSound;

	float defenseNew, defenseOriginal;
	GameObject shieldSpriteInstance;

	#endregion

	void EndBuff() {
		//pull in def, add 50%, start method that destroys this after set time and setting def back to original
		Destroy(shieldSpriteInstance);
		Destroy(this);
	}

	void Start() {
		if (GetComponent<ShieldBuff>()) {
			if (GetComponent<ShieldBuff>() != this) {
				GetComponent<ShieldBuff>().duration = duration;
				Destroy(this);
				return;
			}
		}

		defenseOriginal = GetComponent<CharacterStats>().DataAsPlayer.defense;
		defenseNew = defenseOriginal * 1.5f;
		GetComponent<CharacterStats>().DataAsPlayer.defense = defenseNew;
		shieldSpriteInstance = Instantiate(shieldSprite, caster.transform, false);
		shieldSpriteInstance.transform.localPosition = Vector3.zero;

		Destroy(this, duration);
		GameController.instance.PlaySoundEffect( buffSound );

	}

	void OnDestroy() {
		GetComponent<CharacterStats>().DataAsPlayer.defense = defenseOriginal;
		Destroy(shieldSpriteInstance);
	}
}