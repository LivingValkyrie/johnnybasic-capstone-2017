﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: ProjectileAbility is an ability that allows for creating projectile objects
/// </summary>
[CreateAssetMenu(order = 1)]
public class PlayerProjectileAbility : PlayerAbility {
	#region Fields

	[Header("Projectile Stuff")]
	[Tooltip("The prefab that will be instanitated when the ability is cast")]
	public GameObject projectileGo;
	public bool multiShot = false;
	public int minShots = 3, maxShots = 7;

	Projectile projectile;
	int count = 1;

	#endregion

	/// <summary>
	/// Initializes the projectile component of [projectileGO]
	/// </summary>
	public override void Init(CharacterData data, AbilitySlot slot) {
		base.Init(data, slot);

		projectile = projectileGo.GetComponent<Projectile>();
		projectile.speed = atkSpd;
		projectile.dieTime = atkRng;

		//if ( multiShot ) {
		//	count = Random.Range( minShots, maxShots + 1 );
		//	atkDmg /= count;
		//}

		projectile.damage = atkDmg;

		//Debug.Log($"init called by {data.characterName} with speed {atkSpd}-{projectile.speed} and {atkRng}-{projectile.dieTime} on {projectile.name} in slot {slot}");
	}

	/// <summary>
	/// call this when the ability is being used, this is for projectiles that can be aimed.
	/// </summary>
	/// <param name="direction">The direction.</param>
	/// <param name="position">The position.</param>
	/// <returns></returns>
	public override bool OnCast(Vector2 direction, Vector3 position, GameObject caster) {
		//Debug.Log(name + " casting " + abilityName);
		//Vector2 dir = new Vector2( caster.GetComponent<Controller2D>().Velocity.x, caster.GetComponent<Controller2D>().Velocity.y );
		projectile.direction = direction; // + dir;
		int multiShotAtk = atkDmg;
		if ( multiShot ) {
			count = Random.Range( minShots, maxShots + 1 );
			multiShotAtk /= count;
		}

		projectile.damage = multiShotAtk;

		//Debug.Log(count);
		//projectile.speed = AtkSpeed + caster.GetComponent<Controller2D>().Velocity.magnitude;
		for (int i = 0; i < count; i++) {
			if (i > 0) {
				projectile.direction.y += Random.Range(-1f, 1f);
			}
			Instantiate(projectileGo, position, Quaternion.identity);
		}

		GameController.instance.PlaySoundEffect(soundEffect);

		caster.GetComponent<SpriteRenderer>().flipX = direction.x < 0;

		return true;
	}
}