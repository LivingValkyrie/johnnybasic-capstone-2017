﻿using System.Runtime.InteropServices;
using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PlayerProjectileAbility is an ability that allows for creating projectile objects
/// </summary>
[CreateAssetMenu(order = 2)]
public class EnemyProjectileAbility : EnemyAbility {
	#region Fields

	[Tooltip("The prefab that will be instanitated when the ability is cast")]
	public GameObject projectileGo;

	//public float defaultDirection;
	//public Vector3 defaultPosition;

	Projectile projectile;

	#endregion

	/// <summary>
	/// Initializes the projectile component of [projectileGO]
	/// </summary>
	public override void Init(CharacterData data = null, AbilitySlot slot = AbilitySlot.AttackMain) {
		projectile = projectileGo.GetComponent<Projectile>();
		projectile.damage = atkDmg;
		projectile.speed = atkSpd;
		projectile.dieTime = atkRng;
	}

	/// <summary>
	/// call this when the ability is being used, this is for projectiles that can be aimed.
	/// </summary>
	/// <param name="direction">The direction.</param>
	/// <param name="position">The position.</param>
	/// <returns></returns>
	public override bool OnCast(Vector2 direction, Vector3 position, GameObject caster) {
		projectile.direction = direction;
		//var temp = 
			Instantiate(projectileGo, position, Quaternion.identity);
		//Debug.Log(temp + " shot");
		GameController.instance.PlaySoundEffect( soundEffect );
		return true;
	}
}