﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: EnemyMeleeAbility
/// </summary>
[CreateAssetMenu(order = 2)]
public class EnemyMeleeAbility : EnemyAbility {
	#region Fields

	public MeleeTriggerEnemy collisionVolume, collisionVolumeLeft;

	#endregion

	public override void Init(CharacterData data, AbilitySlot slot) {
		//used to set up meleeTrigger
		collisionVolume.damage = this.atkDmg;
		collisionVolumeLeft.damage = this.atkDmg;
	}

	public override bool OnCast(Vector2 direction, Vector3 position, GameObject caster) {
		MeleeTriggerEnemy toInstaniate;

		if (Mathf.Sign(direction.x) > 0) {
			//right
			//Debug.Log("right");
			toInstaniate = collisionVolume;
		} else {
			//left
			//Debug.Log("left");
			toInstaniate = collisionVolumeLeft;
		}

		GameObject temp = Instantiate(toInstaniate.gameObject, position, Quaternion.identity);
		temp.transform.SetParent(caster.transform);
		GameController.instance.PlaySoundEffect(soundEffect);

		return true;
	}
}