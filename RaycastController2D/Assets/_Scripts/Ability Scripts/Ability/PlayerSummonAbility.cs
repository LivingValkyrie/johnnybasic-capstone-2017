﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PlayerSummonAbility
/// </summary>
[CreateAssetMenu(order = 1)]
public class PlayerSummonAbility : PlayerAbility {
	#region Fields

	public GameObject toSummon;
	public PlayerAbility riderAbility;
	public GameObject toSummonLeft;

	#endregion

	public override void Init(CharacterData data, AbilitySlot slot) {
		base.Init( data, slot );
	}

	public override bool OnCast(Vector2 direction, Vector3 position, GameObject caster) {
		//Debug.Log(caster);
		GameObject toInstaniate = toSummon;

		if (toSummon.GetComponentInChildren<StunTrigger>()) {
			if (Mathf.Sign(direction.x) > 0) {
				//right
				//Debug.Log( "right" );

			} else {
				//left
				//Debug.Log("left");
				toInstaniate = toSummonLeft;
			}
		}

		GameObject temp = Instantiate(toInstaniate,
		                              caster.transform.position,
		                              Quaternion.identity);
		if (temp.GetComponent<SetTargetTrigger>()) {
			temp.GetComponent<SetTargetTrigger>().newTarget = caster;
		}
		if (temp.GetComponent<DamageTrigger>()) {
			temp.GetComponent<DamageTrigger>().damage = AtkDamage;
		}
		if (temp.GetComponent<MassHealingPlantSummon>()) {
			temp.GetComponent<MassHealingPlantSummon>().amountToHeal = AtkDamage;
		}
		if (temp.GetComponent<ChainLightning>()) {
			temp.GetComponent<ChainLightning>().damage = AtkDamage;
			temp.GetComponent<ChainLightning>().origin = caster;
		}

		if (riderAbility) {
			riderAbility.OnCast(direction, position, caster);
		}

		caster.GetComponent<SpriteRenderer>().flipX = direction.x < 0;

		return true;
	}
}