﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: JohnnyBasicLimitAbility
/// </summary>
[CreateAssetMenu( order = 1 )]
public class JohnnyBasicLimitAbility : PlayerAbility {
	#region Fields

	public PlayerAbility first, second, third;
	#endregion
	
	public override bool OnCast(Vector2 direction, Vector3 position, GameObject caster) {
		switch (GameController.instance.progress.playerOneSecondToLastCast) {
			case AbilitySlot.AttackMain:
			case AbilitySlot.AttackSecondary:
			case AbilitySlot.First:
				first.OnCast(direction, position, caster);
				break;
			case AbilitySlot.Second:
				second.OnCast( direction, position, caster );
				break;
			case AbilitySlot.Third:
			case AbilitySlot.Limit:
			case AbilitySlot.Potion:
				third.OnCast( direction, position, caster );
				break;
		}

		GameController.instance.PlaySoundEffect( soundEffect );
		return true;
	}
}