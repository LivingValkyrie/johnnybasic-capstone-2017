﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PlayerPotionAbility
/// </summary>
[CreateAssetMenu(order = 1)]
public class PlayerPotionAbility : PlayerAbility {
	#region Fields

	#endregion

	public override bool OnCast(Vector2 direction, Vector3 position, GameObject caster) {
		//Debug.LogError("ON CAST CALLED");
		if (GameController.instance.progress.currPotions > 0) {
			GameController.instance.progress.currPotions--;

			DamageOverTimeBuff p1Buff = GameController.instance.ActivePlayerGameObject.AddComponent<DamageOverTimeBuff>();
			p1Buff.Init(CharacterStats.DamageType.Heal, 15, 0, false);

			if (GameController.instance.InactivePlayer) {
				DamageOverTimeBuff p2Buff = GameController.instance.InactivePlayerGameObject.AddComponent<DamageOverTimeBuff>();
				p2Buff.Init(CharacterStats.DamageType.Heal, 15, 0, false);
			}

			GameController.instance.PlaySoundEffect(soundEffect);
			return true;
		} else {
			return false;
		}
	}
}