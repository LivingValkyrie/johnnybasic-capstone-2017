﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PlayerShieldAbility
/// </summary>
[CreateAssetMenu(order = 1)]
public class PlayerShieldAbility : PlayerAbility {
	#region Fields

	public float shieldDuration;
	public GameObject shieldSprite;


	#endregion

	public override bool OnCast(Vector2 direction, Vector3 position, GameObject caster) {
		var buff = caster.AddComponent<ShieldBuff>();
		buff.duration = shieldDuration;
		buff.caster = caster;
		buff.shieldSprite = shieldSprite;
		GameController.instance.PlaySoundEffect( soundEffect );
		return true;
	}
}