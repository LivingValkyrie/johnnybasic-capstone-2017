﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PlayerDashingAttackAbility
/// </summary>
[CreateAssetMenu(order = 1)]
public class PlayerDashingAttackAbility : PlayerAbility {
	#region Fields

	public float damage;

	#endregion

	public override bool OnCast(Vector2 direction, Vector3 position, GameObject caster) {
		//set player to move with dash
		caster.GetComponent<PlayerControllerInput>().AirDash((int) Mathf.Sign(caster.GetComponent<Player>().FaceDir), true);

		//attach trigger component that damages enemies
		var temp = caster.AddComponent<DamageTrigger>();
		temp.damage = damage;
		temp.doNotDestroy = true;

		//set to invulnerable, this also reverses things after half second
		caster.GetComponent<CharacterStats>().SetInvulnerable();
		GameController.instance.PlaySoundEffect( soundEffect );
		caster.GetComponent<SpriteRenderer>().flipX = direction.x < 0;

		return true;
	}
}