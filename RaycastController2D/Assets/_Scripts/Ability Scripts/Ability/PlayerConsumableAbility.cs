﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PlayerConsumableAbility is a player ability that requires the consumption of a preset item to cast.
/// </summary>
[CreateAssetMenu(order = 1)]
public class PlayerConsumableAbility : PlayerAbility {
	#region Fields

	//the item consumed on use
	//the ability to cast when successful
	public PlayerAbility abilityToCast;
	PlayerData pData;
	#endregion

	/// <summary>
	/// In place for initializing component data if needed.
	/// </summary>
	/// <param name="data"></param>
	/// <param name="slot"></param>
	public override void Init(CharacterData data, AbilitySlot slot) {
		base.Init(data, slot);
		abilityToCast.Init(data, slot);
		pData = (PlayerData)data;
	}

	/// <summary>
	/// Called when [cast].
	/// </summary>
	/// <param name="direction">The direction.</param>
	/// <param name="position">The position.</param>
	/// <param name="caster"></param>
	/// <returns></returns>
	public override bool OnCast(Vector2 direction, Vector3 position, GameObject caster) {
		//attempt to remove item, if successful then cast ability
		if ( pData.GetConsumableCount() > 0 ) {
			pData.UseConsumable();

			abilityToCast.OnCast( direction, position, caster );

			return true;
		} else {
			Debug.Log( "no item for use in casting" );
			return false;
		}
	}
}