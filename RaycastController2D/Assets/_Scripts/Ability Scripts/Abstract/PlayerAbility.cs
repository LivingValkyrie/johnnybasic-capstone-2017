﻿using System;
using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PlayerAbility is the parent class for all player abilities.
/// </summary>
public abstract class PlayerAbility : Ability {
	#region Fields

	[Tooltip("The name of the ability")]
	public string abilityName;
	[Tooltip("A short description of the ability")]
	public string description;
	[Tooltip("The icon to show on the hud")]
	public Sprite icon;
	[Tooltip( "The icon to show on the hud" )]
	public Sprite iconOnCooldown;
	[Tooltip("How long it takes the ability to recharge")]
	[SerializeField]
	protected float cooldown;
	[Tooltip("How fast the ability increases the limit gauge.")]
	[SerializeField]
	protected float limitGaugeModifier = 1f;

	[Header("Modding")]
	public bool canBeModded = true;
	public AbilityMod upgradeOne;
	public AbilityMod upgradeTwo;
	public AbilityMod upgradeThree;

	AbilityMod myMod;

	public override int AtkDamage {
		get { return atkDmg + myMod.atkDmg; }
	}

	public override float AtkSpeed {
		get { return atkSpd + myMod.atkSpd; }
	}

	public override float AtkRange {
		get { return atkRng + myMod.atkRng; }
	}

	public float Cooldown {
		get {
			return Mathf.Max(0, cooldown + myMod.cooldown);
		}
	}

	public float LimitGaugeModifier {
		get { return limitGaugeModifier + myMod.limitGaugeModifier; }
	}

	#endregion

	public override void Init(CharacterData data = null, AbilitySlot abilitySlot = 0) {
		if (canBeModded) {



			//Debug.Log("pull in mod data from progress and add it to this ability");
			PlayerData pData = (PlayerData) data;

			int currMods = (int) typeof(GameProgress).GetField(pData.abilityMods).GetValue(GameController.instance.progress);

			int toShift;
			int mask;
			switch (abilitySlot) {
				case AbilitySlot.AttackMain:
					toShift = (int) AbilityModding.ShiftToFirst;
					mask = (int) AbilityModding.MaskFirst;
					break;
				case AbilitySlot.AttackSecondary:
					toShift = (int) AbilityModding.ShiftToSecond;
					mask = (int) AbilityModding.MaskedSecond;
					break;
				case AbilitySlot.First:
					toShift = (int) AbilityModding.ShiftToThird;
					mask = (int) AbilityModding.MaskThird;
					break;
				case AbilitySlot.Second:
					toShift = (int) AbilityModding.ShiftToFourth;
					mask = (int) AbilityModding.MaskFourth;
					break;
				case AbilitySlot.Third:
					toShift = (int) AbilityModding.ShiftToFifth;
					mask = (int) AbilityModding.MaskFifth;
					break;
				default:
					toShift = 0;
					mask = 0;
					break;
			}

			//mask to get value, then shift todo this calculation is broken
			//Debug.Log("mods: " + Convert.ToString(currMods,2));
			//Debug.Log( "mask: " + Convert.ToString( mask,2 ));
			//Debug.Log(currMods &= mask);
			//Debug.Log( currMods >> toShift );
			currMods &= mask;
			currMods = currMods >> toShift;
			AbilityModding mods = (AbilityModding) currMods;

			myMod = new AbilityMod();
			switch (mods) {
				case AbilityModding.UnlockedNone:

					//no change
					break;
				case AbilityModding.UnlockedThreeOnly:
					myMod += upgradeThree;
					break;
				case AbilityModding.UnlockedTwoOnly:
					myMod += upgradeTwo;
					break;
				case AbilityModding.UnlockedTwoAndThree:
					myMod += upgradeThree;
					myMod += upgradeTwo;
					break;
				case AbilityModding.UnlockedOneOnly:
					myMod += upgradeOne;
					break;
				case AbilityModding.UnlockedOneAndThree:
					myMod += upgradeOne;
					myMod += upgradeThree;
					break;
				case AbilityModding.UnlockedOneAndTwo:
					myMod += upgradeTwo;
					myMod += upgradeOne;
					break;
				case AbilityModding.UnlockedAll:
					myMod += upgradeTwo;
					myMod += upgradeOne;
					myMod += upgradeThree;
					break;
				default:
					throw new ArgumentOutOfRangeException($"some how mods got out of range for {data.characterName}.{this.abilityName}");
			}
		}

		//Debug.Log(abilityName + " " + abilitySlot +" "+ myMod + " "+ mods);
	}

	public override string ToString() {
		return
			$"{abilityName}: {atkDmg}-{AtkDamage} | {atkRng}-{AtkRange} | {atkSpd}-{AtkSpeed} | {cooldown}-{Cooldown} | {limitGaugeModifier}-{LimitGaugeModifier}\n" +
			myMod;
	}
}