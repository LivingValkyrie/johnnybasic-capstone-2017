﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: EnemyAbility
/// </summary>
public abstract class EnemyAbility : Ability {
	#region Fields

	//add [CreateAssetMenu] to any children classes

	public override int AtkDamage
	{
		get { return atkDmg; }
	}

	public override float AtkSpeed
	{
		get { return atkSpd; }
	}

	public override float AtkRange
	{
		get { return atkRng; }
	}

	#endregion
}