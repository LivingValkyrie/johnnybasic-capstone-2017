﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: Ability is the parent class for all abilities, player and enemy.
/// </summary>
public abstract class Ability : ScriptableObject {
	#region Fields

	public abstract int AtkDamage { get; }
	public abstract float AtkSpeed { get; }
	public abstract float AtkRange { get; }

	//the damage of the attack
	[SerializeField]
	protected int atkDmg;

	//the speed of the attack
	[SerializeField]
	protected float atkSpd;

	//the range of the attack
	[SerializeField]
	protected float atkRng;
	[HideInInspector]
	public GameObject myCaster;

	public AudioClip soundEffect;

	#endregion

	/// <summary>
	/// In place for initializing component data if needed.
	/// </summary>
	public abstract void Init(CharacterData data = null, AbilitySlot slot = 0);

	/// <summary>
	/// call this when the ability is being used
	/// </summary>
	/// <param name="direction">The direction.</param>
	/// <param name="position">The position.</param>
	/// <param name="caster">The object that called the abilities oncast</param>
	/// <returns>The success of the cast.</returns>
	public virtual bool OnCast(float direction, Vector3 position, GameObject caster = null) {
		if (Mathf.Sign(direction) == -1) {
			return OnCast(Vector2.left, position, caster);
		} else {
			return OnCast(Vector2.right, position, caster);
		}
	}

	/// <summary>
	/// Called when [cast].
	/// </summary>
	/// <param name="direction">The direction.</param>
	/// <param name="position">The position.</param>
	/// <param name="caster">The object that called the abilities oncast</param>
	/// <returns></returns>
	public abstract bool OnCast(Vector2 direction, Vector3 position, GameObject caster);

	public virtual bool OnCast(Vector2 direction, Vector3 position) {
		return OnCast(direction, position, null);
	}
}