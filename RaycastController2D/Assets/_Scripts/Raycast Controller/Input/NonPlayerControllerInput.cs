﻿
using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: NonPlayerControllerInput is parent class for npc and enemy objects
/// </summary>
public class NonPlayerControllerInput : ControllerInput {

	#region Fields

	#endregion

	/// <summary>
	/// Used to set [targetVelocity] which is used for calculating velocity
	/// </summary>
	/// <returns></returns>
	protected override Vector2 GetInput() {
		return Vector2.zero;
	}
}
