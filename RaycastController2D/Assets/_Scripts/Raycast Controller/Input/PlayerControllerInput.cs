﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: deadwynn@gmail.com
/// very basic player script for a platformer-style game. 
/// </summary>
[DisallowMultipleComponent]
public class PlayerControllerInput : ControllerInput {
	#region Fields

	[Tooltip("")]
	public bool limitJumpOnMaxSlope = false;

	[Header("Wall Jumping")]
	[Tooltip("The maximum speed when sliding down a wall")]
	public float wallSlideSpeedMax = 3;
	[Tooltip("The velocity of jumping UP a wall from a wall jump")]
	public Vector2 wallJumpClimb;
	[Tooltip("The velocity of jumping off of a wall")]
	public Vector2 wallJumpOff;
	[Tooltip("The velocity of jumping away from a wall during a wall jump")]
	public Vector2 wallLeap;
	[Tooltip("The amount of time the player has to perform a wall leap")]
	public float wallStickTime = .25f;
	float timeToWallRelease;

	[HideInInspector]
	public bool canWallJump;
	[HideInInspector]
	public bool canMultiJump;
	[HideInInspector]
	public int maxJumps = 2;
	int jumpCount = 0;
	[HideInInspector]
	public bool canAirDash;
	[HideInInspector]
	public float airDashBoost = 30f;

	//used within script
	Player player;
	bool wallSliding;
	int wallDirX;
	Vector2 targetVelocity;
	bool hasDashed = false;
	#endregion



	/// <summary>
	/// Starts this instance.
	/// </summary>
	void Start() {
		player = GetComponent<Player>();
		jumpCount = 0;
	}

	/// <summary>
	/// Used to set [targetVelocity] which is used for calculating velocity
	/// </summary>
	/// <returns></returns>
	protected override Vector2 GetInput() {
			return targetVelocity = player.directionalInput;
		
	}

	/// <summary>
	/// Calls base.update then handles wall sliding before calling move
	/// </summary>
	protected override void Update() {

		if (GameController.instance.GamePaused) {
			return;
		}

		base.Update();

		//handle wall sliding
		if (canWallJump) {
			WallSlide();
		}

		//move the player
		controller.Move(velocity * Time.deltaTime, targetVelocity);

		player.animator.SetFloat("speed",velocity.x);
		player.animator.SetFloat( "verticalSpeed", velocity.y );
		//print(velocity.y);

		//reset multijump
		if ( controller.collisions.below || wallSliding) {
			jumpCount = 0;
			hasDashed = false;
			player.animator.SetTrigger("hasLanded");
			player.animator.ResetTrigger( "runToFall" );
		} else {
			player.animator.ResetTrigger("hasLanded");
			player.animator.SetTrigger("runToFall");
		}
	}

	/// <summary>
	/// called by player input
	/// </summary>
	public void JumpPressed() {
		//print($"controller hash {controller.collisions.GetHashCode()} on player in " + gameObject.name);
		Jump();
	}

	/// <summary>
	/// limits jump velocity based on when released
	/// </summary>
	public void JumpReleased() {
		if (velocity.y > minJumpVelocity) {
			velocity.y = minJumpVelocity;
		}
	}

	/// <summary>
	/// handles wallsliding
	/// </summary>
	void WallSlide() {
		wallDirX = (controller.collisions.left) ? -1 : 1;

		//initialize wallSliding to false each frame
		wallSliding = false;

		//if you meet the requirements of a wallslide
		if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0 && controller.collisions.horizontalTag == "Wall") {
			wallSliding = true;

			//if falling faster than slide speed cap at slidespeed
			if (velocity.y < -wallSlideSpeedMax) {
				velocity.y = -wallSlideSpeedMax;
			}

			//stick to wall at the start to assist in jumping away from the wall
			if (timeToWallRelease > 0) {
				//housekeeping
				velocityXSmoothing = 0;
				velocity.x = 0;

				//reset wall sticking aswell as do countdown
				if (targetVelocity.x != wallDirX && targetVelocity.x != 0) {
					timeToWallRelease -= Time.deltaTime;
				} else {
					timeToWallRelease = wallStickTime;
				}
			} else {
				timeToWallRelease = wallStickTime;
			}
		}
	}

	void Jump() {
		//todo check for multijump
		jumpCount++;
		//print($"name {GetComponent<CharacterStats>().data.characterName} wallslide {wallSliding} below {controller.collisions.below} maxJumps {maxJumps} jumpCount {jumpCount} maxSlope {controller.collisions.slidingDownMaxSlope} ");

		if (wallSliding) {
			//jumping toward wall
			if (wallDirX == player.directionalInput.x) {
				if (canWallJump) {
					velocity.x = -wallDirX * wallJumpClimb.x;
					velocity.y = wallJumpClimb.y;
				} else {
					velocity.x = -wallDirX * wallJumpOff.x;
					velocity.y = wallJumpOff.y;
				}
			} else if (player.directionalInput.x == 0) { //jumping off wall
				velocity.x = -wallDirX * wallJumpOff.x;
				velocity.y = wallJumpOff.y;
			} else { //jumping away from wall
				if (canWallJump) {
					velocity.x = -wallDirX * wallLeap.x;
					velocity.y = wallLeap.y;
				} else {
					velocity.x = -wallDirX * wallJumpOff.x;
					velocity.y = wallJumpOff.y;
				}
			}
		}


		if ((controller.collisions.below || jumpCount <= maxJumps) && !wallSliding) {
			//print("pre " + velocity.x);
			if (controller.collisions.slidingDownMaxSlope) {
				if (limitJumpOnMaxSlope) {
					//not jumping against max slope
					if (player.directionalInput.x == Mathf.Sign(controller.collisions.slopeNormal.x)) {
						velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
						velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
					}
				} else {
					velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
					velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
				}
			} else {
				//print("max jump " + maxJumpVelocity);
				velocity.y = maxJumpVelocity;

				//print("velocity " + velocity);
			}

			player.animator.ResetTrigger("hasLanded");
			player.animator.SetTrigger( "jumping" );
			ResetIdle();
		} else {
			//print("Jump failed");
		}

		controller.Move( velocity * Time.deltaTime, targetVelocity );
	}

	/// <summary>
	/// performs an air dash if conditions are met
	/// </summary>
	/// <param name="dir">The direction of the dash. -1 for left, 1 for right.</param>
	public void AirDash(int dir, bool ignoreFlags = false) {
		if (!wallSliding || ignoreFlags) {
			if ((canAirDash && !hasDashed) || ignoreFlags) {
				velocity.x += dir * airDashBoost;

				//velocity.y = -gravity / 3;
				//print(velocity);
				hasDashed = true;
			}
		}
	}


}