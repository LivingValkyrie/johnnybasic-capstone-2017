﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: GravityObject
/// </summary>
public class GravityObject : NonPlayerControllerInput {
	#region Fields

	#endregion

	protected override Vector2 GetInput() {
		return base.GetInput();
	}

	protected override void Update() {
		base.Update();
	}

	protected virtual void FixedUpdate() {
		Controller.Move( velocity * Time.deltaTime );
	}
}