﻿using System;
using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: ControllerInput is a parent class for scripts that will control Raycast controllers. player scripts, enemy 
/// behaviors and the like. 
/// </summary>
[RequireComponent(typeof(Controller2D))]
public abstract class ControllerInput : MonoBehaviour {
	#region Fields

	[Tooltip("Debug out values when needed")]
	public bool debug = false;

	[Header("Movement Values")]
	[Tooltip("How highest you can jump")]
	public float maxJumpHeight = 4;
	[Tooltip("The minimum height you can jump")]
	public float minJumpHeight = 1;
	[Tooltip("How long it takes you to reach the top of a jump")]
	public float timeToJumpApex = 0.4f;
	[Tooltip("The speed of the controller")]
	public float moveSpeed = 6;

	//auto-calculated values used for jumping
	protected float gravity;
	protected float maxJumpVelocity;
	protected float minJumpVelocity;

	/// <summary>
	/// Used for public access to [velocity]
	/// </summary>
	public Vector3 Velocity {
		get { return velocity; }
	}

	/// <summary>
	/// value used for movement. used by jumping, smoothing and gravity. not implicitly used for controller.move
	/// </summary>
	protected Vector3 velocity;

	/// <summary>
	/// Used for public access to [controller]
	/// </summary>
	public Controller2D Controller {
		get { return controller; }
	}

	protected Controller2D controller;

	//horizontal smoothing
	protected float velocityXSmoothing = 0f;
	protected float accelerationTimeAirborn = .2f;
	protected float accelerationTimeGrounded = .1f;

	protected SpriteRenderer spriteRenderer;
	Animator animator;

	float xVelOld = 0;
	float idleFrames = 0;
	float framesUntilIdleAnim = 3;

	#endregion

	/// <summary>
	/// Sets references and calculates gravity and jump values
	/// </summary>
	protected virtual void Awake() {
		controller = GetComponent<Controller2D>();
		if (GetComponent<SpriteRenderer>()) {
			spriteRenderer = GetComponent<SpriteRenderer>();
		}

		CalculateGravityAndJumpVelocity();
		animator = GetComponent<Animator>();
	}

	/// <summary>
	/// Used to set [targetVelocity] which is used for calculating velocity
	/// </summary>
	protected abstract Vector2 GetInput();

	protected virtual void Update() {
		//if game paused, return out
		//print( GameController.instance.GamePaused + " in player");
		if (GameController.instance.GamePaused) {
			//print("Returning due to pause");
			return;
		}

		//take input
		CalculateVelocity(GetInput());

		if (velocity.x == 0) {
			idleFrames += Time.deltaTime;

			//( "flipx called in 0 " + name );
		} else {
			if (xVelOld != velocity.x) {
				spriteRenderer.flipX = Velocity.x < 0;
			}

			idleFrames = 0;
		}

		xVelOld = velocity.x;

		if (animator) {
			if (animator.ContainsParam("idle")) {
				//print(name);
				if (idleFrames >= framesUntilIdleAnim) {
					//print(name + " inside idle frames");
					animator.SetTrigger("idle");
				} else {
					//print( name + " inside reset" );

					animator.ResetTrigger("idle");
				}
			}
		}

		//}
	}

	public void ResetIdle() {
		idleFrames = 0;
	}

	protected virtual void LateUpdate() {
		//stop vertical movement if collision
		if (controller.collisions == null) {
			//print("col was null for " + name);
			controller.collisions = new Controller2D.CollisionInfo();
		}

		if (controller.collisions.above || controller.collisions.below) {
			if (controller.collisions.slidingDownMaxSlope) {
				velocity.y += controller.collisions.slopeNormal.y * -gravity * Time.deltaTime;
			} else {
				velocity.y = 0;
			}
		}
	}

	/// <summary>
	/// Calculates the gravity and jump velocity.
	/// </summary>
	protected void CalculateGravityAndJumpVelocity() {
		//calculate gravity
		gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);

		//calculate jump velocity
		maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);

		//debug to see calulation
		if (debug) {
			print("Name: " + name + " Gravity: " + gravity + " jump velocity: " + maxJumpVelocity);
		}
	}

	protected void CalculateVelocity(Vector2 input) {
		HorizontalSmoothing(input);
		ApplyGravity();
	}

	/// <summary>
	/// Applies the gravity.
	/// </summary>
	protected void ApplyGravity() {
		//if (debug) print(velocity.y + " pre");
		velocity.y += gravity * Time.deltaTime;

		//if ( debug ) print( velocity.y + " post" );
	}

	/// <summary>
	/// Horizontals the smoothing.
	/// </summary>
	/// <param name="input">The input.</param>
	protected void HorizontalSmoothing(Vector2 input) {
		float targetVelocityX = input.x * moveSpeed;

		//todo added truncation. if we start getting bugs revert this first.
		try {

			velocity.x = Mathf.SmoothDamp(velocity.x,
			                              targetVelocityX,
			                              ref velocityXSmoothing,
			                              (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborn).Truncate(5);
		} catch {
			print($"{name}" +
			      $" {velocity} " +
			      $"{targetVelocityX} " +
			      $"{velocityXSmoothing} " +
			      $"{controller.collisions.below}");
			return;
		}
	}

	/// <summary>
	/// Called when [draw gizmos].
	/// </summary>
	void OnDrawGizmos() {
#if UNITY_EDITOR
		if (debug) {
			UnityEditor.Handles.Label(transform.position + new Vector3(1, 1, 0),
			                          "Vel: " + string.Format("{0:##.00000}", velocity.x));
			UnityEditor.Handles.Label(transform.position + new Vector3(1, .5f, 0), "idle: " + idleFrames);
		}
#endif
	}
}