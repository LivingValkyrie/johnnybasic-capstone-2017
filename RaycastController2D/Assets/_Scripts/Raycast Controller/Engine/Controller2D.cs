﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: deadwynn@gmail.com 
/// Controller2D is a raycastController build for the purposes of a platformer-style game. It handles collision detection,
/// including upward and downward slopes and whether to allow movement on them or to treat them as a vertical surface.
/// </summary>
[DisallowMultipleComponent]
public class Controller2D : RaycastController {
	#region Fields

	//todo modify to allow input on slopes when moving away from slope

	[Range(1, 90)]
	[Tooltip("The maximum angle of slope the controller can climb")]
	public float maxSlopeAngle = 80;

	public bool limitVelocity = false;
	public float maxSpeed;

	//all of the info of the controllers current collisions are stored here
	public CollisionInfo collisions;
	[HideInInspector]
	public Vector2 playerInput;

	public float Facing {
		get { return collisions.faceDir; }
	}

	public Vector2 Velocity {
		get { return velocity; }
	}

	Vector2 velocity;

	#endregion

	/// <summary>
	/// Initializes the rayCastController. must use an override method that calls the base start to avoid errors on children.
	/// </summary>
	public override void Start() {
		base.Start();
		//print("Satr on rc2d on " + name);
		collisions = new CollisionInfo();
		collisions.faceDir = 1;
	}

	/// <summary>
	/// Overload method for platforms and objects that will never pass through input
	/// </summary>
	/// <param name="velocity">The velocity.</param>
	/// <param name="standingOnPlatform">if set to <c>true</c> [standing on platform].</param>
	public void Move(Vector2 velocity, bool standingOnPlatform = false) {
		Move(velocity, Vector2.zero, standingOnPlatform);
	}

	/// <summary>
	/// Moves based on the specified velocity after checking collisions. and uses input for extra decisions.
	/// </summary>
	/// <param name="velocity">The velocity of the controller.</param>
	/// <param name="input"></param>
	/// <param name="standingOnPlatform">if set to <c>true</c> [standing on platform]. used to force collisions.below in cases 
	/// where a collision below the controller wouldn't be read.</param>
	public void Move(Vector2 velocity, Vector2 input, bool standingOnPlatform = false) {
		//if(velocity.y > 0)print( $"controller hash {collisions.GetHashCode()} on controller in " + gameObject.name );

		if ( limitVelocity) {	
			velocity = LimitVelocity(velocity);
		}
		UpdateRaycastOrigins();
		collisions.Reset();
		collisions.velocityOld = velocity;
		playerInput = input;

		//if falling check if you are descending a slope
		if (velocity.y < 0) {
			DescendSlope(ref velocity);
		}

		//set face dir
		if (velocity.x != 0) {
			collisions.faceDir = (int) Mathf.Sign(velocity.x);
		}

		//if moving in the X axis check collisions
		HorizontalCollisions(ref velocity);

		//if moving in the Y axis check collisions
		if (velocity.y != 0) {
			VerticalCollisions(ref velocity);
		}

		//actually move the controller
		transform.Translate(velocity);

		//force collisions.below
		if (standingOnPlatform) {
			collisions.below = true;
		}

		this.velocity = velocity;
	}

	Vector2 LimitVelocity(Vector2 velocity) {
		if (velocity.magnitude > maxSpeed) {
			velocity = velocity.normalized * maxSpeed;
		}
		return velocity;
	}

	/// <summary>
	/// Checks Horizontal collisions and modifies the velocity if needed.
	/// </summary>
	/// <param name="velocity">The velocity of the controller.</param>
	void HorizontalCollisions(ref Vector2 velocity) {
		//get direction of movement
		float directionX = collisions.faceDir;

		//get the raylength, based on how fast you are moving + skinwidth
		float rayLength = Mathf.Abs(velocity.x) + SKIN_WIDTH;

		//set ray incase of wall jumping
		if (Mathf.Abs(velocity.x) < SKIN_WIDTH) {
			rayLength = 2 * SKIN_WIDTH;
		}

		//cast rays
		for (int i = 0; i < horizontalRayCount; i++) {
			//decide raycast origin
			Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
			rayOrigin += Vector2.up * (horizontalRaySpacing * i);

			//cast the ray
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

			//draw raycast gizmo for testing
			Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.blue);

			if (hit) {
				//if inside of the collision move to next ray
				if (hit.distance == 0) {
					continue;
				}

				//get angle of surface
				float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

				//check if you can climb the slope
				if (i == 0 && slopeAngle <= maxSlopeAngle) {
					//if you were descending
					if (collisions.descendingSlope) {
						collisions.descendingSlope = false;
						velocity = collisions.velocityOld;
					}

					//change slope angle smoothly
					float distanceToSlopeStart = 0;
					if (slopeAngle != collisions.slopeAngleOld) {
						distanceToSlopeStart = hit.distance - SKIN_WIDTH;
						velocity.x -= distanceToSlopeStart * directionX;
					}

					//climb the slope
					ClimbSlope(ref velocity, slopeAngle, hit.normal);
					velocity.x += distanceToSlopeStart * directionX;
				}

				//side collisions that are not climbable
				if (!collisions.climbingSlope || slopeAngle > maxSlopeAngle) {
					//assign new velocity
					velocity.x = (hit.distance - SKIN_WIDTH) * directionX;

					//change rayLength to prevent missed collisions
					rayLength = hit.distance;

					//if already on a slope
					if (collisions.climbingSlope) {
						velocity.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x);
					}

					//set collisionInfo variables
					collisions.left = directionX == -1;
					collisions.right = directionX == 1;
					collisions.horizontalTag = hit.transform.tag;
				}
			}
		}
	}

	/// <summary>
	/// Checks Vertical collisions and modifies the velocity if needed.
	/// </summary>
	/// <param name="velocity">The velocity of the controller.</param>
	void VerticalCollisions(ref Vector2 velocity) {
		//get direction of movement
		float directionY = Mathf.Sign(velocity.y);

		//get the raylength, based on how fast you are moving + skinwidth
		float rayLength = Mathf.Abs(velocity.y) + SKIN_WIDTH;

		//cast rays
		for (int i = 0; i < verticalRayCount; i++) {
			//decide raycast origin
			Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
			rayOrigin += Vector2.right * (verticalRaySpacing * i + velocity.x);

			//cast the ray
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

			//draw raycast gizmo for testing
			Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.red);

			if (hit) {
				//check if through object
				if (hit.collider.tag == "Through") {
					//if moving up or inside object
					if (directionY == 1 || hit.distance == 0) {
						//skip collision code for this ray
						continue;
					}

					//check fall thru
					if (collisions.fallingThroughPlatform) {
						//skip collisions
						continue;
					}

					//if pressing down
					if (playerInput.y == -1) {
						//set fall thru
						collisions.fallingThroughPlatform = true;

						//invoke reset
						Invoke("ResetFallThrough", 0.5f);

						//skip code for this ray
						continue;
					}
				}

				//assign new velocity
				velocity.y = (hit.distance - SKIN_WIDTH) * directionY;

				//change rayLength to prevent missed collisions
				rayLength = hit.distance;

				//if climbing a slope
				if (collisions.climbingSlope) {
					velocity.x = velocity.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(velocity.x);
				}

				//set collisionInfo variables
				collisions.below = directionY == -1;
				collisions.above = directionY == 1;
				collisions.verticalTag = hit.transform.tag;
			}
		}

		//check for new slopes and adjust if found
		if (collisions.climbingSlope) {
			float directionX = Mathf.Sign(velocity.x);
			rayLength = Mathf.Abs(velocity.x) + SKIN_WIDTH;
			Vector2 rayOrigin = ((directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight) +
			                    Vector2.up * velocity.y;
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

			if (hit) {
				float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
				if (slopeAngle != collisions.slopeAngle) {
					velocity.x = (hit.distance - SKIN_WIDTH) * directionX;
					collisions.slopeAngle = slopeAngle;
					collisions.slopeNormal = hit.normal;
				}
			}
		}
	}

	/// <summary>
	/// Calculates the velocity of ascending a slope.
	/// </summary>
	/// <param name="velocity">The velocity of the controller.</param>
	/// <param name="slopeAngle">The angle of the slope.</param>
	void ClimbSlope(ref Vector2 velocity, float slopeAngle, Vector2 slopeNormal) {
		float moveDistance = Mathf.Abs(velocity.x);
		float climbVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

		if (velocity.y <= climbVelocityY) {
			velocity.y = climbVelocityY;
			velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);
			collisions.below = true;
			collisions.climbingSlope = true;
			collisions.slopeAngle = slopeAngle;
			collisions.slopeNormal = slopeNormal;
		}
	}

	/// <summary>
	/// Calculates the velocity of descending on a slope.
	/// </summary>
	/// <param name="velocity">The velocity.</param>
	void DescendSlope(ref Vector2 velocity) {
		//cast ray to check for slope on left side
		RaycastHit2D maxSlopeHitLeft = Physics2D.Raycast(raycastOrigins.bottomLeft,Vector2.down,Mathf.Abs(velocity.y) + SKIN_WIDTH,collisionMask);

		//cast ray to check for slope on right side
		RaycastHit2D maxSlopeHitRight = Physics2D.Raycast(raycastOrigins.bottomRight,Vector2.down,Mathf.Abs(velocity.y) + SKIN_WIDTH,collisionMask);

		//exclusive or of right and left. evaluates true only if 1 of them is true and the other is false
		if (maxSlopeHitLeft ^ maxSlopeHitRight) {
			SlideDownMaxSlope(maxSlopeHitLeft, ref velocity);
			SlideDownMaxSlope(maxSlopeHitRight, ref velocity);
		}

		//if sliding down max slope manual calculations are not needed
		if (!collisions.slidingDownMaxSlope) {
			float directionX = Mathf.Sign(velocity.x);
			Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomRight : raycastOrigins.bottomLeft;
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, -Vector2.up, Mathf.Infinity, collisionMask);

			if (hit) {
				float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

				//if there a slope below you?
				if (slopeAngle != 0 && slopeAngle <= maxSlopeAngle) {
					//are you facing the same direction as the slope?
					if (Mathf.Sign(hit.normal.x) == directionX) {
						//are you close enough for the slope to matter?
						if (hit.distance - SKIN_WIDTH <= Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x)) {
							float moveDistance = Mathf.Abs(velocity.x);
							float descendVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
							velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);
							velocity.y -= descendVelocityY;

							collisions.slopeAngle = slopeAngle;
							collisions.descendingSlope = true;
							collisions.below = true;
							collisions.slopeNormal = hit.normal;
						}
					}
				}
			}
		}
	}

	void SlideDownMaxSlope(RaycastHit2D hit, ref Vector2 velocity) {
		if (hit) {
			float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
			if (slopeAngle > maxSlopeAngle) {
				velocity.x = Mathf.Sign(hit.normal.x) * (Mathf.Abs(velocity.y) - hit.distance) / Mathf.Tan(slopeAngle * Mathf.Deg2Rad);

				//if input in same direction as slope
				if (Mathf.Sign(playerInput.x) == Mathf.Sign(hit.normal.x)) {
					velocity.x *= 2;
				}

				collisions.slopeAngle = slopeAngle;
				collisions.slidingDownMaxSlope = true;
				collisions.slopeNormal = hit.normal;
			}
		}
	}

	void ResetFallThrough() {
		collisions.fallingThroughPlatform = false;
	}

	/// <summary>
	/// Holds all of the controllers collision info.
	/// </summary>
	public class CollisionInfo {

		//directional collision
		public bool above, below;
		public bool left, right;

		//slope collisions
		public bool climbingSlope, descendingSlope;

		//this is for automatic slide when on max slope
		public bool slidingDownMaxSlope;

		//slope angle and the angle of the previous frame, used for changing slopes
		public float slopeAngle, slopeAngleOld;
		public Vector2 slopeNormal;

		//previous velocity, used for special cases
		public Vector2 velocityOld;
		/// <summary>
		/// The face dir of the controller, returns -1 or 1
		/// </summary>
		public int faceDir;

		public bool fallingThroughPlatform;

		public string horizontalTag, verticalTag;

		/// <summary>
		/// Resets this instance.
		/// </summary>
		public void Reset() {
			above = below = false;
			left = right = false;
			climbingSlope = descendingSlope = slidingDownMaxSlope = false;
			slopeNormal = Vector2.zero;

			slopeAngleOld = slopeAngle;
			slopeAngle = 0;

			horizontalTag = verticalTag = "";
		}
	}
}