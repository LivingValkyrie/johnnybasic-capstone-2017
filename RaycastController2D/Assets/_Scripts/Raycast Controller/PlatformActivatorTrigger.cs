﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PlatformActivatorTrigger
/// </summary>
public class PlatformActivatorTrigger : MonoBehaviour {
	#region Fields

	public PlatformController platformToActivate;
	public bool turnOffOnExit = false;

	#endregion

	public void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject == GameController.instance.ActivePlayerGameObject) {
			platformToActivate.canMove = true;
		}
	}

	public void OnTriggerExit2D(Collider2D other) {
		if (other.gameObject == GameController.instance.ActivePlayerGameObject) {
			if (turnOffOnExit) {
				platformToActivate.canMove = false;
			}
		}
	}
}