﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: CameraInputToggle
/// </summary>
public class CameraInputToggle : MonoBehaviour {
	#region Fields

	[Tooltip("This controls whether input is allowed while inside the trigger or not.")]
	public bool allowOnEnter = true;
	[Tooltip( "This allows the input changes to stay even after leaving the trigger." )]
	public bool keepChanges = false;

	#endregion

	/// <summary>
	/// Called when [trigger enter2 d]. locks camera
	/// </summary>
	/// <param name="other">The other.</param>
	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject == GameController.instance.ActivePlayerGameObject) {
			Camera.main.GetComponent<BoundingBoxFollowCamera>().allowInput = allowOnEnter;
		}
	}

	/// <summary>
	/// Called when [trigger exit2 d]. unlocks camera
	/// </summary>
	/// <param name="other">The other.</param>
	void OnTriggerExit2D(Collider2D other) {
		if (!keepChanges) {
			if (other.gameObject == GameController.instance.ActivePlayerGameObject) {
				Camera.main.GetComponent<BoundingBoxFollowCamera>().allowInput = !allowOnEnter;
			}
		}
	}
}