﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: CameraZoomTrigger
/// </summary>
public class CameraZoomTrigger : MonoBehaviour {
	#region Fields

	//the time it takes to zoom fully
	public float zoomTime;

	//the level of zoom 
	public float zoomTarget;

	#endregion

	public void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject == GameController.instance.ActivePlayerGameObject) {
			Camera.main.GetComponent<BoundingBoxFollowCamera>().Zoom(zoomTarget, zoomTime);
		}
	}

	public void OnTriggerExit2D(Collider2D other) {
		if (other.gameObject == GameController.instance.ActivePlayerGameObject) {
			Camera.main.GetComponent<BoundingBoxFollowCamera>().Zoom(zoomTarget, zoomTime, false);
		}
	}

}