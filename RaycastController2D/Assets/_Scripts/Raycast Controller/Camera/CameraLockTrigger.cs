﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: CameraLockTrigger
/// </summary>
public class CameraLockTrigger : MonoBehaviour {
	#region Fields

	#endregion

	/// <summary>
	/// Called when [trigger enter2 d]. locks camera
	/// </summary>
	/// <param name="other">The other.</param>
	void OnTriggerEnter2D( Collider2D other ) {
		if ( other.gameObject == GameController.instance.ActivePlayerGameObject) {
			Camera.main.GetComponent<BoundingBoxFollowCamera>().isLocked = true;
		}
	}

	/// <summary>
	/// Called when [trigger exit2 d]. unlocks camera
	/// </summary>
	/// <param name="other">The other.</param>
	void OnTriggerExit2D( Collider2D other ) {
		if ( other.gameObject == GameController.instance.ActivePlayerGameObject ) {
			Camera.main.GetComponent<BoundingBoxFollowCamera>().isLocked = false;
		}
	}
}