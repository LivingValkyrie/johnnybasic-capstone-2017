﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: BoundingBoxFollowCamera 
/// </summary>
[RequireComponent(typeof(Camera))]
public class BoundingBoxFollowCamera : MonoBehaviour {
	#region Fields

	public ControllerInput target;
	public Vector2 boundingBoxSize;
	public float lookAheadDistanceX, lookSmoothTimeX, verticalSmoothTime;

	public float verticalOffset;

	public bool isLocked = false;
	public float maxMoveX;
	public CameraBounds bounds;

	public bool allowInput;
	//public bool zoomByVelocity = true;
	//public float velMinThreshold, velMaxThreshold;
	//public float minZoom, maxZoom;

	BoundingBox boundingBox;

	float currLookAheadX, targetLookAheadX, lookAheadDirectionX, smoothLookVelocityX, smoothVelocityY;
	bool lookAheadStopped;

	float targetZoom, startingZoom, delay;

	//input fields
	Vector2 offset;
	bool hasMoved = true;
	public float inputSpeed = 2;

	#endregion

	/// <summary>
	/// Starts this instance.
	/// </summary>
	void Start() {
		//init bounding box size and position
		boundingBox = new BoundingBox(target.Controller.myCollider.bounds, boundingBoxSize);

		startingZoom = Camera.main.orthographicSize;
	}

	/// <summary>
	/// Changes the target.
	/// </summary>
	/// <param name="newTarget">The new target.</param>
	public void ChangeTarget(ControllerInput newTarget) {
		target = newTarget;
		boundingBox = new BoundingBox(target.Controller.myCollider.bounds, boundingBoxSize);
	}

	/// <summary>
	/// updates camera position is unlocked and within bounds
	/// </summary>
	void LateUpdate() {
		if (isLocked /*|| GameController.instance.GamePaused*/) {
			return;
		}
		
		//use an x check aswell as y check for to allow movment in each seperate direction

		if (allowInput && target.Velocity.y == 0) {
			offset = new Vector2(0, Input.GetAxis("Vertical") * Time.deltaTime * inputSpeed);
			boundingBox.Update(offset);
		} else {
			hasMoved = true;
		}

		if (hasMoved) {
			//update bounding box
			boundingBox.Update(target.Controller.myCollider.bounds);
		}

		Vector2 boundingPosition = boundingBox.center + Vector2.up * verticalOffset;

		//if moving on x
		if (boundingBox.velocity.x != 0) {
			//get direction of movement
			lookAheadDirectionX = Mathf.Sign(boundingBox.velocity.x);

			//if moving the same way as player input
			if (Mathf.Sign(target.Controller.playerInput.x) == Mathf.Sign(boundingBox.velocity.x) && target.Controller.playerInput.x != 0) {
				lookAheadStopped = false;
				targetLookAheadX = lookAheadDirectionX * lookAheadDistanceX;
			} else {
				//ran for 1 frame after movement has stopped
				if (!lookAheadStopped) {
					lookAheadStopped = true;
					targetLookAheadX = currLookAheadX + (lookAheadDirectionX * lookAheadDistanceX - currLookAheadX) / 4;
				}
			}
		}

		//smooth x
		currLookAheadX = Mathf.SmoothDamp(currLookAheadX, targetLookAheadX, ref smoothLookVelocityX, lookSmoothTimeX);

		//smooth y
		boundingPosition.y = Mathf.SmoothDamp(transform.position.y, boundingPosition.y, ref smoothVelocityY, verticalSmoothTime);

		//update position with smoothed value
		boundingPosition += Vector2.right * currLookAheadX;

		//move camera
		Vector3 newPos = (Vector3) boundingPosition + Vector3.forward * -10;

		//check x, allow x to move thru
		if (!bounds.ContainsPointX(newPos)) {
			//modify x to not change
			newPos.x = transform.position.x;
		}

		//check y, allow y to move thru
		if (!bounds.ContainsPointY(newPos)) {
			//modify y to not change
			newPos.y = transform.position.y;
		}

		//do actual move with new vector
		if (bounds.ContainsPoint(newPos)) {
			transform.position = newPos;
		}

		//if ( zoomByVelocity ) {
		//	print( "zoom " + Mathf.Lerp( minZoom, maxZoom, ( Mathf.Lerp( velMinThreshold, velMaxThreshold, target.Velocity.magnitude ) ) ) );
		//	print( "vel lerp " + Mathf.Lerp( velMinThreshold, velMaxThreshold, target.Velocity.magnitude ) );
		//	print( "vel lerp normalized " + Mathf.Lerp( velMinThreshold, velMaxThreshold, target.Velocity.magnitude ).Remap( velMinThreshold, velMaxThreshold, 0, 1 ) );

		//	print( "vel mag " + target.Velocity.magnitude );
		//	Camera.main.orthographicSize = Mathf.Lerp( minZoom, maxZoom, ( Mathf.Lerp( velMinThreshold, velMaxThreshold, target.Velocity.magnitude ).Remap( minZoom, maxZoom, 0, 1 ) ) );
		//}
	}

	

	/// <summary>
	/// Zooms to the specified target zoom level.
	/// </summary>
	/// <param name="targetZoomLevel">The target zoom level.</param>
	/// <param name="zoomTime">The zoom time.</param>
	/// <param name="entering">if set to <c>true</c> [entering].</param>
	public void Zoom(float targetZoomLevel, float zoomTime, bool entering = true) {
		if (entering) {
			startingZoom = Camera.main.orthographicSize;
			targetZoom = targetZoomLevel;
		} else {
			targetZoom = startingZoom;
			startingZoom = Camera.main.orthographicSize;
		}

		delay = zoomTime;

		StopCoroutine(ZoomOverTime());
		StartCoroutine(ZoomOverTime());
	}

	/// <summary>
	/// Zooms over time.
	/// </summary>
	/// <returns></returns>
	public IEnumerator ZoomOverTime() {
		yield return new WaitForEndOfFrame();

		float timeElapsed = 0;
		while (timeElapsed < delay) {
			Camera.main.orthographicSize = Mathf.Lerp(startingZoom, targetZoom, (timeElapsed / delay));
			timeElapsed += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

		yield return null;
	}

	/// <summary>
	/// Called when [draw gizmos].
	/// </summary>
	void OnDrawGizmos() {
		if (Application.isEditor && !Application.isPlaying) {
			if (target) {
				boundingBox = new BoundingBox(target.GetComponent<Collider2D>().bounds, boundingBoxSize);
			}
		}

		Gizmos.color = new Color(0, 1, 0, 0.5f);
		Gizmos.DrawCube(boundingBox.center, boundingBoxSize);

		Gizmos.color = Color.blue;

		bounds.DrawGizmo();

		CameraBounds outerBounds = new CameraBounds();
		outerBounds.min = bounds.min -
		                  new Vector2(GetComponent<Camera>().orthographicSize * 1.6f, GetComponent<Camera>().orthographicSize);
		outerBounds.max = bounds.max +
		                  new Vector2(GetComponent<Camera>().orthographicSize * 1.6f, GetComponent<Camera>().orthographicSize);

		Gizmos.color = Color.red;
		outerBounds.DrawGizmo();

		//transform.position = new Vector3(target.transform.position.x, target.transform.position.y, -10);
	}

}

public static class ExtensionMethods {

	public static float Remap( this float value, float baseMin, float baseMax, float goalMin, float goalMax ) {
		return ( value - baseMin ) / ( baseMax - baseMin ) * ( goalMax - goalMin ) + goalMin;
	}

	public static bool ContainsParam( this Animator _Anim, string _ParamName ) {
		foreach ( AnimatorControllerParameter param in _Anim.parameters ) {
			if ( param.name == _ParamName )
				return true;
		}
		return false;
	}

	public static float Truncate( this float value, int digits ) {
		double mult = Math.Pow( 10.0, digits );
		double result = Math.Truncate( mult * value ) / mult;
		return (float)result;
	}

}

[System.Serializable]
public struct CameraBounds {
	public Vector2 min, max;

	public Vector2 BottomLeft {
		get { return new Vector3(min.x, min.y, 0); }
	}

	public Vector2 BottomRight {
		get { return new Vector3(max.x, min.y, 0); }
	}

	public Vector2 TopLeft {
		get { return new Vector3(min.x, max.y, 0); }
	}

	public Vector2 TopRight {
		get { return new Vector3(max.x, max.y, 0); }
	}

	/// <summary>
	/// whether the point is contained within. Z axis is ignored
	/// </summary>
	/// <param name="point"></param>
	/// <returns></returns>
	public bool ContainsPoint(Vector3 point) {
		return point.x < max.x && point.x > min.x && point.y > min.y && point.y < max.y;
	}

	public bool ContainsPointX(Vector3 point) {
		return point.x < max.x && point.x > min.x;
	}

	public bool ContainsPointY(Vector3 point) {
		return point.y > min.y && point.y < max.y;
	}

	/// <summary>
	/// Draws the gizmo.
	/// </summary>
	public void DrawGizmo() {
		Gizmos.DrawLine(TopRight, TopLeft);
		Gizmos.DrawLine(TopRight, BottomRight);
		Gizmos.DrawLine(BottomLeft, TopLeft);
		Gizmos.DrawLine(BottomLeft, BottomRight);
	}
}

struct BoundingBox {
	//todo add check in bounds method

	public Vector2 center;
	public Vector2 velocity;
	float left, right, top, bottom;

	public BoundingBox(Bounds targetBounds, Vector2 size) {
		left = targetBounds.center.x - size.x / 2;
		right = targetBounds.center.x + size.x / 2;
		bottom = targetBounds.min.y;
		top = targetBounds.min.y + size.y;

		velocity = Vector2.zero;
		center = new Vector2((left + right) / 2, (top + bottom) / 2);
	}

	public void Update(Vector2 targetCenter) {
		//shift value for moving
		float shiftX = targetCenter.x;

		//apply shift value, this moves both sides equally
		left += shiftX;
		right += shiftX;

		//same as X
		float shiftY = targetCenter.y;
		top += shiftY;
		bottom += shiftY;

		//recalculate center and velocity
		center = new Vector2((left + right) / 2, (top + bottom) / 2);
		velocity = new Vector2(shiftX, shiftY);
	}

	public void Update(Bounds targetBounds) {
		//shift value for moving
		float shiftX = 0;
		if (targetBounds.min.x < left) {
			//if bounds is to my left shift by the difference
			shiftX = targetBounds.min.x - left;
		} else if (targetBounds.max.x > right) {
			//if bounds is to my right shift by the difference
			shiftX = targetBounds.max.x - right;
		}

		//apply shift value, this moves both sides equally
		left += shiftX;
		right += shiftX;

		//same as X
		float shiftY = 0;
		if (targetBounds.min.y < bottom) {
			shiftY = targetBounds.min.y - bottom;
		} else if (targetBounds.max.y > top) {
			shiftY = targetBounds.max.y - top;
		}

		//same as X
		top += shiftY;
		bottom += shiftY;

		//recalculate center and velocity
		center = new Vector2((left + right) / 2, (top + bottom) / 2);
		velocity = new Vector2(shiftX, shiftY);
	}
}