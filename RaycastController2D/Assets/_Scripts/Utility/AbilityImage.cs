﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: AbilityImage
/// </summary>
public class AbilityImage : MonoBehaviour {
	#region Fields

	public Image icon;
	public Image coolDownIcon;

	[HideInInspector]
	public PlayerAbility ability;

	#endregion

	public void UpdateAbility(PlayerAbility newAbility) {
		//print("New Ability" + name);
		coolDownIcon.sprite = newAbility.icon;
		icon.sprite = newAbility.iconOnCooldown;
		ability = newAbility;
		//print( "New Ability finished" + name );

	}

}