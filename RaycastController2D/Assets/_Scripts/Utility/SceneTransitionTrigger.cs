﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SceneTransitionTrigger is a trigger volume script used to show scene transistion dialogs for level progression
/// </summary>
public class SceneTransitionTrigger : SimpleSwitch {
	#region Fields

	//the scene that will be loaded when accepting the prompt
	public string levelToLoad;
	public int spawnPointToLoad;
	public int mySpawnpointID;
	public GameObject spawnPointObject;
	public bool showTransitionDialog = false;

	#endregion

	/// <summary>
	/// Called on [trigger enter2d]. shows transistion dialog
	/// </summary>
	/// <param name="other">The other.</param>
	public void OnTriggerEnter2D(Collider2D other) {
		//if other is player
		if (other.gameObject == GameController.instance.ActivePlayerGameObject) {
			//if 1 player is ko
			if (GameController.instance.progress.hasCompanion) {
				if (!GameController.instance.InactivePlayer.stats.isKOd ||
				    (GameController.instance.InactivePlayer.stats.isKOd && GameController.instance.progress.isCarrying)) {
					if (showTransitionDialog) {
						GameController.instance.ShowSceneTransitionPrompt(levelToLoad, spawnPointToLoad);
					} else {
						GameController.instance.PauseGame(false);
						GameController.instance.progress.spawnPointId = spawnPointToLoad;
						GameController.instance.LoadLevel(levelToLoad);
					}
				} else {
					print("Need to be carrying ko'd partner");
				}
			} else {
				if (showTransitionDialog) {
					GameController.instance.ShowSceneTransitionPrompt(levelToLoad, spawnPointToLoad);
				} else {
					GameController.instance.PauseGame(false);
					GameController.instance.progress.spawnPointId = spawnPointToLoad;
					GameController.instance.LoadLevel(levelToLoad);
				}
			}
		}
		//print($"obj: {other.gameObject.name}");
	}

	public override void OnActivate() {
		//todo needs to check if in base or not
		if (GameController.instance.progress.hasCompanion && !SceneManager.GetActiveScene().name.Contains( "Base" ) ) {
			if (!GameController.instance.InactivePlayer.stats.isKOd ||
			    (GameController.instance.InactivePlayer.stats.isKOd && GameController.instance.progress.isCarrying)) {
				GameController.instance.ShowSceneTransitionPrompt(levelToLoad, spawnPointToLoad);
			} else {
				print("Need to be carrying ko'd partner");
			}
		} else {
			GameController.instance.ShowSceneTransitionPrompt(levelToLoad, spawnPointToLoad);
		}
	}

	public override void OnDeactivate() {
		Toggle(activator);
	}
}