﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: HeadsUpDisplay
/// </summary>
public class HeadsUpDisplay : MonoBehaviour {
	#region Fields

	public AbilityImage first, second, third;
	public Slider healthSliderPlayer;

	public AbilityImage limitPlayer;

	//new
	public Slider healthSliderCompanion;
	public AbilityImage limitCompanion;
	public AbilityImage potion;
	public Text potionCount;
	public Image KOImage;

	public Image portraitPlayer, portraitCompanion;
	public Text consumableCount;
	public Color textColor;

	bool initHasRan;

	#endregion

	void Start() {
		GameController.instance.SwapEventHandler += Init;
	}

	public void Init() {
		first.UpdateAbility(GameController.instance.ActivePlayer.GetComponent<CharacterStats>().DataAsPlayer.abilityOne);
		second.UpdateAbility(GameController.instance.ActivePlayer.GetComponent<CharacterStats>().DataAsPlayer.abilityTwo);
		third.UpdateAbility(GameController.instance.ActivePlayer.GetComponent<CharacterStats>().DataAsPlayer.abilityThree);
		potion.UpdateAbility(GameController.instance.potionAbility);

		healthSliderPlayer.maxValue = GameController.instance.ActivePlayer.GetComponent<CharacterStats>().data.maxHealth;
		healthSliderPlayer.minValue = 0;

		limitPlayer.UpdateAbility(GameController.instance.ActivePlayer.GetComponent<CharacterStats>().DataAsPlayer.limitBreak);
		portraitPlayer.sprite = GameController.instance.ActivePlayer.stats.DataAsPlayer.portrait;

		consumableCount.text = GameController.instance.ActivePlayer.stats.DataAsPlayer.GetConsumableCount().ToString();

		if (int.Parse(consumableCount.text) > 0) {
			consumableCount.color = textColor;

		} else {
			consumableCount.color = Color.red;
		}

		KOImage.gameObject.SetActive(false);

		if (GameController.instance.progress.hasCompanion) {
			if (SceneManager.GetActiveScene().name.Contains("Base")) {
				healthSliderCompanion.maxValue = 100;
				portraitCompanion.sprite =
					GameController.instance.GetPlayerData(GameController.instance.progress.playerTwoName).inactivePortrait;
				limitCompanion.UpdateAbility(GameController.instance.GetPlayerData(GameController.instance.progress.playerTwoName).limitBreak);
			} else {
				healthSliderCompanion.maxValue = GameController.instance.InactivePlayer.GetComponent<CharacterStats>().data.maxHealth;
				portraitCompanion.sprite = GameController.instance.InactivePlayer.stats.DataAsPlayer.inactivePortrait;
				limitCompanion.UpdateAbility(GameController.instance.InactivePlayer.stats.DataAsPlayer.limitBreak);
			}

			healthSliderCompanion.value = healthSliderCompanion.maxValue;
			healthSliderCompanion.minValue = 0;

			healthSliderCompanion.gameObject.SetActive(true);

			limitCompanion.gameObject.SetActive(true);

			portraitCompanion.gameObject.SetActive(true);
		} else {
			healthSliderCompanion.gameObject.SetActive(false);
			limitCompanion.gameObject.SetActive(false);
			portraitCompanion.gameObject.SetActive(false);
			KOImage.gameObject.SetActive(false);
		}

		initHasRan = true;
	}

	void Update() {
		//update ability images
		if (!initHasRan) {
			return;
		}

		first.coolDownIcon.fillAmount = ((Time.time - GameController.instance.ActivePlayer.LastCastAbilityOne) / first.ability.Cooldown);
		second.coolDownIcon.fillAmount = ((Time.time - GameController.instance.ActivePlayer.LastCastAbilityTwo) /
		                                  second.ability.Cooldown);
		third.coolDownIcon.fillAmount = ((Time.time - GameController.instance.ActivePlayer.LastCastAbilityThree) /
		                                 third.ability.Cooldown);

		//update health stuff
		healthSliderPlayer.value = GameController.instance.ActivePlayer.GetComponent<CharacterStats>().currHealth;
		limitPlayer.coolDownIcon.fillAmount = GameController.instance.ActivePlayer.LimitAsPercent;

		potionCount.text = $"{GameController.instance.progress.currPotions} / {GameController.instance.progress.maxPotions}";
		if (GameController.instance.progress.currPotions <= 0) {
			potionCount.color = Color.red;
		} else {
			potionCount.color = textColor;
		}

		potion.coolDownIcon.fillAmount = ((Time.time - GameController.instance.ActivePlayer.LastCastPotion) /
		                                  GameController.instance.potionAbility.Cooldown);

		consumableCount.text = GameController.instance.ActivePlayer.stats.DataAsPlayer.GetConsumableCount().ToString();

		if (int.Parse(consumableCount.text) > 0) {
			consumableCount.color = textColor;
		} else {
			consumableCount.color = Color.red;
		}

		if (!SceneManager.GetActiveScene().name.Contains("Base")) {
			if (GameController.instance.progress.hasCompanion) {
				healthSliderCompanion.value = GameController.instance.InactivePlayer.stats.currHealth;
				if (GameController.instance.InactivePlayer.stats.isKOd) {
					KOImage.gameObject.SetActive(true);
				} else {
					KOImage.gameObject.SetActive(false);
					limitCompanion.coolDownIcon.fillAmount = GameController.instance.InactivePlayer.LimitAsPercent;
				}
			}
		}
	}
}