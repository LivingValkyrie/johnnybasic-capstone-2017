﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SimpleSwitch is a class meant to be a buiding block for any kind of on/off switch type object.
/// </summary>
public abstract class SimpleSwitch : MonoBehaviour {
	#region Fields

	[Header("Interactivity")]
	[Tooltip("Wether or not the switch is active.")]
	public bool isActive;
	public bool interactible = true;
	public Sprite interactionSprite;
	protected GameObject activator;

	#endregion

	/// <summary>
	/// Toggles this Switch and calls the appropriate method
	/// </summary>
	public virtual void Toggle(GameObject activatingObject = null) {
		this.activator = activatingObject;
		isActive = !isActive;
		if (isActive) {
			OnActivate();
		} else {
			OnDeactivate();
		}
	}

	/// <summary>
	/// Called when [isActive].
	/// </summary>
	public abstract void OnActivate();

	/// <summary>
	/// Called when ![isActive].
	/// </summary>
	public abstract void OnDeactivate();
}