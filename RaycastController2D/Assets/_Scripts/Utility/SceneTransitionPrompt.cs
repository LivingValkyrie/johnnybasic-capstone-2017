﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SceneTransitionPrompt
/// </summary>
public class SceneTransitionPrompt : MonoBehaviour {
	#region Fields

	//gotten from scene transistion trigger
	[HideInInspector]
	public string levelToLoad;
	[HideInInspector]
	public int spawnPointID;

	public Text myText;

	#endregion

	void OnEnable() {
		myText.text = "Go To " + levelToLoad;
	}

	/// <summary>
	/// Call with Accept btton in dialog
	/// </summary>
	public void Accept() {
		GameController.instance.progress.spawnPointId = spawnPointID;
		GameController.instance.LoadLevel(levelToLoad);
	}

	/// <summary>
	/// Call with Decline button in dialog
	/// </summary>
	public void Decline() {
		GameController.instance.HideSceneTransitionPrompt();
	}
}