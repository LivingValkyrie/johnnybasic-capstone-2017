﻿using System;
using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: CharacterStats is a component that allows for entities to have stats like health and be able to die.
/// </summary>
[DisallowMultipleComponent]
public class CharacterStats : MonoBehaviour {
	#region Fields

	//the data asset with this entities stats
	public CharacterData data;
	public float minimumDamage = 1;

	//the objects current health
	[HideInInspector]
	public float currHealth = 1;

	//whether the object is KOd or not
	[HideInInspector]
	public bool isKOd = false;

	//the OnDeathEvent and signature. on death is called when currHealth reachs 0 or below
	public delegate void DeathEvent();
	public event DeathEvent OnDeathEventHandler;

	public bool initHealth = true;

	bool invulnerable = false;

	public void SetInvulnerable() {
		invulnerable = true;
		Invoke("ReturnToNormal", 0.5f);
	}

	void ReturnToNormal() {
		invulnerable = false;
		Destroy(GetComponent<DamageTrigger>());
	}

	#region data cast properties

	//data returned casted as playerData
	public PlayerData DataAsPlayer {
		get { return data as PlayerData; }
	}

	//data returned as enemyData
	public EnemyData DataAsEnemy {
		get { return data as EnemyData; }
	}

	#endregion

	#endregion

	/// <summary>
	/// Starts this instance. calls data.init and sets currHealth to maxHealthIncrease
	/// </summary>
	void Start() {
		if (initHealth) {
			//print($"setting health to data max {name}");
			currHealth = data.maxHealth;
		}
		data.Init();
	}

	/// <summary>
	/// Takes damage and called death event if needed
	/// </summary>
	/// <param name="dmgAmount">The DMG amount.</param>
	/// <param name="dmgType"></param>
	public void TakeDamage(float dmgAmount, DamageType dmgType = DamageType.None) {
		//check type of damage and process.
		switch (dmgType) {
			case DamageType.Fire:
				dmgAmount *= ((int) data.fireRes * .1f);
				goto case DamageType.None;
			case DamageType.Ice:
				dmgAmount *= ((int) data.iceRes * .1f);
				goto case DamageType.None;
			case DamageType.Shock:
				dmgAmount *= ((int) data.shockRes * .1f);
				goto case DamageType.None;
			case DamageType.Acid:
				dmgAmount *= ((int) data.acidRes * .1f);
				goto case DamageType.None;
			case DamageType.None:

				//if player object then add armorIncrease to calculation
				if (data is PlayerData) {
					dmgAmount -= ((PlayerData) data).armor + ((PlayerData) data).defense;
					dmgAmount = Mathf.Max(dmgAmount, minimumDamage);
				}
				if (!invulnerable) {
					currHealth -= dmgAmount;
					GameController.instance.PlaySoundEffect(data.hurtSound);
				}

				break;
			case DamageType.Heal:

				//increase health by dmgAmount instead of decrease
				currHealth += dmgAmount;
				GameController.instance.PlaySoundEffect(data.healingSound);

				break;
			default:
				throw new ArgumentOutOfRangeException("dmgType", dmgType, null);
		}

		GameController.instance.CreateFloatingText(dmgAmount.ToString(), transform, dmgType);

		//print(name + " hp: " + currHealth);

		//limit health to maxHealthIncrease and call death event if needed
		if (currHealth >= data.maxHealth) {
			currHealth = data.maxHealth;
		} else if (currHealth <= 0) {
			//print("less than 0");
			//@Mike
			gameObject.tag = "Dead";

			//@end Mike

			//print(name +" death length " + OnDeathEventHandler.GetInvocationList().Length);

			GameController.instance.PlaySoundEffect(data.deathSound);
			if (OnDeathEventHandler != null) {
				//print("calling death");
				OnDeathEventHandler();
			}
		} else {
			//damage that didnt kill, do knockback
			if (dmgType != DamageType.Heal) {
				KnockBack(dmgAmount);

				//drop carried obj if carry
				if (GameController.instance.progress.isCarrying) {
					if (GetComponent<Player>()) {
						GetComponent<Player>().DropCarried();
					}
				}
			}

			//if player then increase limit gauge
			if (GetComponent<Player>()) {
				GetComponent<Player>().Limit += dmgAmount;
			}
		}
	}

	/// <summary>
	/// Very basic knockback. will be improved later
	/// </summary>
	/// <param name="force">The force.</param>
	void KnockBack(float force) {
		force /= 4;
		float dir = -GetComponent<ControllerInput>().Controller.Facing;

		GetComponent<ControllerInput>().Controller.Move(new Vector2(dir * force, force) / data.weight);
	}

	/// <summary>
	/// Called when [draw gizmos].
	/// </summary>
	void OnDrawGizmos() {
#if UNITY_EDITOR
		UnityEditor.Handles.Label(transform.position + new Vector3(0, 1, 0), "HP: " + currHealth);
#endif
	}

	#region resistances

	public enum ResistLevel : int {
		Low = 20,
		Med = 10,
		High = 5
	}

	public enum DamageType {
		Fire,
		Ice,
		Shock,
		Acid,
		None,
		Heal
	}

	#endregion
}