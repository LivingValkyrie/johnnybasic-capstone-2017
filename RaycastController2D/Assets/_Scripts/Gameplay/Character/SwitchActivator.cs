﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SwitchActivator allows for activating simple switches. needs heavy modification to work with Johnny Basic.
/// </summary>
[DisallowMultipleComponent]
public class SwitchActivator : MonoBehaviour {
	#region Fields

	Controller2D c2d;
	public SpriteRenderer interactionSpriteRenderer;
	public Sprite defaultSprite;

	public bool debug = false;

	#endregion

	/// <summary>
	/// defaults the cursor color
	/// </summary>
	void Start() {
		c2d = GetComponent<Controller2D>();
		interactionSpriteRenderer.enabled = false;
	}

	/// <summary>
	/// checks for input to activte switches and updates the cursor outline if needed
	/// </summary>
	void Update() {
		if (GameController.instance.GamePaused || gameObject != GameController.instance.ActivePlayerGameObject) {
			interactionSpriteRenderer.enabled = false;
			return;
		}

		RaycastHit hit;
		//cast dual rays
		if (CastDualRays(out hit, debug)) {
			//if hits a simple switch
			if (hit.transform.GetComponentInParent<SimpleSwitch>().interactible) {
				//shows interaction sprite, sets to default if switch does not have upgradeRecipeOne
				interactionSpriteRenderer.sprite = hit.transform.GetComponentInParent<SimpleSwitch>().interactionSprite
												 ? hit.transform.GetComponentInParent<SimpleSwitch>().interactionSprite
												 : defaultSprite;

				interactionSpriteRenderer.enabled = true;

				//if interact pressed then it will call toggle on the switch
				if (Input.GetButtonDown("Interact")) {
					foreach (var VARIABLE in hit.transform.GetComponentsInParent<SimpleSwitch>()) {
						VARIABLE.Toggle(gameObject);
					}
				}
			}
		} else {
			//if no hit then turn off sprite
			interactionSpriteRenderer.enabled = false;
		}
	}

	/// <summary>
	/// Casts a ray on the facing side of the c2d, if the ray fails to hit it will then cast upgradeRecipeOne on the opposite side. 
	/// </summary>
	/// <param name="hit">The hit object.</param>
	/// <param name="debug">if set to <c>true</c> [debug].</param>
	/// <returns>If there was a hit or not</returns>
	bool CastDualRays(out RaycastHit hit, bool debug = false) {
		bool success = false;
		float raycastOffset = c2d.Facing * (c2d.myCollider.bounds.size.x / 2);

		//do a ray from facing side, then non facing side
		//change the ray to be what i need
		Vector3 rayOrigin = transform.position;
		rayOrigin.x += raycastOffset;

		Ray interactionRay = new Ray(rayOrigin, Vector3.forward * 5);

		if (debug) {
			Debug.DrawRay(interactionRay.origin, interactionRay.direction, Color.blue);
		}

		if (Physics.Raycast(interactionRay, out hit)) {
			//print("first cast hit");
			success = true;
		} else {
			//print("first cast missed, trying second");

			rayOrigin.x += (raycastOffset * 2) * -1;
			interactionRay.origin = rayOrigin;

			if (debug) {
				Debug.DrawRay(interactionRay.origin, interactionRay.direction, Color.red);
			}

			if (Physics.Raycast(interactionRay, out hit)) {
				success = true;
			}
		}

		return success;
	}

	/// <summary>
	/// Just here for completeness
	/// </summary>
	/// <param name="debug">if set to <c>true</c> [debug].</param>
	/// <returns></returns>
	RaycastHit2D CastDualRays2D(bool debug = false) {
		float raycastOffset = c2d.Facing * (c2d.myCollider.bounds.size.x / 5);

		//do a ray from facing side, then non facing side
		//change the ray to be what i need
		Vector3 rayOrigin = transform.position;
		rayOrigin.x += raycastOffset;

		Ray interactionRay = new Ray(rayOrigin, Vector3.forward * 2);

		if (debug) {
			Debug.DrawRay(interactionRay.origin, interactionRay.direction, Color.blue);
		}

		RaycastHit2D hit = Physics2D.Raycast(interactionRay.origin, interactionRay.direction);
		if (!hit) {
			//print( "first cast missed, trying second" );

			rayOrigin.x += (raycastOffset * 2) * -1;
			interactionRay.origin = rayOrigin;

			if (debug) {
				Debug.DrawRay(interactionRay.origin, interactionRay.direction, Color.red);
			}

			hit = Physics2D.Raycast(interactionRay.origin, interactionRay.direction);
		}

		return hit;
	}
}