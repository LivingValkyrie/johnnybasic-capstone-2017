﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Player
/// </summary>
[RequireComponent(typeof(PlayerControllerInput), typeof(CharacterStats))]
[DisallowMultipleComponent]
public class Player : MonoBehaviour {
	#region Fields

	PlayerControllerInput playerController;

	[HideInInspector]
	public Vector2 directionalInput;

	[HideInInspector]
	public CharacterStats stats;

	//double tap stuff
	float tapTimerLeft;
	int tapCountLeft;
	float tapTimerRight;
	int tapCountRight;

	//cooldown casting stuff
	float lastCastAttackMain = 0;
	float lastCastAttackSecond = 0;
	float lastCastAbilityOne = 0;
	float lastCastAbilityTwo = 0;
	float lastCastAbilityThree = 0;

	public float Limit {
		get { return limit; }
		set {
			if (value >= stats.DataAsPlayer.limitGaugeSize) {
				limit = stats.DataAsPlayer.limitGaugeSize;
				canLimitBreak = true;
			} else if (value < 0) {
				limit = 0;
				canLimitBreak = false;
			} else {
				limit = value;
				canLimitBreak = false;
			}

			//print(limit + " " + stats.DataAsPlayer.limitGaugeSize);
		}
	}
	public float LimitAsPercent {
		get { return limit / stats.DataAsPlayer.limitGaugeSize; }
	}
	float limit;
	public bool canLimitBreak = false;

	public GameObject carryKoPrefab;
	public GameObject carriedObj;

	#region Cooldown Properties

	public float FaceDir {
		get { return playerController.Controller.collisions.faceDir; }
	}

	public float LastCastAttackMain {
		get { return lastCastAttackMain; }
	}

	public float LastCastAttackSecond {
		get { return lastCastAttackSecond; }
	}

	public float LastCastAbilityOne {
		get { return lastCastAbilityOne; }
	}

	public float LastCastAbilityTwo {
		get { return lastCastAbilityTwo; }

	}

	public float LastCastAbilityThree {
		get { return lastCastAbilityThree; }
	}

	public float LastCastPotion {
		get { return GameController.instance.lastCastPotion; }
	}

	#endregion

	//animation integration
	public Animator animator;

	bool canTakeInput = true;

	public void StopInput( bool stop ) {
		canTakeInput = !stop;
		directionalInput = Vector2.zero;
	}

	#endregion

	void Awake() {
		//print("assigning stats " + name);
		stats = GetComponent<CharacterStats>();
		stats.OnDeathEventHandler += KnockOut;
	}

	void Start() {
		playerController = GetComponent<PlayerControllerInput>();
		GameController.instance.SwapEventHandler += SwapToons;

		playerController.Controller.maxSpeed = stats.data.moveSpeed;

		PlayerData data = stats.data as PlayerData;
		if (data != null) {
			PlayerData pData = data;

			playerController.canWallJump = pData.canWallJump;
			playerController.canAirDash = pData.canAirDash;
			playerController.canMultiJump = pData.canMultiJump;
			playerController.airDashBoost = pData.airDashVelocity;
			playerController.maxJumps = pData.maxMultiJump;
			playerController.Controller.maxSlopeAngle = pData.maxSlopeAngle;
			//print( playerController.Controller.maxSlopeAngle  + "  " + pData.maxSlopeAngle + " " + name + " " + pData.characterName);
		}

		animator = GetComponent<Animator>();
		animator.runtimeAnimatorController = stats.data.animatorController;
	}

	void Update() {
		if (!GameController.instance.GamePaused && canTakeInput ) {
			directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

			//jump button
			if (Input.GetButtonDown("Jump")) {
				playerController.JumpPressed();
			} else if (Input.GetButtonUp("Jump")) {
				playerController.JumpReleased();
			}

			HandleAirDashInput();

			#region Sprinting

			if (Input.GetButton("Sprint")) {
				PlayerData data = stats.data as PlayerData;
				playerController.moveSpeed = data.sprintSpeed;
			}

			if (Input.GetButtonUp("Sprint")) {
				PlayerData data = stats.data as PlayerData;
				playerController.moveSpeed = data.WalkSpeed;
			}

			#endregion

			#region Combat

			if (Input.GetButton("AttackPrimary")) {
				CastAbility(AbilitySlot.AttackMain);
			}
			if (Input.GetButton("AttackSecondary")) {
				CastAbility(AbilitySlot.AttackSecondary);
			}
			if (Input.GetButton("Ability1")) {
				CastAbility(AbilitySlot.First);
			}
			if (Input.GetButton("Ability2")) {
				CastAbility(AbilitySlot.Second);
			}
			if (Input.GetButton("Ability3")) {
				CastAbility(AbilitySlot.Third);
			}
			if (Input.GetButton("LimitBreak")) {
				if (canLimitBreak) {
					CastAbility(AbilitySlot.Limit);
					Limit = 0;
				}
			}
			if (Input.GetButton("HealthPotion")) {
				//print("potion");
				CastAbility(AbilitySlot.Potion);
			}

			#endregion

			if (Input.GetButtonDown("Pause")) {
				GameController.instance.PauseGame(true);
			}

			if (Input.GetButtonDown("Swap")) {
				if (GameController.instance.progress.hasCompanion && !SceneManager.GetActiveScene().name.Contains("Base")) {
					SwapToons();
				} else {
					print("No companion, cant swap");
				}

				//GameController.instance.OnSwapEventHandler(); //todo implement this
			}
		}
	}

	bool CheckCooldown(float lastCastTime, PlayerAbility ability) {
		//print(Time.time - lastCastTime + " " + ability.Cooldown);
		if (Time.time - lastCastTime > ability.Cooldown) {
			//print(ability.Cooldown);
			return true;
		} else {
			//print(Time.time - lastCastTime);
			return false;
		}
	}

	void CastAbility(AbilitySlot slot) {
		//print("casting: " + slot);

		Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;

		//Vector2 direction = new Vector2( playerController.Controller.Facing, 0);

		direction.Normalize();
		direction += playerController.Controller.Velocity;
		direction.Normalize();

		//float direction = FaceDir;
		direction *= playerController.moveSpeed; //todo need to figure out a better solution

		bool dropCarried = false;
		bool playAttack = false;

		switch (slot) {
			case AbilitySlot.AttackMain:
				if (stats.DataAsPlayer.attackMain) {
					//print("in main");
					if (CheckCooldown(lastCastAttackMain, stats.DataAsPlayer.attackMain)) {
						//print( "in cooldown" );
						if (stats.DataAsPlayer.attackMain.OnCast(direction, transform.position, gameObject)) {
							//print( "in cast" );
							lastCastAttackMain = Time.time;
							Limit += stats.DataAsPlayer.attackMain.LimitGaugeModifier;
							UpdateLastCastAbilities(slot);

							playAttack = true;

							//print( "last cast: " + lastCastAttackMain );
						}
					}
				} else {
					print("no main attack set");
				}
				break;

			case AbilitySlot.AttackSecondary:
				if (stats.DataAsPlayer.attackSecondary) {
					if (CheckCooldown(lastCastAttackSecond, stats.DataAsPlayer.attackSecondary)) {
						if (stats.DataAsPlayer.attackSecondary.OnCast(direction, transform.position, gameObject)) {
							lastCastAttackSecond = Time.time;
							Limit += stats.DataAsPlayer.attackSecondary.LimitGaugeModifier;
							UpdateLastCastAbilities(slot);
							playAttack = true;
						}
					}
				} else {
					print("no secondary attack set");
				}
				break;

			case AbilitySlot.First:
				if (stats.DataAsPlayer.abilityOne) {
					if (CheckCooldown(lastCastAbilityOne, stats.DataAsPlayer.abilityOne)) {
						if (stats.DataAsPlayer.abilityOne.OnCast(direction, transform.position, gameObject)) {
							lastCastAbilityOne = Time.time;
							Limit += stats.DataAsPlayer.abilityOne.LimitGaugeModifier;
							UpdateLastCastAbilities(slot);
							dropCarried = true;
							playAttack = true;
						}
					}
				} else {
					print("no ability upgradeRecipeOne set");
				}
				break;

			case AbilitySlot.Second:
				if (stats.DataAsPlayer.abilityTwo) {
					if (CheckCooldown(lastCastAbilityTwo, stats.DataAsPlayer.abilityTwo)) {
						if (stats.DataAsPlayer.abilityTwo.OnCast(direction, transform.position, gameObject)) {
							lastCastAbilityTwo = Time.time;
							Limit += stats.DataAsPlayer.abilityTwo.LimitGaugeModifier;
							UpdateLastCastAbilities(slot);
							dropCarried = true;
							playAttack = true;
						}
					}
				} else {
					print("no ability upgradeRecipeTwo set");
				}
				break;

			case AbilitySlot.Third:
				if (stats.DataAsPlayer.abilityThree) {
					if (CheckCooldown(lastCastAbilityThree, stats.DataAsPlayer.abilityThree)) {
						if (stats.DataAsPlayer.abilityThree.OnCast(direction, transform.position, gameObject)) {
							lastCastAbilityThree = Time.time;
							Limit += stats.DataAsPlayer.abilityThree.LimitGaugeModifier;
							UpdateLastCastAbilities(slot);
							dropCarried = true;
							playAttack = true;
						}
					}
				} else {
					print("no ability upgradeRecipeThree set");
				}
				break;
			case AbilitySlot.Limit:
				if (stats.DataAsPlayer.limitBreak) {
					stats.DataAsPlayer.limitBreak.OnCast(direction, transform.position, gameObject);
					dropCarried = true;
					playAttack = true;
				} else {
					print("no limit break set");
				}
				break;
			case AbilitySlot.Potion:

				//print("er");
				if (CheckCooldown(LastCastPotion, GameController.instance.potionAbility)) {
					//print("fs");
					if (GameController.instance.potionAbility.OnCast(direction, transform.position, gameObject)) {
						GameController.instance.lastCastPotion = Time.time;
						dropCarried = true;
					}
				}
				break;

			default:
				throw new ArgumentOutOfRangeException("slot", slot, null);
		}

		if (dropCarried) {
			if (GameController.instance.progress.isCarrying) {
				DropCarried();
			}
		}

		if (playAttack) {
			animator.SetTrigger("attack");
			playerController.ResetIdle();
		}
	}

	void HandleAirDashInput() {
		if (!playerController.Controller.collisions.below) {
			if (Input.GetKeyDown(KeyCode.D)) {
				if (tapTimerRight > 0 && tapCountRight == 1) {
					tapCountRight++;
					playerController.AirDash(1);
				} else {
					tapTimerRight = 0.5f;
					tapCountRight++;
				}
			}

			if (Input.GetKeyDown(KeyCode.A)) {
				if (tapTimerLeft > 0 && tapCountLeft == 1) {
					tapCountLeft++;
					playerController.AirDash(-1);
				} else {
					tapTimerLeft = 0.5f;
					tapCountLeft++;
				}
			}
		} else {
			tapTimerRight = 0;
			tapCountRight = 0;
			tapTimerLeft = 0;
			tapCountLeft = 0;
		}

		if (tapTimerRight > 0) {
			tapTimerRight -= Time.deltaTime;
		} else {
			tapCountRight = 0;
		}

		if (tapTimerLeft > 0) {
			tapTimerLeft -= Time.deltaTime;
		} else {
			tapCountLeft = 0;
		}
	}

	void SwapToons() {
		//todo fix zooming when changing characters OR use individual cameras for each object
		//0 out input so that character doesnt keep moving
		directionalInput = Vector2.zero;

		if (GameController.instance.oneIsActive) {
			if (GameController.instance.playerTwo.stats.isKOd) {
				return;
			}

			GameController.instance.playerOne.enabled = false;
			GameController.instance.playerTwo.enabled = true;

			GameController.instance.playerOne.GetComponent<CompStateMachine>().CycleController();
			GameController.instance.playerTwo.GetComponent<CompStateMachine>().CycleController();

			Camera.main.GetComponent<BoundingBoxFollowCamera>().ChangeTarget(
				      GameController.instance.playerTwo.GetComponent<ControllerInput>());
		} else {
			if (GameController.instance.playerOne.stats.isKOd) {
				return;
			}

			GameController.instance.playerTwo.enabled = false;
			GameController.instance.playerOne.enabled = true;

			GameController.instance.playerTwo.GetComponent<CompStateMachine>().CycleController();
			GameController.instance.playerOne.GetComponent<CompStateMachine>().CycleController();

			Camera.main.GetComponent<BoundingBoxFollowCamera>().ChangeTarget(
				      GameController.instance.playerOne.GetComponent<ControllerInput>());
		}

		GameController.instance.oneIsActive = !GameController.instance.oneIsActive;
		GameController.instance.hud.Init();

		Vector3 p1 = GameController.instance.ActivePlayerGameObject.transform.position;
		Vector3 p2 = GameController.instance.InactivePlayerGameObject.transform.position;
		GameController.instance.ActivePlayerGameObject.transform.position = new Vector3(p1.x, p1.y, -1);
		GameController.instance.InactivePlayerGameObject.transform.position = new Vector3(p2.x, p2.y, 1);

		GameController.instance.ActivePlayer.ResetAnims();
		GameController.instance.InactivePlayer.ResetAnims();
	}

	void ResetAnims() {
		if (animator == null) {
			animator = GetComponent<Animator>();
		}

		animator.ResetTrigger("attack");
		animator.ResetTrigger("jumping");
		animator.ResetTrigger("hasLanded");
		animator.ResetTrigger("runToFall");
		animator.ResetTrigger("idle");
		animator.SetFloat("speed", 0);
		animator.SetFloat("verticalSpeed", 0);

		animator.SetTrigger("reset");
	}

	/// <summary>
	/// Delegate for testing death
	/// </summary>
	void KnockOut() {
		if (stats.isKOd) {
			print("KnockOut called in sentinel " + name + " should go to gameover if single player");
			return;
		}

		//print("KnockOut called " + name);
		stats.isKOd = true;

		if (this == GameController.instance.ActivePlayer) {
			if (GameController.instance.progress.hasCompanion) {
				if (GameController.instance.InactivePlayer.stats.isKOd) {
					//gameover
					GameController.instance.OnGameOverEventHandler();
				} else {
					SwapToons();

					Vector3 newPos = transform.position;
					newPos.z = GameController.instance.ActivePlayer.transform.position.z + 1;
					var temp = Instantiate(carryKoPrefab, newPos, carryKoPrefab.transform.rotation);
					temp.GetComponentInChildren<SpriteRenderer>().sprite = stats.DataAsPlayer.mySprite;

					gameObject.SetActive(false);
				}
			} else {
				//call gameover
				GameController.instance.OnGameOverEventHandler();
			}
		} else {
			Vector3 newPos = transform.position;
			newPos.z = GameController.instance.ActivePlayer.transform.position.z + 1;

			var temp = Instantiate(carryKoPrefab, newPos, carryKoPrefab.transform.rotation);
			temp.GetComponentInChildren<SpriteRenderer>().sprite = stats.DataAsPlayer.mySprite;

			gameObject.SetActive(false);
		}
	}

	public void DropCarried() {
		Vector3 spawnPos = transform.position;
		spawnPos.z += 1;
		var temp = Instantiate(carryKoPrefab, spawnPos, carryKoPrefab.transform.rotation);
		temp.GetComponentInChildren<SpriteRenderer>().sprite = carriedObj.GetComponent<SpriteRenderer>().sprite;
		carriedObj.SetActive(false);
		GameController.instance.progress.isCarrying = false;
	}

	void UpdateLastCastAbilities(AbilitySlot slot) {
		if (this == GameController.instance.playerOne) {
			GameController.instance.progress.playerOneSecondToLastCast = GameController.instance.progress.playerOneLastCast;
			GameController.instance.progress.playerOneLastCast = slot;

			//print( name + " " + GameController.instance.progress.playerOneLastCast + " " + GameController.instance.progress.playerOneSecondToLastCast );
		} else {
			GameController.instance.progress.playerTwoSecondToLastCast = GameController.instance.progress.playerTwoLastCast;
			GameController.instance.progress.playerTwoLastCast = slot;

			//print( name + " " + GameController.instance.progress.playerTwoLastCast + " " + GameController.instance.progress.playerTwoSecondToLastCast );
		}
	}

}

public enum AbilitySlot {
	AttackMain,
	AttackSecondary,
	First,
	Second,
	Third,
	Limit,
	Potion
}