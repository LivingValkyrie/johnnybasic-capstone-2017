﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: EnemyData
/// </summary>
//[CreateAssetMenu]
public class EnemyData : CharacterData {
	#region Fields

	public bool canFly = false;
	public bool isRanged = false;

	public EnemyAbility basicAttack;
	public LootTable lootTable;
	public Color hue = Color.white;

    //AI Specific variables @Mike
    public float attackDistance;
    public float attackCooldown;
    public float chaseSpeed;
    public Animator myAnimator;
    public EnemyTypes myType;
    public bool stationaryPatrol;


	#endregion

	public override void Init() {
		//needs to update sprite renderer to use hue as color
	}
}