﻿using System.Runtime.CompilerServices;
using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: EnemyData
/// </summary>
//[CreateAssetMenu]
public class PlayerData : CharacterData {
	#region Fields

	public Sprite portrait, inactivePortrait;

	[Header("Movement")]
	public float maxJumpHeight;
	public float defense;
	public float armor;

	public bool canWallJump, canAirDash, canMultiJump;
	public int maxMultiJump;
	public float airDashVelocity = 30;

	public float sprintSpeed = 12;
	public float WalkSpeed {
		get { return moveSpeed; }
	}

	[Header("Combat")]
	[Tooltip("The ability used as the main attack.")]
	public PlayerAbility attackMain;
	[Tooltip("The ability used as the secondary attack")]
	public PlayerAbility attackSecondary;
	[Tooltip("The ability in the first slot")]
	public PlayerAbility abilityOne;
	[Tooltip("The ability in the second slot")]
	public PlayerAbility abilityTwo;
	[Tooltip("The ability in the third slot")]
	public PlayerAbility abilityThree;
	[Tooltip("The ability used as the limit break attack")]
	public PlayerAbility limitBreak;
	[Tooltip("The size of the limit gauge, the larger the gauge the longer it takes to fill.")]
	public float limitGaugeSize = 100f;

	[Header("Recipes")]
	[Tooltip("This is the recipe used every time the player wants to craft upgradeRecipeOne of this characters consumables.")]
	public UpgradeRecipe consumableRecipe;
	[Tooltip("The recipe used to upgrade room for the first time (room level 2)")]
	public UpgradeRecipe firstRoomUpgrade;
	[Tooltip("The recipe used to upgrade room for the second time (room level 3)")]
	public UpgradeRecipe secondRoomUpgrade;

	public string consumableName;
	public string abilityMods;


	#region AI Specific variables @Mike

	[Header("AI Values")]
	public float sprintDistance;
	public float runDistance;
	public float walkDistance;
	//@Mike
	public Animator myAnimator;
	public float engagementRadius;

	#endregion

	#endregion

	/// <summary>
	/// Initializes this instance by calling init on each ability
	/// </summary>
	public override void Init() {
		if (attackMain) {
			attackMain.Init(this, AbilitySlot.AttackMain);
		}
		if (attackSecondary) {
			attackSecondary.Init(this, AbilitySlot.AttackSecondary);
		}
		if (abilityOne) {
			abilityOne.Init(this, AbilitySlot.First);
		}
		if (abilityTwo) {
			abilityTwo.Init(this, AbilitySlot.Second);
		}
		if (abilityThree) {
			abilityThree.Init(this, AbilitySlot.Third);
		}
		if (limitBreak) {
			limitBreak.Init(this, AbilitySlot.Limit);
		}
	}

	public void ClonePlayerData(PlayerData toClone) {
		this.characterName = toClone.characterName;
		this.maxHealth = toClone.maxHealth;
		this.moveSpeed = toClone.moveSpeed;
		this.weight = toClone.weight;

		this.fireRes = toClone.fireRes;
		this.iceRes = toClone.iceRes;
		this.shockRes = toClone.shockRes;
		this.acidRes = toClone.acidRes;

		this.mySprite = toClone.mySprite;

		this.maxJumpHeight = toClone.maxJumpHeight;
		this.armor = toClone.armor;
		this.defense = toClone.defense;

		this.canAirDash = toClone.canAirDash;
		this.canWallJump = toClone.canWallJump;
		this.canMultiJump = toClone.canMultiJump;

		this.maxMultiJump = toClone.maxMultiJump;
		this.airDashVelocity = toClone.airDashVelocity;

		this.sprintSpeed = toClone.sprintSpeed;

		this.abilityOne = toClone.abilityOne;
		this.abilityTwo = toClone.abilityTwo;
		this.abilityThree = toClone.abilityThree;
		this.attackMain = toClone.attackMain;
		this.attackSecondary = toClone.attackSecondary;
		this.limitBreak = toClone.limitBreak;
		this.limitGaugeSize = toClone.limitGaugeSize;

		this.consumableRecipe = toClone.consumableRecipe;
		this.firstRoomUpgrade = toClone.firstRoomUpgrade;
		this.secondRoomUpgrade = toClone.secondRoomUpgrade;

		this.runDistance = toClone.runDistance;
		this.sprintDistance = toClone.sprintDistance;
		this.walkDistance = toClone.walkDistance;

		this.consumableName = toClone.consumableName;
		this.abilityMods = toClone.abilityMods;

		this.portrait = toClone.portrait;
		this.inactivePortrait = toClone.inactivePortrait;

		this.animatorController = toClone.animatorController;
	}

	public static PlayerData operator +(PlayerData data, PlayerMod mod) {
		PlayerData temp = CreateInstance<PlayerData>();

		//PlayerData temp = new PlayerData(data);
		temp.ClonePlayerData(data);
		temp.armor += mod.armorIncrease;
		temp.maxHealth += mod.maxHealthIncrease;
		temp.defense += mod.defenseIncrease;

		//Debug.Log(data.characterName + " " + mod.characterName +" "+temp.armor + " " + temp.maxHealth + " " + temp.defense);

		return temp;
	}

	public int GetConsumableCount() {
		if (string.IsNullOrEmpty(consumableName)) {
			//returns false as there was no increment
			return 0;
		}

		return (int) typeof(GameProgress).GetField(consumableName).GetValue(GameController.instance.progress);
	}

	public void UseConsumable() {
		if ( string.IsNullOrEmpty( consumableName ) ) {
			//returns false as there was no increment
		}

		typeof( GameProgress ).GetField( consumableName ).SetValue( GameController.instance.progress, GetConsumableCount() - 1 );
	}
}