﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Enemy
/// </summary>
[RequireComponent(typeof(CharacterStats))]
[DisallowMultipleComponent]
public class Enemy : MonoBehaviour {
	#region Fields

	CharacterStats stats;

	#endregion

	/// <summary>
	/// Starts this instance. adds droploot to death event
	/// </summary>
	public void Start() {
		stats = GetComponent<CharacterStats>();
		stats.OnDeathEventHandler += Death;
		//GetComponent<ControllerInput>().moveSpeed = stats.data.moveSpeed;
	}

	/// <summary>
	/// Kills this instance.
	/// </summary>
	void Death() {
		stats.DataAsEnemy.lootTable.DropLoot(transform.position);
		Destroy(gameObject);
	}
}