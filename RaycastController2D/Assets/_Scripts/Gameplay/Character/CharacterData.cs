﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: CharacterData is a parent and storage class for entity data 
/// </summary>
public abstract class CharacterData : ScriptableObject {
	#region Fields

	//the name of the character
	public string characterName;

	//the maximum health
	public int maxHealth;

	//how fast they move
	public float moveSpeed;

	[Range(1, 90)]
	[Tooltip("The maximum angle of slope the controller can climb")]
	public float maxSlopeAngle = 44;

	//the characters weight. used in knockback
	public float weight = 10;

	////the characters resistance levels. med is standard
	public CharacterStats.ResistLevel fireRes = CharacterStats.ResistLevel.Med;
	public CharacterStats.ResistLevel iceRes = CharacterStats.ResistLevel.Med;
	public CharacterStats.ResistLevel shockRes = CharacterStats.ResistLevel.Med;
	public CharacterStats.ResistLevel acidRes = CharacterStats.ResistLevel.Med;

	[Header("Audial/Visual")]
	public Sprite mySprite;
	public AnimatorOverrideController animatorController;
	public AudioClip hurtSound;
	public AudioClip deathSound;
	public AudioClip healingSound;

	#endregion

	/// <summary>
	/// Initializes this instance. 
	/// </summary>
	public abstract void Init();

}