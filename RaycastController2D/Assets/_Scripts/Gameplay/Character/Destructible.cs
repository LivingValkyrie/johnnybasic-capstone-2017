﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Destructible is an object that can be destroyed in order to instaniate an object
/// </summary>
public class Destructible : MonoBehaviour {
	#region Fields

	public LootTable lootTable;
	public AudioClip destroySound;
	public GameObject[] toSpawn;

	#endregion

	/// <summary>
	/// Destructs this instance and spawns loot object
	/// </summary>
	public void Destruct() {
		if (lootTable) {
			lootTable.DropLoot(transform.position);
		}
		GameController.instance.PlaySoundEffect( destroySound );

		if (toSpawn.Length > 0) {
			foreach (GameObject o in toSpawn) {
				Instantiate(o, transform.position, Quaternion.identity);
			}
		}

		Destroy( gameObject);
	}
}