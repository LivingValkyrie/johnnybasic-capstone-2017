﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: FloatingText
/// </summary>
public class FloatingText : MonoBehaviour {
	#region Fields

	public Animator anim;

	#endregion
	
	void Start() {
		AnimatorClipInfo[] clipInfo = anim.GetCurrentAnimatorClipInfo(0);
		Destroy(gameObject, clipInfo[0].clip.length);
	}

	public void Init(string text, Color color) {
		anim.GetComponent<Text>().text = text;
		anim.GetComponent<Outline>().effectColor = color;

	}
}
