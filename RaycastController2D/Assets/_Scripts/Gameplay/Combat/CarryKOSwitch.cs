﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: CarryKOSwitch
/// </summary>
public class CarryKOSwitch : SimpleSwitch {
	#region Fields

	#endregion

	public override void OnActivate() {
		activator.GetComponent<Player>().carriedObj.SetActive(true);
		activator.GetComponent<Player>().carriedObj.GetComponent<SpriteRenderer>().sprite = GetComponent<SpriteRenderer>().sprite;
		GameController.instance.progress.isCarrying = true;

		Destroy(gameObject);
	}

	public override void OnDeactivate() {
		Toggle(activator);
	}
}