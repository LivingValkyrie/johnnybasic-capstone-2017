﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: InventoryItemDisplay is a component that displays item data based on its inventory position
/// </summary>
public class InventoryItemDisplay : MonoBehaviour {
	#region Fields

	//inventory to read from
	public Inventory inventory;

	//icon to display
	public Image icon;
	//text objects to display data in
	public Text nameText, countText;
	//position of the invenotory item to display
	public int inventoryPos;
	
	#endregion

	/// <summary>
	/// Called when [enabled]. fills component objects with values from players inventory
	/// </summary>
	void OnEnable() {
		//check to see if position is within bounds
		if (inventory.items.Count > inventoryPos) {
			//check that item at position is not null
			if ( inventory.items.ElementAt( inventoryPos ).Key != null ) {
			
				//populate ui objects with data from inventory object	
			icon.sprite = inventory.items.ElementAt(inventoryPos).Key.icon;
			nameText.text = inventory.items.ElementAt( inventoryPos ).Key.itemName;
			countText.text = inventory.items.ElementAt( inventoryPos ).Key.ToString();
			}
		}
	}
}