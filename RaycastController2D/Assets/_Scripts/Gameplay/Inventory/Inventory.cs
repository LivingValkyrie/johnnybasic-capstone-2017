﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Inventory is a component that allows for collecting inventory items.
/// </summary>
[DisallowMultipleComponent]
public class Inventory : MonoBehaviour {
	#region Fields

	//the collection that holds items
	public Dictionary<InventoryItem, int> items;
	//how many different items can be held at once
	public int maxCapacity;

	#region temp base inventory
	//the inventory of the base
	public Dictionary<InventoryItem, int> baseItems;

	#endregion

	#endregion

	/// <summary>
	/// Initializes item collections.
	/// </summary>
	void Awake() {
		items = new Dictionary<InventoryItem, int>();
		baseItems = new Dictionary<InventoryItem, int>();
	}

	public void Restart() {
		Awake();
	}

	//void Update() {
	//	if (Input.GetKeyDown(KeyCode.Return)) {
	//		string temp = "";
	//		foreach (KeyValuePair<InventoryItem, int> kvp in items) {
	//			temp += $"\n {kvp.Key.itemName} {kvp.Value}";
	//		}
	//		print(temp);
	//	}
	//}

	/// <summary>
	/// Adds [itemToAdd] to inventory if inventory is not full and inventory does not hold capacity of [itemToAdd]
	/// </summary>
	/// <param name="itemToAdd">The item to add.</param>
	/// <param name="amount"></param>
	/// <returns>Whether or not [itemToAdd] was added to inventory or not.</returns>
	public bool AddToInventory(InventoryItem itemToAdd, int amount = 1) {
		//check if exist
		if (items.ContainsKey(itemToAdd)) {
			//check if at capacity and add if not
			if (items[itemToAdd] < itemToAdd.maxCapacity) {
				items[itemToAdd] += amount;
				//print(itemToAdd.itemName + " has been added to inventory");
				return true;
			} else {
				print(itemToAdd.itemName + " is at capacity");
				return false;
			}
		} else {
			//if inventory not full
			if (items.Count < maxCapacity) {
				items.Add(itemToAdd, amount);
				//print(itemToAdd.itemName + " has been added to inventory");
				return true;
			} else {
				//inventory full
				print("Inventory is at capacity");
				return false;
			}
		}
	}

	/// <summary>
	/// Removes from inventory.
	/// </summary>
	/// <param name="itemToRemove">The item to remove.</param>
	/// <param name="amount">The amount.</param>
	/// <returns></returns>
	public bool RemoveFromInventory(InventoryItem itemToRemove, int amount = 1) {
		//check if exist
		//print("exist check");
		//	print( items.ContainsKey( itemToRemove ) );
		InventoryItem refItem;
		if (items.ContainsInventoryItem(itemToRemove, out refItem)) {
			//check if at amount or greater
			//print("amount check");
			if (items[refItem] >= amount) {
				//print("remove amount");
				items[refItem] -= amount;
				//print("0 check");
				if (items[refItem] == 0) {
					//print("remove object");
					items.Remove(refItem);
				}
				return true;
			} else {
				return false;
			}
		} else {
			print("isnt in inventory");
			return false;
		}
	}

	public bool RemoveFromBaseInventory( InventoryItem itemToRemove, int amount = 1 ) {
		//check if exist
		//print("exist check");
		//	print( items.ContainsKey( itemToRemove ) );
		InventoryItem refItem;
		if ( baseItems.ContainsInventoryItem( itemToRemove, out refItem ) ) {
			//check if at amount or greater
			//print("amount check");
			if ( baseItems[refItem] >= amount ) {
				//print("remove amount");
				baseItems[refItem] -= amount;
				//print("0 check");
				if ( baseItems[refItem] == 0 ) {
					//print("remove object");
					baseItems.Remove( refItem );
				}
				return true;
			} else {
				return false;
			}
		} else {
			print( "isnt in inventory" );
			return false;
		}
	}

	public bool TestRecipe(UpgradeRecipe recipe) {
		//print("----- recipe test -----");
		foreach (RecipeEntry entry in recipe.itemsRequired) {
			//test to see if item is in collection, if so check count else return false
			//if count is >= required add item to list, else return false;
			//move to next item
			//if all items checked and havent returned false yet, then remove all item counts
			//then clean house by checking ==0 for every item count in collection


			//this is how this works, the refItem is the actual reference inside of the dictionary, so it is needed for accurate calls after the game has been loaded. you can think reference types for that.
			InventoryItem refItem;

			if ( baseItems.ContainsInventoryItem( entry.item, out refItem ) ) {
				//check if at amount or greater
				//print("amount check");
				if ( baseItems[refItem] >= entry.required ) {
				//	print( $"passed: {entry.item}, needed: {entry.required} have: {baseItems[refItem]}" );
					continue;
				} else {
					//print( $"not enough {entry.item}, needed: {entry.required} have: {baseItems[refItem]}" );
					return false;
				}
			} else {
				//print( $"isnt in inventory {entry.item}" );
				return false;
			}
		}

		//all items in collection
		//print("All items in inventory");

		foreach (RecipeEntry entry in recipe.itemsRequired) {
			RemoveFromBaseInventory(entry.item, entry.required);
			//print($"Removed {entry.required} of {entry.item}");
		}

		//need to remove items from inventory now
		return true;

	}

	/// <summary>
	/// Moves items to base inventory.
	/// </summary>
	public void MoveToBaseInventory() {
		//foreach item in inventory
		foreach (KeyValuePair<InventoryItem, int> item in items) {
			//check if base already has entry, add if so, create entry if not
			InventoryItem refItem;
			if (baseItems.ContainsInventoryItem(item.Key, out refItem)) {
				baseItems[refItem] += item.Value;
			} else {
				baseItems.Add(item.Key, item.Value);
			}
		}

		//clear inventory
		items.Clear();
	}

	public override string ToString() {
		print(items.Count + " " + baseItems.Count);
		string toReturn = "Items: \n";

		foreach (KeyValuePair<InventoryItem, int> keyValuePair in items) {
			toReturn += $"{keyValuePair.Key.itemName} {keyValuePair.Value.ToString()}";
		}

		toReturn += "Base Items: \n";

		foreach ( KeyValuePair<InventoryItem, int> keyValuePair in baseItems ) {
			//print("base item " + keyValuePair.Key.itemName);
			toReturn += $"{keyValuePair.Key.itemName} {keyValuePair.Value.ToString()}\n";
		}
		
		return toReturn;
	}

	public void AddLootTableToBaseInventory(LootTable table) {
		foreach (LootItem tableLootItem in table.lootItems) {
			AddToInventory(tableLootItem.item, 100);
		}

		MoveToBaseInventory();
	}
}

public static class Extensions {
	public static bool TryGetInventoryItem(this Dictionary<InventoryItem, int> dictionary, InventoryItem item, out int value) {
		foreach (KeyValuePair<InventoryItem, int> kvp in dictionary) {
			if (string.Equals(kvp.Key.itemName, item.itemName)) {
				value = kvp.Value;
				return true;
			}
		}

		value = 0;
		return false;
	}

	public static bool ContainsInventoryItem( this Dictionary<InventoryItem, int> dictionary, InventoryItem item, out InventoryItem itemRef ) {
		foreach ( KeyValuePair<InventoryItem, int> kvp in dictionary ) {
			if ( string.Equals( kvp.Key.itemName, item.itemName ) ) {
				itemRef = kvp.Key;
				return true;
			}
		}

		itemRef = null;
		return false;
	}
}