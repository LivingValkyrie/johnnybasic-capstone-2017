﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: LootObject is the in pick up that interacts with players for adding invventory items
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class LootObject : NonPlayerControllerInput {
	#region Fields

	//loot item of this instance
	public InventoryItem item;

	#endregion

	public void Init() {
		velocity = new Vector3( Random.Range( 0, 10 ), Random.Range( 0, 10 ), 0 );
		GetComponent<SpriteRenderer>().sprite = item.icon;
		
	}

	protected virtual void FixedUpdate() {
		Controller.Move(velocity * Time.deltaTime);
	}

	/// <summary>
	/// Called when [trigger enter2 d]. attempts to add [item] to player inventory, destroys [gameobject] if successful
	/// </summary>
	/// <param name="other">The other.</param>
	void OnTriggerEnter2D(Collider2D other) {
		//print("triggered");
		//check if colliding with active player
		if (other.gameObject == GameController.instance.ActivePlayerGameObject ||
		    other.gameObject == GameController.instance.InactivePlayerGameObject) {
			//add to inventory, if successful destroy self
			if (GameController.instance.inventory.AddToInventory(item)) {
				GameController.instance.PlaySoundEffect( item.pickUpSound );

				Destroy( gameObject);
			}
		}
	}
}