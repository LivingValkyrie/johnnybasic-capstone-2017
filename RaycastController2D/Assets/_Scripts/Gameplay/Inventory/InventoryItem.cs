﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: InventoryItem is a data class for in game items
/// </summary>
[CreateAssetMenu]
public class InventoryItem : ScriptableObject {
	#region Fields

	//the name
	public string itemName;
	//the icon
	public Sprite icon;
	//how many u can carry at once 
	public int maxCapacity;
	//the rarity of the item
	public Rarity rarity;
	//the description of the item
	public string description;
	public AudioClip pickUpSound;

	#endregion

	public enum Rarity {
		VeryCommon, Common, Uncommon, Rare, VeryRare
	}

	public override string ToString() {
		return itemName;
	}

}