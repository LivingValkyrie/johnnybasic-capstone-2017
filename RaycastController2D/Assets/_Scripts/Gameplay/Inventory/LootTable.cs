﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: LootTable
/// </summary>
[CreateAssetMenu]
public class LootTable : ScriptableObject {
	#region Fields

	[Tooltip("The objects to be dropped when destroyed")]
	public LootItem[] lootItems;

	[HideInInspector]
	public GameObject lootObj;

	#endregion

	public void DropLoot(Vector3 pos) {
		foreach (LootItem item in lootItems) {
			if (item.Equals(default(LootItem))) {
				continue;
			}

			int drop = Random.Range(1, 100);
			if (drop <= item.chance) {
				int toDrop = Random.Range(item.quantityMin, item.quantityMax);
				for (int i = 0; i < toDrop; i++) {
					LootObject temp = Instantiate( lootObj, pos, Quaternion.identity ).GetComponent<LootObject>();
					temp.item = item.item;
					temp.Init();
				}
			}
		}


	}

}

[System.Serializable]
public struct LootItem {
	public InventoryItem item;
	public float chance;
	public int quantityMin, quantityMax;
}