﻿/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: WaypointSwitch
/// </summary>
public class WaypointSwitch : SimpleSwitch {
	#region Fields

	#endregion

	public override void OnActivate() {
		GetComponentInParent<Waypoint>().ToggleMap();
		isActive = false;
	}

	public override void OnDeactivate() {
		Toggle(activator);
	}
}