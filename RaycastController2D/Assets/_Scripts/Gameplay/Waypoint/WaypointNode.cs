﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: WaypointNode is a component for the waypoint map. functions as a way to load levels thru waypoints
/// </summary>
public class WaypointNode : MonoBehaviour {
	#region Fields

	//the id of the waypoint this node represents
	public string waypointId;
	public AudioClip teleportSound;

	#endregion

	/// <summary>
	/// Loads the waypoint level if it exist in memory.
	/// </summary>
	public void LoadWaypointLevel() {
		//create array of waypoints
		string[] files = ES2.GetFiles(SaveDataBackendHandler.saveSlot + "/Waypoints/");

		//load each waypoint until a match is found, then load the level it is assigned to
		foreach (string file in files) {
			WaypointData temp = ES2.Load<WaypointData>(SaveDataBackendHandler.saveSlot + "/Waypoints/" + file);
			if (temp.waypointId == waypointId) {
				GameController.instance.PlaySoundEffect(teleportSound, true);
				GameController.instance.progress.spawnPointId = -1;
				GameController.instance.LoadLevel(temp.levelToLoad);

				//print($"{temp.waypointId}");
			}
		}
	}

	void Start() {
		//create array of waypoints
		string[] files = ES2.GetFiles(SaveDataBackendHandler.saveSlot + "/Waypoints/");

		bool turnOff = true;

		//load each waypoint until a match is found, then load the level it is assigned to
		foreach (string file in files) {
			WaypointData temp = ES2.Load<WaypointData>(SaveDataBackendHandler.saveSlot + "/Waypoints/" + file);
			if (temp.waypointId == waypointId) {
				if (!temp.isBroken) {
					turnOff = false;
				}
			}
		}

		gameObject.SetActive(!turnOff);

		//print( $"{name} is {turnOff}" );
	}
}