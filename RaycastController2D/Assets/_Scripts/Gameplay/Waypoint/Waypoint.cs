﻿using System.Security.Permissions;
using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Waypoint is a gameobject that allows for teleporting to other waypoints or destroying to receive loot objects.
/// </summary>
public class Waypoint : MonoBehaviour {
	#region Fields

	//data for this waypoint. needed for saving
	public WaypointData data;
	public GameObject spawnPos;
	public LootTable lootTable;

	//debug stuff
	public bool deleteOnDestroy;
	string path;
	public GameObject prompt;
	public GameObject map;

	#endregion

	/// <summary>
	/// Starts this instance.
	/// </summary>
	void Start() {
		prompt.SetActive(false);

		path = SaveDataBackendHandler.SaveFolder + "Waypoints/Waypoint_" + data.waypointId;

		if (ES2.Exists(path)) {
			ES2.Load<WaypointData>(path, this.data);

			if (data.isBroken) {
				//print("Was broken in save file");
				Destroy(transform.root.gameObject);
			}
		} else {
			//save data so that its in the file
			ES2.Save(this.data, path);

			//print("saved inital data");
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (!data.indestructible) {
			if (other.gameObject == GameController.instance.ActivePlayerGameObject) {
				if (data.firstEnter) {
					prompt.SetActive(true);
					data.firstEnter = false;
					GameController.instance.PauseGame(false);
				}
			}
		}
	}

	void OnDestroy() {
		if (deleteOnDestroy) {
			ES2.Delete(path);

			//print("Deleted waypoint from data");
		}
	}

	public void BreakOrSave(bool save) {
		if (save) {
			//print("waypoint saved");

			//set bool and unpause, then save
			data.isBroken = false;
			GameController.instance.PauseGame(false);
			ES2.Save(this.data, path);

			//shut off prompt
			prompt.SetActive(false);
		} else {
			//set bool and unpause
			data.isBroken = true;
			GameController.instance.PauseGame(false);

			//spawn loot objects, change this to chance based later
			lootTable.DropLoot(transform.position);

			//save then destroy
			ES2.Save(this.data, path);
			Destroy(transform.root.gameObject);
		}
	}

	public void ToggleMap() {
		//print("toggle called");
		map.SetActive(!map.activeInHierarchy);
		GameController.instance.PauseGame(false);
	}
}

/// <summary>
/// WaypointData is a data class for storing waypoint info in save files
/// </summary>
[System.Serializable]
public class WaypointData {
	public bool firstEnter = true;
	public bool indestructible = false;
	public string waypointId;
	public string levelToLoad;
	public bool isBroken = false;
}