﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: TouchMenuHandler 
/// </summary>
public class Menu : MonoBehaviour {
	#region Fields

	public GameObject[] menus;

	public bool rememberMenuOnClose;
	public MenuState currentState;
	MenuState prevState;

	#endregion

	void Start() {
		foreach (GameObject menu in menus) {
			menu.SetActive(false);
		}

		prevState = currentState;
		menus[(int) currentState].SetActive(true);
	}

	void Update() {
		if (prevState != currentState) {
			menus[(int) currentState].SetActive(true);
			menus[(int) prevState].SetActive(false);
			prevState = currentState;
		}
	}

	public void SetState(MenuState state) {
		currentState = state;
	}

	MenuState GetNextState() {
		if ((int) currentState >= Enum.GetValues(typeof (MenuState)).Length - 1) {
			//on final state
			return currentState;
		} else {
			return currentState + 1;
		}
	}

	MenuState GetPreviousState() {
		if ((int) currentState <= 0) {
			//on first state
			return currentState;
		} else {
			return currentState - 1;
		}
	}

}

public enum MenuState {
	MenuNew,
	MenuHelp,
	MenuCredits,
	MenuQuit
}