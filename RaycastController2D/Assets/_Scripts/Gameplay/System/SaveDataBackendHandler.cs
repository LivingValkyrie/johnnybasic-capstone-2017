﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SaveDataBackendHandler is a gameobject that handles all saving and loading
/// </summary>
[DisallowMultipleComponent]
public class SaveDataBackendHandler : MonoBehaviour {
	#region Fields

	public static string saveSlot = "Save_One";
	
	public static string SaveFolder {
		get { return saveSlot + "/"; }
	}
	public static string ProgressPath {
		get { return SaveFolder + "Progress/"; }
	}

	#endregion

	public static void SaveData() {
		ES2.Save(GameController.instance.inventory.maxCapacity, ProgressPath + "InventoryCapacity");
		ES2.Save(GameController.instance.inventory.items, ProgressPath + "Inventory");
		ES2.Save(GameController.instance.inventory.baseItems, ProgressPath + "BaseInventory");

		//print( "--- saving ---" );
		//print( GameController.instance.inventory );
		//print( "--------------" );

		ES2.Save(GameController.instance.progress, ProgressPath + "GameProgress");
	}

	public void LoadData(int slot) {
		SetSaveString(slot);

		if (ES2.Exists(ProgressPath)) {
			//print("loading inventory");
			GameController.instance.inventory.maxCapacity = ES2.Load<int>(ProgressPath + "InventoryCapacity");
			GameController.instance.inventory.items = ES2.LoadDictionary<InventoryItem, int>(ProgressPath + "Inventory");
			GameController.instance.inventory.baseItems = ES2.LoadDictionary<InventoryItem, int>(ProgressPath + "BaseInventory");

			GameController.instance.progress = ES2.Load<GameProgress>(ProgressPath + "GameProgress");


			SceneManager.LoadScene( GameController.instance.progress.levelName);

			//print("--- Loaded ---");
			//print(GameController.instance.inventory);
			//print("--------------");

		} else {
			//load defaults
			GameController.instance.Restart();

			SceneManager.LoadScene(GameController.instance.defaultScene);
		}
	}

	public static string GetData( int slot ) {
		SetSaveString( slot );
		if ( ES2.Exists( ProgressPath ) ) {
			GameProgress temp = ES2.Load<GameProgress>( ProgressPath + "GameProgress" );
			return temp.ToString();
		} else {
			return "New Game";
		}
	}

	public void DeleteData(int slot) {
		SetSaveString(slot);
		ES2.Delete(SaveFolder);
		//print("Deleted file at " + saveSlot);
	}

	static void SetSaveString(int slot) {
		saveSlot = "Save_" + ((SaveSlot) slot);

		//print(saveSlot);
	}

}

[System.Serializable]
public enum SaveSlot {
	Zero, // do not use
	One,
	Two,
	Three
}