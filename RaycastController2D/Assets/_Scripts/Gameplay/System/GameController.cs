﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: GameController
/// </summary>
[RequireComponent(typeof(Inventory))]
[DisallowMultipleComponent]
public class GameController : MonoBehaviour {
	#region Fields

	/// <summary>
	/// The default scene when loading a new game
	/// </summary>
	public string defaultScene = "Zone_0_1";
	public AudioClip menuMusic, safezoneMusic, dungeonMusic, gameOverMusic, overWorldMusic, bossIntro;
	public bool autoSave = true;
	public bool testing = false;

	public static GameController instance;

	public bool GamePaused { get; private set; }

	[Header("Menus")]
	public SceneTransitionPrompt sceneTransitionPrompt;
	public GameObject pauseMenu;
	[HideInInspector]
	public Inventory inventory;

	public Player playerOne, playerTwo;
	public bool oneIsActive = true;
	public float lastCastPotion = 0;

	public bool playMusic = true;
	public LootTable cheatTable;

	#region Player properties

	/// <summary>
	/// Gets the Player component of the active player.
	/// </summary>
	/// <value>
	/// The active player.
	/// </value>
	public Player ActivePlayer {
		get {
			if (oneIsActive) {
				return playerOne;
			} else {
				return playerTwo;
			}
		}
	}

	/// <summary>
	/// Gets the active player game object.
	/// </summary>
	/// <value>
	/// The active player game object.
	/// </value>
	public GameObject ActivePlayerGameObject {
		get {
			if (oneIsActive) {
				return playerOne.gameObject;
			} else {
				return playerTwo.gameObject;
			}
		}
	}

	/// <summary>
	/// Gets the Player component of the inactive player.
	/// </summary>
	/// <value>
	/// The active player.
	/// </value>
	public Player InactivePlayer {
		get {
			if (oneIsActive) {
				return playerTwo;
			} else {
				return playerOne;
			}
		}
	}

	/// <summary>
	/// Gets the inactive player game object.
	/// </summary>
	/// <value>
	/// The active player game object.
	/// </value>
	public GameObject InactivePlayerGameObject {
		get {
			if (oneIsActive) {
				return playerTwo.gameObject;
			} else {
				return playerOne.gameObject;
			}
		}
	}

	#endregion

	public delegate void SwapEvent();
	public event SwapEvent SwapEventHandler;
	public delegate void GameOver();
	public event GameOver GameOverEventHandler;

	public string gameoverScene = "GameOver";

	#region hud stuff

	public HeadsUpDisplay hud;
	bool firstRan = false;
	public PlayerAbility potionAbility;
	public FloatingText popupText;

	#endregion

	#region Saving stuff

	public GameProgress progress;
	PlayerData[] playerDataArray;

	public PlayerData GetPlayerData(string playerName) {
		return playerDataArray[DataIndex(playerName)] + instance.progress.GetPlayerMod(playerName);
	}

	#endregion

	AudioSource musicSource;

	#endregion

	/// <summary>
	/// Awakes this instance. sets up singleton
	/// </summary>
	void Awake() {
		if (instance == null) {
			instance = this;

			DontDestroyOnLoad(gameObject);
			LevelLoaded(SceneManager.GetActiveScene(), LoadSceneMode.Single);
		} else {
			if (instance != this) {
				Destroy(gameObject);
			}
		}
		musicSource = GetComponent<AudioSource>();
	}

	void Start() {
		GameController.instance.inventory = GetComponent<Inventory>();
		GameOverEventHandler += LoadGameOver;
		SceneManager.sceneLoaded += LevelLoaded;
		playerDataArray = Resources.LoadAll<PlayerData>("");

		//previousSceneName = SceneManager.GetActiveScene().name;

		if (testing) {
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);

			//LoadLevel( SceneManager.GetActiveScene().name );
		}
	}

	void Update() {
		//GetCheats();

		//if (Input.GetKeyDown(KeyCode.Delete)) {
		//	playMusic = !playMusic;

		//	if (playMusic) {
		//		musicSource.Play();
		//	} else {
		//		musicSource.Stop();
		//	}
		//}

		if (Input.GetKeyDown(KeyCode.F5)) {
			inventory.AddLootTableToBaseInventory(cheatTable);
		}
	}

	public void Restart() {
		progress = new GameProgress();
		if (progress.baseData.Equals(default(BaseData))) {
			//print( "base data has not been set, set up" );
			progress.baseData = new BaseData {johnnysRoom = 1, abigalesRoom = 1};
		}

		//previousSceneName = SceneManager.GetActiveScene().name;

		instance.inventory.Restart();
	}

	public void CreateFloatingText(string text, Transform location, CharacterStats.DamageType type) {
		FloatingText fText = Instantiate(popupText);
		Vector2 screenPos = Camera.main.WorldToScreenPoint(location.position);
		fText.transform.SetParent(hud.transform, false);
		fText.transform.position = screenPos;

		Color color = type == CharacterStats.DamageType.Heal ? Color.green : Color.red;
		fText.Init(text, color);
	}

	int DataIndex(string name) {
		for (int i = 0; i < playerDataArray.Length; i++) {
			PlayerData data = playerDataArray[i];
			if (string.Equals(data.characterName, name)) {
				return i;
			}
		}
		Debug.LogWarning("Name " + name + " does not match any playerdata object");
		return -1;
	}

	public void PlaySoundEffect(AudioClip clip, bool persistent = false) {
		if (clip == null) {
			return;
		}
		GameObject temp = new GameObject(clip.name + " player");
		temp.AddComponent<AudioSource>().clip = clip;
		temp.AddComponent<DestroyAfterClip>();
		temp.GetComponent<AudioSource>().Play();
		if (persistent) {
			DontDestroyOnLoad(temp);
		}
	}

	/// <summary>
	/// Loads the level. if in a game level it first updates progress.
	/// </summary>
	/// <param name="levelName">Name of the level.</param>
	public void LoadLevel(string levelName) {
		//print("load level");
		if (loadCheat) {
			loadCheat = false;
			levelName = "LootTestRoom";
		}

		if (!SceneManager.GetActiveScene().name.Contains("Menu_") && !SceneManager.GetActiveScene().name.Contains("GameOver")) {
			//print("Updating instance progress before loading new scene");
			progress.levelName = SceneManager.GetActiveScene().name;

			progress.playerOneName = ActivePlayer.stats.DataAsPlayer.characterName;
			progress.playerOneHealth = ActivePlayer.stats.currHealth;
			progress.playerOneLimit = ActivePlayer.Limit;

			if (progress.hasCompanion && !instance.progress.levelName.Contains("Base")) {
				progress.playerTwoName = InactivePlayer.stats.DataAsPlayer.characterName;
				progress.playerTwoHealth = InactivePlayer.stats.currHealth;
				progress.playerTwoLimit = playerTwo.Limit;
			}

			//print(GameController.instance.progress);

			//Debug.Break();
		}

		//SceneManager.LoadScene(levelName);
		StartCoroutine(LoadAfterFade(levelName));
		HideSceneTransitionPrompt();

		//previousSceneName = SceneManager.GetActiveScene().name;
	}

	void LevelLoaded(Scene scene, LoadSceneMode mode) {
		//print($"loading {scene.name} and changing progress to reflect that.");
		progress.levelName = scene.name;

		if (scene.name.Contains("Waypoint") || scene.name.Contains("Base")) {
			//print("in a safe zone, handle save stuff and set up player refs");

			if (scene.name.Contains("Base")) {
				if (!inventory) {
					instance.inventory = GetComponent<Inventory>();
				}
				inventory.MoveToBaseInventory();

				//if (ActivePlayer == null) {
				//print("active is null");
				if (progress.playerOneName != GameProgress.defaultPlayerName) {
					//Debug.LogError("Swapping players in progress with " + progress.playerOneName + " as active player");
					instance.progress.SwapPlayers();
				}

				//}

				oneIsActive = true;

				instance.progress.playerTwoHealth = instance.progress.playerOneHealth = 100;
				instance.progress.currPotions = instance.progress.maxPotions;
				instance.progress.isCarrying = false;
			}

			SetupPlayers();

			hud.gameObject.SetActive(true);

			if (!firstRan) {
				firstRan = true;
				hud.Init();
			}

			if (autoSave && !testing) {
				SaveDataBackendHandler.SaveData();
			}
		} else if (scene.name.Contains("Menu_")) {
			//print( "in a menu, turn off shit" );
			hud.gameObject.SetActive(false);
		} else if (scene.name.Contains("GameOver")) {
			hud.gameObject.SetActive(false);
		} else {
			//print("in a normal scene, find players and turn on hud");
			SetupPlayers();

			hud.gameObject.SetActive(true);

			if (!firstRan) {
				firstRan = true;
				hud.Init();
			}
		}

		ChangeMusic(scene);
		
	}

	//string previousSceneName;

	void SetupPlayers() {
		oneIsActive = true;
		playerOne = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

		#region spawn points

		GameObject spawnPos = null;
		try {
			//print("In try");
			if (instance.progress.spawnPointId == -1) {
				//print("Spawn point is waypoint");
				spawnPos = FindObjectOfType<Waypoint>().spawnPos;
			} else {
				SceneTransitionTrigger[] spawnPoints = Resources.FindObjectsOfTypeAll<SceneTransitionTrigger>();
				foreach (SceneTransitionTrigger trigger in spawnPoints) {
					if (trigger.mySpawnpointID == instance.progress.spawnPointId) {
						spawnPos = trigger.spawnPointObject;
						break;
					}
				}
			}

			if (spawnPos == null) {
				spawnPos = playerOne.gameObject;
			}

			//print(spawnPos.name + " at end of try");
		} catch (Exception) {
			Debug.LogWarning("no spawn points match id, loading at default location");
			spawnPos = playerOne.gameObject;
		}

		playerOne.transform.position = spawnPos.transform.position;
		Camera.main.transform.position = new Vector3(spawnPos.transform.position.x,
		                                             spawnPos.transform.position.y,
		                                             Camera.main.transform.position.z);

		#endregion

		if (!string.IsNullOrEmpty(progress.playerOneName)) {
			//print("Loading values " + progress.playerOneName);
			playerOne.stats.data = playerDataArray[DataIndex(progress.playerOneName)] +
			                       instance.progress.GetPlayerMod(progress.playerOneName);
			playerOne.stats.currHealth = progress.playerOneHealth;
			playerOne.stats.initHealth = false;
			playerOne.Limit = progress.playerOneLimit;
			playerOne.GetComponent<SpriteRenderer>().sprite = playerOne.stats.data.mySprite;

			//print(playerOne.name + " " + playerOne.stats.DataAsPlayer.maxHealth);
		}

		try {
			playerTwo = GameObject.FindGameObjectWithTag("PlayerTwo").GetComponent<Player>();
		} catch (Exception) {
			playerTwo = null;

			if (!SceneManager.GetActiveScene().name.Contains("Base")) {
				print("No p2 gameobject and not in base, turning off companion");
				progress.hasCompanion = false;
			}

			//print($"Player upgradeRecipeTwo wasnt found, setting to null and setting hasCompanion to false - {SceneManager.GetActiveScene().name}");

			//throw;
		}

		if (playerTwo != null) {
			//print("P2 exist");
			playerTwo.transform.position = spawnPos.transform.position;

			if (!progress.hasCompanion) {
				//print( "no companion" );

				playerTwo.gameObject.SetActive(false);
			} else {
				//has companion
				//print( "companion is " + progress.playerTwoName );
				playerTwo.stats.data = playerDataArray[DataIndex(progress.playerTwoName)];

				if (progress.playerTwoHealth <= 0 || progress.isCarrying) {
					playerTwo.gameObject.SetActive(false);
					playerTwo.stats.isKOd = true;
					playerTwo.stats.currHealth = 0;
				} else {
					if (!string.IsNullOrEmpty(progress.playerTwoName)) {
						//print( "Loading values " + progress.playerTwoName );

						// causing load error, not setting but needed for carry
						playerTwo.stats.currHealth = progress.playerTwoHealth;
						playerTwo.stats.initHealth = false;
						playerTwo.Limit = progress.playerTwoLimit;
						playerTwo.GetComponent<SpriteRenderer>().sprite = playerTwo.stats.data.mySprite;
					} else {
						//no data in prgress, use data on the game object itself
					}
				}
			}
		}

		//print($"p1 {playerOne.stats.DataAsPlayer.characterName} p2 {playerTwo.stats.DataAsPlayer.characterName}");

		if (instance.progress.isCarrying) {
			playerOne.carriedObj.SetActive(true);
			playerOne.carriedObj.GetComponent<SpriteRenderer>().sprite = playerDataArray[DataIndex(progress.playerTwoName)].mySprite;

			//Debug.LogError("i dropped my ko, load sprite of " + progress.playerTwoName);
		}

		hud.Init();
	}

	void ChangeMusic(Scene scene) {
		AudioClip nextClip;
		if (musicSource == null) {
			musicSource = GetComponent<AudioSource>();
		}

		//incoming scene
		if (scene.name.Contains("Base") || scene.name.Contains("Waypoint")) {
			//safe zone
			nextClip = safezoneMusic;
		} else if (scene.name.Contains("Dungeon")) {
			//dungen
			nextClip = dungeonMusic;
		} else if (scene.name.Contains("Menu")) {
			if (!scene.name.Contains("Menu_Thank_You")) {
				nextClip = menuMusic;
			} else {
				nextClip = musicSource.clip;
			}

			//menu
		} else if (scene.name.Contains("GameOver")) {
			//gameOver
			nextClip = gameOverMusic;
		} else {
			//levels
			nextClip = overWorldMusic;
		}

		//print(nextClip.name + " is next, curr is " + musicSource.clip.name);
		if (musicSource.clip == null) {
			musicSource.clip = nextClip;
			musicSource.Play();
		} else if (musicSource.clip != nextClip) {
			musicSource.clip = nextClip;
			musicSource.Play();
		}
	}

	public float fadeSpeed = .5f;
	float alpha = 0;
	public Texture2D fadeOutTexture;
	int drawDepth = -1000;
	int fadeDir = -1;

	IEnumerator LoadAfterFade(string levelName) {
		//print( Time.timeScale + " " + instance.GamePaused );

		fadeDir = 1;
		yield return new WaitForEndOfFrame();

		//print( Time.timeScale + " " + instance.GamePaused );

		GameController.instance.GamePaused = true;
		yield return new WaitForSeconds(fadeSpeed);

		//print("after wait");
		SceneManager.LoadScene(levelName);

		fadeDir = -1;
		yield return new WaitForSeconds(fadeSpeed);
		GameController.instance.GamePaused = false;

		yield return null;
	}

	void OnGUI() {
		alpha += fadeDir * fadeSpeed * Time.deltaTime;
		alpha = Mathf.Clamp01(alpha);

		GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
		GUI.depth = drawDepth;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
	}

	#region

	/// <summary>
	/// Pauses the game.
	/// </summary>
	public void PauseGame(bool handleMenu) {
		if (GamePaused) {
			//unpause
			GamePaused = false;
			Time.timeScale = 1;
		} else {
			//pause
			GamePaused = true;
			Time.timeScale = 0;
		}

		hud.gameObject.SetActive(!GamePaused);

		if (handleMenu) {
			pauseMenu.SetActive(GamePaused);
			hud.gameObject.SetActive(!GamePaused);
		}
	}

	/// <summary>
	/// Sets the pause to newState, only use when game needs to be paused but timescale needs to remain in use.
	/// </summary>
	/// <param name="newState">if set to <c>true</c> [new state].</param>
	public void SetPause(bool newState) {
		GamePaused = newState;

		//print("Called");
	}

	/// <summary>
	/// Shows the scene transition prompt.
	/// </summary>
	/// <param name="levelToLoad">The level to load.</param>
	public void ShowSceneTransitionPrompt(string levelToLoad, int spawnid) {
		GameController.instance.PauseGame(false);
		sceneTransitionPrompt.levelToLoad = levelToLoad;
		sceneTransitionPrompt.spawnPointID = spawnid;

		GameController.instance.sceneTransitionPrompt.gameObject.SetActive(true);
	}

	/// <summary>
	/// Hides the scene transition prompt.
	/// </summary>
	public void HideSceneTransitionPrompt() {
		//print("scene trans");
		GameController.instance.sceneTransitionPrompt.gameObject.SetActive(false);
		GameController.instance.pauseMenu.SetActive(false);
		GameController.instance.PauseGame(false);
	}

	void LoadGameOver() {
		print("Game Over!");
		SceneManager.LoadScene(gameoverScene);
	}

	public void OnSwapEventHandler() {
		if (SwapEventHandler != null) {
			SwapEventHandler();
		}
	}

	public void OnGameOverEventHandler() {
		if (GameOverEventHandler != null) {
			GameOverEventHandler();
		}
	}

	#endregion

	bool first, second, third;
	bool loadCheat;

	void GetCheats() {
		if (loadCheat) {
			return;
		}

		//print("called");
		if (Input.GetKey(KeyCode.LeftShift)) {
			//print("in cheat");
			if (Input.GetKeyDown(KeyCode.Alpha1)) {
				first = true;

				//Debug.Log("first");
			}
			if (Input.GetKeyDown(KeyCode.Alpha2)) {
				second = true;

				//Debug.Log( "second" );
			}
			if (Input.GetKeyDown(KeyCode.Alpha3)) {
				third = true;

				//Debug.Log( "third" );
			}
		}

		if (first && second && third) {
			first = second = third = false;
			loadCheat = true;

			//instance.LoadLevel( "LootTestRoom" );
		}
	}
}

[System.Serializable]
public class GameProgress {
	//this has been made a class so that reflection can set the value for recipes. this is a record incase this causes problems later 
	public string levelName = "";
	public int spawnPointId;
	public string playerOneName = "Johnny Basic", playerTwoName = "Abigail C. Rowe";
	public float playerOneHealth = 100, playerTwoHealth = 100, playerOneLimit, playerTwoLimit;
	public BaseData baseData;
	public AbilitySlot playerOneLastCast, playerOneSecondToLastCast, playerTwoLastCast, playerTwoSecondToLastCast;
	public bool isCarrying = false;
	public bool hasCompanion = true;

	public int currPotions = 3, maxPotions = 3;
	public int johnnyConsumable, abbyConsumable, mannyConsumable, valConsumable;

	public int johnnyAbilities, abbyAbilities, mannyAbilities, valAbilities;

	public SpecialConditions specialConditions = new SpecialConditions();

	public static string defaultPlayerName = "Johnny Basic";

	//init with names so that they can be set to by name
	public PlayerMod[] playerMods = {
		new PlayerMod("Johnny Basic"),
		new PlayerMod("Abigail C. Rowe"),
		new PlayerMod("Manny Hart"),
		new PlayerMod("Valorie Leon")
	};

	/// <summary>
	/// used when entering the base with companion active, sets active to jb
	/// </summary>
	public void SwapPlayers() {
		//Debug.Log( "Swap called" );
		var p1 = playerOneName;
		var p1H = playerOneHealth;
		var p1L = playerOneLimit;

		//add ability tracking
		var p1lc = playerOneLastCast;
		var p1slc = playerOneSecondToLastCast;

		playerOneName = playerTwoName;
		playerOneHealth = playerTwoHealth;
		playerOneLimit = playerTwoLimit;
		playerOneLastCast = playerTwoLastCast;
		playerOneSecondToLastCast = playerTwoSecondToLastCast;

		playerTwoName = p1;
		playerTwoHealth = p1H;
		playerTwoLimit = p1L;
		playerTwoLastCast = p1lc;
		playerTwoSecondToLastCast = p1slc;
	}

	public void UpdatePlayerMods(string name, PlayerMod mod) {
		for (int i = 0; i < playerMods.Length; i++) {
			if (playerMods[i].characterName == name) {
				playerMods[i] += mod;
			}
		}
	}

	public PlayerMod GetPlayerMod(string name) {
		//Debug.Log("getting mod for " + name + " "+playerMods.Length);
		for (int i = 0; i < playerMods.Length; i++) {
			//Debug.Log(playerMods[i].characterName);
			if (playerMods[i].characterName == name) {
				return playerMods[i];
			}
		}

		//Debug.Log("Couldnt find mod");
		return new PlayerMod();
	}

	public override string ToString() {
		return $"Level: {levelName} Player: {playerOneName} {playerOneHealth} {playerOneLimit} " +
		       $"Companion: {playerTwoName} {playerTwoHealth} {playerTwoLimit}";
	}
}

public class SpecialConditions {
	public bool secretOne, secretTwo, secretThree, dungeonOne, dungeonTwo, dungeonThree;
}