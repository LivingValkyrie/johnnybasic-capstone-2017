﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PauseMenu
/// </summary>
public class PauseMenu : MonoBehaviour {
	#region Fields

	#endregion

	/// <summary>
	/// called by close button
	/// </summary>
	public void ButtonClose() {
		GameController.instance.PauseGame(true);
	}

	/// <summary>
	/// Listens for escape to unpause game
	/// </summary>
	void Update() {
		if (Input.GetButtonDown("Pause")) {
			GameController.instance.PauseGame(true);
		}
	}
}