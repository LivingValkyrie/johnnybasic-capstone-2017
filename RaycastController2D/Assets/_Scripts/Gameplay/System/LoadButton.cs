﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: LoadButton is a basic script that shows save data in a text object
/// </summary>
public class LoadButton : MonoBehaviour {
	#region Fields

	//the slot this button will show data for.
	public SaveSlot slot;
	//the text that the slots data will be shown in
	public Text text;

	#endregion

	/// <summary>
	/// sets the text to that of the slots data
	/// </summary>
	public void Start() {
		text.text = SaveDataBackendHandler.GetData((int)slot);
	}

	
}