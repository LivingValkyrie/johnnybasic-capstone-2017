﻿using System;
using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: RoomUpgradeRecipe
/// </summary>
[CreateAssetMenu]
public class UpgradeRecipe : ScriptableObject {
	#region Fields

	public RecipeType type;
	public RecipeEntry[] itemsRequired;

	//[Tooltip("This is the name of the requirement boolean. these must be a single word." +
	//         " ask the programmers to create these values behind the scenes for you.")]
	//public string specialRequirement;
	public static int maxRecipeItems = 4;
	

	[Header("Player playerMod for room upgrade")]
	public PlayerMod playerMod;

	[Header("Ability upgrade stuff")]
	public string upgradeName;
	public string description;

	#endregion

	//bool CheckSpecialRequirement() {
	//	if (string.IsNullOrEmpty(specialRequirement)) {
	//		//returns true if no special requiremnet
	//		return true;
	//	}

	//	// using reflection this gets the field by string then gets the value of that field. this will error in its current state
	//	// if the field does not exist. it is also using the gamecontroller. this will be updated with its own data type that will
	//	// be used just for these event flags
	//	return (bool) typeof(SpecialConditions).GetField(specialRequirement).GetValue(GameController.instance.progress.specialConditions);
	//}

	public bool CraftRecipe(CraftPrompt prompt, PlayerData data = null, AbilitySlot abilitySlot = 0, ModSlot modSlot = 0 ) {
		//if (!CheckSpecialRequirement()) {
		//	return false;
		//}

		//this will test for recipe type, then return the proper method
		switch (type) {
			case RecipeType.Consumable:
				return IncreaseConsumableCount(data);
			case RecipeType.SkillUpgrade:
				return UpgradeSkill(data, abilitySlot, modSlot);
			case RecipeType.RoomUpgrade:
				return UpgradeRoom(prompt.room);
			default:
				return false;
		}
	}

	bool UpgradeSkill( PlayerData data, AbilitySlot abilitySlot , ModSlot modSlot ) {
		//Debug.LogWarning("Upgrading of skills not yet supported");
		//character &= modslot << ability
		if ( string.IsNullOrEmpty( data.abilityMods ) ) {
			//returns false as there was no increment
			return false;
		}

		int currMods = (int)typeof( GameProgress ).GetField( data.abilityMods ).GetValue( GameController.instance.progress );


		int modSlotBits;
		switch (modSlot) {
			case ModSlot.First:
				modSlotBits = (int)AbilityModding.UnlockedOneOnly;
				break;
			case ModSlot.Second:
				modSlotBits = (int) AbilityModding.UnlockedTwoOnly;
				break;
			case ModSlot.Third:
				modSlotBits = (int) AbilityModding.UnlockedThreeOnly;
				break;
			default:
				modSlotBits = (int) AbilityModding.UnlockedNone;
				break;
		}

		int toShift;
		switch (abilitySlot) {
			case AbilitySlot.AttackMain:
				toShift = (int) AbilityModding.ShiftToFirst;
				break;
			case AbilitySlot.AttackSecondary:
				toShift = (int)AbilityModding.ShiftToSecond;
				break;
			case AbilitySlot.First:
				toShift = (int)AbilityModding.ShiftToThird;
				break;
			case AbilitySlot.Second:
				toShift = (int)AbilityModding.ShiftToFourth;
				break;
			case AbilitySlot.Third:
				toShift = (int)AbilityModding.ShiftToFifth;
				break;
			default:
				throw new ArgumentOutOfRangeException(abilitySlot.ToString(), abilitySlot, null);
		}


		Debug.Log("Upgrade");
		//Debug.Log(currMods);
		//Debug.Log( modSlotBits );
		//Debug.Log( toShift );

		Debug.Log( "curr" + Convert.ToString( currMods, 2 ) );
		Debug.Log( "modSlot" + Convert.ToString( modSlotBits, 2 ) );
		Debug.Log( "toShift" + Convert.ToString( toShift, 2 ) );

		currMods |= modSlotBits << toShift;

		Debug.Log( Convert.ToString( currMods, 2 ) + " after shift");

		//Debug.Log(currCount);
		typeof( GameProgress ).GetField( data.abilityMods ).SetValue( GameController.instance.progress, currMods );
		data.Init();
		return true;
	}

	bool UpgradeRoom(BaseRoom room) {
		//Debug.LogWarning("Upgrading of rooms not yet supported via recipe");
		room.state++;
		room.UpdateRoomState();
		GameController.instance.progress.UpdatePlayerMods(room.GetNameByRoomId(), playerMod);
		return false;
	}

	bool IncreaseConsumableCount(PlayerData data) {
		if (string.IsNullOrEmpty(data.consumableName)) {
			//returns false as there was no increment
			return false;
		}

		int currCount = (int) typeof(GameProgress).GetField(data.consumableName).GetValue(GameController.instance.progress);

		//Debug.Log(currCount);
		typeof(GameProgress).GetField(data.consumableName).SetValue(GameController.instance.progress, currCount + 1);
		return true;
	}

}

[System.Serializable]
public class RecipeEntry {
	public InventoryItem item;
	public int required;
}

public enum RecipeType {
	Consumable,
	SkillUpgrade,
	RoomUpgrade
}