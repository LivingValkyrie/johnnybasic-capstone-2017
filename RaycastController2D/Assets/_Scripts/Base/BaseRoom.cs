﻿using System;
using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: BaseRoom
/// </summary>
[DisallowMultipleComponent]
public class BaseRoom : MonoBehaviour {
	#region Fields

	public BaseRoomID roomId;
	[HideInInspector]
	public int state = 0;
	public Sprite[] roomSprites;

	#endregion

	void Awake() {
		switch (roomId) {
			case BaseRoomID.JohnnysRoom:
				state = GameController.instance.progress.baseData.johnnysRoom;
				break;
			case BaseRoomID.AbigalesRoom:
				state = GameController.instance.progress.baseData.abigalesRoom;
				break;
			case BaseRoomID.MannysRoom:
				state = GameController.instance.progress.baseData.mannysRoom;
				break;
			case BaseRoomID.ValoriesRoom:
				state = GameController.instance.progress.baseData.valoriesRoom;
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}

		GetComponent<SpriteRenderer>().sprite = roomSprites[state];
	}

	public void UpdateRoomState() {
		switch (roomId) {
			case BaseRoomID.JohnnysRoom:
				GameController.instance.progress.baseData.johnnysRoom++;
				break;
			case BaseRoomID.AbigalesRoom:
				GameController.instance.progress.baseData.abigalesRoom++;
				break;
			case BaseRoomID.MannysRoom:
				GameController.instance.progress.baseData.mannysRoom++;
				break;
			case BaseRoomID.ValoriesRoom:
				GameController.instance.progress.baseData.valoriesRoom++;
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}

		Awake();
	}

	public string GetNameByRoomId() {
		switch (roomId) {
			case BaseRoomID.JohnnysRoom:
				return "Johnny Basic";
			case BaseRoomID.AbigalesRoom:
				return "Abigail C. Rowe";
			case BaseRoomID.MannysRoom:
				return "Manny Hart";
			case BaseRoomID.ValoriesRoom:
				return "Valorie Leon";
			default:
				Debug.LogError("roomid out of range");
				return "";
		}
	}
}


public enum BaseRoomID {
	JohnnysRoom,
	AbigalesRoom,
	MannysRoom,
	ValoriesRoom

}