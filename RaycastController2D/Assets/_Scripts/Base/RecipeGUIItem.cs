﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: RecipeGUIItem
/// </summary>
public class RecipeGUIItem : MonoBehaviour {
	#region Fields

	public Text itemName, needed, have;

	#endregion
}