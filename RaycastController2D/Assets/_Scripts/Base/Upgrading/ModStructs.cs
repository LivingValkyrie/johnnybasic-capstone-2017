﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct PlayerMod {
	public string characterName;
	public int maxHealthIncrease;
	public float defenseIncrease;
	public float armorIncrease;

	public PlayerMod(string name) {
		characterName = name;
		maxHealthIncrease = 0;
		defenseIncrease = armorIncrease = 0;
	}

	public static PlayerMod operator +(PlayerMod m1, PlayerMod m2) {
		var mod = new PlayerMod(m1.characterName) {
			armorIncrease = m1.armorIncrease + m2.armorIncrease,
			defenseIncrease = m1.defenseIncrease + m2.defenseIncrease,
			maxHealthIncrease = m1.maxHealthIncrease + m2.maxHealthIncrease
		};

		//Debug.Log("\n" + m1 + "\n" + m2 + "\n" + mod);
		return mod;
	}

	public override string ToString() {
		return $"{characterName} {maxHealthIncrease} {defenseIncrease} {armorIncrease}";
	}
}

[System.Serializable]
public struct AbilityMod {
	public int atkDmg;
	public float atkSpd;
	public float atkRng;
	public float cooldown;
	public float limitGaugeModifier;

	public UpgradeRecipe recipe;

	public static AbilityMod operator +(AbilityMod m1, AbilityMod m2) {
		var mod = new AbilityMod() {
			atkDmg = m1.atkDmg + m2.atkDmg,
			atkSpd = m1.atkSpd + m2.atkSpd,
			atkRng = m1.atkRng + m2.atkDmg,
			cooldown = m1.cooldown + m2.cooldown,
			limitGaugeModifier = m1.limitGaugeModifier + m2.limitGaugeModifier
		};

		return mod;
	}

	public override string ToString() {
		return $" {atkDmg} {atkRng} {atkSpd} {cooldown} {limitGaugeModifier}";
	}
}

public enum AbilityModding {
	UnlockedNone = 0,
	UnlockedThreeOnly = 1,
	UnlockedTwoOnly = 2,
	UnlockedTwoAndThree = 3,
	UnlockedOneOnly = 4,
	UnlockedOneAndThree = 5,
	UnlockedOneAndTwo = 6,
	UnlockedAll = 7,

	MaskFirst = 28672,
	MaskedSecond = 3584,
	MaskThird = 448,
	MaskFourth = 56,
	MaskFifth = 7,

	ShiftToFirst = 12,
	ShiftToSecond = 9,
	ShiftToThird = 6,
	ShiftToFourth = 3,
	ShiftToFifth = 0
}

public enum ModSlot {
	First,
	Second,
	Third
}

[System.Serializable]
public struct BaseData {
	// have NPC set room to 1 when acquiring them for the first time. prolly wont need for magazine demo.
	//range of 0-3, 0 is unopened
	public int johnnysRoom, abigalesRoom, mannysRoom, valoriesRoom;

	//base objects will update these values when upgraded which will then be saved.
	public void UnlockRoom(Character character) {
		switch (character) {
			case Character.JB:
				johnnysRoom = Mathf.Max(johnnysRoom, 1);
				break;
			case Character.AR:
				abigalesRoom = Mathf.Max(abigalesRoom, 1);

				break;
			case Character.MH:
				mannysRoom = Mathf.Max(mannysRoom, 1);

				break;
			case Character.VL:
				valoriesRoom = Mathf.Max(valoriesRoom, 1);

				break;
		}
	}

	public int GetRoomState(Character character) {
		int toReturn = 0;
		switch ( character ) {
			case Character.JB:
				toReturn = johnnysRoom;
				break;
			case Character.AR:
				toReturn = abigalesRoom;

				break;
			case Character.MH:
				toReturn = mannysRoom;

				break;
			case Character.VL:
				toReturn = valoriesRoom;

				break;
		}

		return toReturn;
	}

	public enum Character {
		JB,
		AR,
		MH,
		VL
	}
}