﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: NPCAbilityUpgradePrompt
/// </summary>
public class NPCAbilityUpgradePrompt : PromptFourChoice {
	#region Fields

	public PlayerData data;
	public PlayerAbility ability;
	public CraftPrompt craftPrompt;
	public AbilitySlot slot;

	#endregion

	void OnEnable() {
		choiceFour.gameObject.SetActive(false);
		promptText.text = ability.abilityName;

		//these turn off the buttons if the mod does not exist or is default value, maning it doesnt have anything of value
		if (ability.upgradeOne.Equals(default(AbilityMod))) {
			choiceOne.gameObject.SetActive(false);
		} else {
			choiceOne.GetComponentInChildren<Text>().text = ability.upgradeOne.recipe.upgradeName;
		}
		if (ability.upgradeTwo.Equals(default(AbilityMod))) {
			choiceTwo.gameObject.SetActive(false);
		} else {
			choiceTwo.GetComponentInChildren<Text>().text = ability.upgradeTwo.recipe.upgradeName;
		}
		if (ability.upgradeThree.Equals(default(AbilityMod))) {
			choiceThree.gameObject.SetActive(false);
		} else {
			choiceThree.GetComponentInChildren<Text>().text = ability.upgradeThree.recipe.upgradeName;
		}

		TurnOffActiveMods();
		//print("upgrade prompt slot " + slot);
	}

	void TurnOffActiveMods() {
		int currMods = (int) typeof(GameProgress).GetField(data.abilityMods).GetValue(GameController.instance.progress);

		int toShift;
		int mask;
		switch (slot) {
			case AbilitySlot.AttackMain:
				toShift = (int) AbilityModding.ShiftToFirst;
				mask = (int) AbilityModding.MaskFirst;
				break;
			case AbilitySlot.AttackSecondary:
				toShift = (int) AbilityModding.ShiftToSecond;
				mask = (int) AbilityModding.MaskedSecond;
				break;
			case AbilitySlot.First:
				toShift = (int) AbilityModding.ShiftToThird;
				mask = (int) AbilityModding.MaskThird;
				break;
			case AbilitySlot.Second:
				toShift = (int) AbilityModding.ShiftToFourth;
				mask = (int) AbilityModding.MaskFourth;
				break;
			case AbilitySlot.Third:
				toShift = (int) AbilityModding.ShiftToFifth;
				mask = (int) AbilityModding.MaskFifth;
				break;
			default:
				toShift = 0;
				mask = 0;
				break;
		}

		currMods &= mask;
		currMods = currMods >> toShift;
		AbilityModding mods = (AbilityModding) currMods;

		switch (mods) {
			case AbilityModding.UnlockedNone:
				break;
			case AbilityModding.UnlockedThreeOnly:
				choiceThree.gameObject.GetComponentInChildren<Text>().text = "Purchased";
				choiceThree.interactable = false;
				break;
			case AbilityModding.UnlockedTwoOnly:
				choiceTwo.gameObject.GetComponentInChildren<Text>().text = "Purchased";
				choiceTwo.interactable = false;
				break;
			case AbilityModding.UnlockedTwoAndThree:
				choiceTwo.gameObject.GetComponentInChildren<Text>().text = "Purchased";
				choiceThree.gameObject.GetComponentInChildren<Text>().text = "Purchased";
				choiceThree.interactable = false;
				choiceTwo.interactable = false;

				break;
			case AbilityModding.UnlockedOneOnly:
				choiceOne.gameObject.GetComponentInChildren<Text>().text = "Purchased";
				choiceOne.interactable = false;
				break;
			case AbilityModding.UnlockedOneAndThree:
				choiceOne.gameObject.GetComponentInChildren<Text>().text = "Purchased";
				choiceThree.gameObject.GetComponentInChildren<Text>().text = "Purchased";
				choiceThree.interactable = false;
				choiceOne.interactable = false;


				break;
			case AbilityModding.UnlockedOneAndTwo:
				choiceOne.gameObject.GetComponentInChildren<Text>().text = "Purchased";
				choiceTwo.gameObject.GetComponentInChildren<Text>().text = "Purchased";
				choiceTwo.interactable = false;
				choiceOne.interactable = false;

				break;
			case AbilityModding.UnlockedAll:
				choiceOne.gameObject.GetComponentInChildren<Text>().text = "Purchased";
				choiceTwo.gameObject.GetComponentInChildren<Text>().text = "Purchased";
				choiceThree.gameObject.GetComponentInChildren<Text>().text = "Purchased";
				choiceThree.interactable = false;
				choiceTwo.interactable = false;
				choiceOne.interactable = false;
				break;
		}
	}

	public override void ButtonNo() {
		gameObject.SetActive(false);
		GameController.instance.PauseGame(false);
	}

	public override void ChoiceOne() {
		craftPrompt.data = data;
		craftPrompt.recipe = ability.upgradeOne.recipe;
		craftPrompt.abSlot = slot;
		craftPrompt.modSlot = ModSlot.First;

		gameObject.SetActive(false);
		craftPrompt.gameObject.SetActive(true);
		craftPrompt.repeatable = false;
	}

	public override void ChoiceTwo() {
		craftPrompt.data = data;
		craftPrompt.recipe = ability.upgradeTwo.recipe;
		craftPrompt.abSlot = slot;
		craftPrompt.modSlot = ModSlot.Second;

		gameObject.SetActive(false);
		craftPrompt.gameObject.SetActive(true);
		craftPrompt.repeatable = false;
	}

	public override void ChoiceThree() {
		craftPrompt.data = data;
		craftPrompt.recipe = ability.upgradeThree.recipe;
		craftPrompt.abSlot = slot;
		craftPrompt.modSlot = ModSlot.Third;

		gameObject.SetActive(false);
		craftPrompt.gameObject.SetActive(true);
		craftPrompt.repeatable = false;
	}

	public override void ButtonYes() {
		throw new System.NotImplementedException();
	}

	public override void ChoiceFour() {
		throw new System.NotImplementedException();
	}
}