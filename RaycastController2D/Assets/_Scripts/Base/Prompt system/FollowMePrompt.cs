﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: FollowMePrompt
/// </summary>
public class FollowMePrompt : Prompt {
	#region Fields

	public PlayerData data;

	public Text defenseText, armorText, healthText, movementSpecialText;
	public Image abilityOne, abilityTwo, abilityThree;

	#endregion

	void OnEnable() {
		//print(data.characterName);
		data += GameController.instance.progress.GetPlayerMod(data.characterName);

		if (!GameController.instance.progress.hasCompanion) {
			buttonYes.GetComponentInChildren<Text>().text = "Follow Me";
		} else {
			//has companion, find out if its you
			if (GameController.instance.progress.playerTwoName == data.characterName) {
				buttonYes.GetComponentInChildren<Text>().text = "Stay here";
			} else {
				buttonYes.GetComponentInChildren<Text>().text = "Follow Me";
			}
		}

		if (data.characterName == GameController.instance.progress.playerOneName) {
			buttonYes.gameObject.SetActive(false);
		}

		//populate data
		promptText.text = data.characterName;

		abilityOne.sprite = data.abilityOne.icon;
		abilityTwo.sprite = data.abilityTwo.icon;
		abilityThree.sprite = data.abilityThree.icon;

		defenseText.text = $"Defense: {data.defense}";
		armorText.text = $"Armor: {data.armor}";
		healthText.text = $"Max Health: {data.maxHealth}";

		movementSpecialText.gameObject.SetActive( true );

		if ( data.canMultiJump) {
			movementSpecialText.text = $"Special: Multijump {data.maxMultiJump}";
		} else if (data.canAirDash) {
			movementSpecialText.text = $"Special: Air Dash";
		} else if (data.canWallJump) {
			movementSpecialText.text = $"Special: Wall Jump";
		} else {
			movementSpecialText.gameObject.SetActive(false);
		}
	}

	public override void ButtonNo() {
		gameObject.SetActive(false);

		GameController.instance.PauseGame(false);
	}

	public override void ButtonYes() {
		//follow/unfollow
		if (!GameController.instance.progress.hasCompanion) {
			//follow
			GameController.instance.progress.hasCompanion = true;
			GameController.instance.progress.playerTwoName = data.characterName;
		} else {
			if (GameController.instance.progress.playerTwoName == data.characterName) {
				//unfollow
				GameController.instance.progress.hasCompanion = false;
				GameController.instance.progress.playerTwoName = "";
			} else {
				//follow
				GameController.instance.progress.hasCompanion = true;
				GameController.instance.progress.playerTwoName = data.characterName;
			}
		}

		//OnEnable();
		ButtonNo();
		GameController.instance.hud.Init();
	}
}