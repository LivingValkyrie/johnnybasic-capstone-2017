﻿using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PromptFourChoice
/// </summary>
public abstract class PromptFourChoice : Prompt {
	#region Fields

	public Button choiceOne, choiceTwo, choiceThree, choiceFour;

	#endregion

	public abstract void ChoiceOne();
	public abstract void ChoiceTwo();
	public abstract void ChoiceThree();
	public abstract void ChoiceFour();

}