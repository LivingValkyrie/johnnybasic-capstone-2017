﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Prompt
/// </summary>
public abstract class Prompt : MonoBehaviour {
	#region Fields

	public Text promptText;
	public Button buttonNo, buttonYes;

	#endregion

	public abstract void ButtonNo();

	public abstract void ButtonYes();



}