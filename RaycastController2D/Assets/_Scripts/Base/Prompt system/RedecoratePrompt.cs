﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: YesNoPromt
/// </summary>
public class RedecoratePrompt : Prompt {
	#region Fields

	public CharacterData data;
	public BaseRoom room;

	#endregion

	void Start() {}

	void OnEnable() {
		switch (room.state) {
			case 0:
				promptText.text = "Uh, i shouldnt be here. the room hasn't been unlocked, which means i haven't either.";
				buttonYes.gameObject.SetActive(false);
				break;
			case 1:
				promptText.text = "Well, this room is really bland, want to help me improve it?";
				buttonYes.gameObject.SetActive(true);
				break;
			case 2:
				promptText.text = "this is nice, but i think it could be better...";
				buttonYes.gameObject.SetActive(true);
				break;
			case 3:
				promptText.text = "I appreciate all the help, now my room is perfect!";
				buttonYes.gameObject.SetActive(false);
				break;
			default:
				promptText.text = "IDK what the hell you did with my room, but lets just leave it alone.";
				print($"Room state for {room.gameObject.name} is out of range with a score of {room.state}, it should be 0-3.");
				buttonYes.gameObject.SetActive(false);
				break;
		}
	}

	public override void ButtonNo() {
		gameObject.SetActive(false);

		GameController.instance.PauseGame(false);
	}

	public override void ButtonYes() {
		print("Check recipe then if successful increase state. for initial version will skip recipe checking.");
		room.state++;
		room.UpdateRoomState();

		gameObject.SetActive(false);

		GameController.instance.PauseGame(false);
	}
}