﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: NPCAbilityListPrompt
/// </summary>
public class NPCAbilityListPrompt : PromptFourChoice {
	#region Fields

	public PlayerData data;
	public NPCAbilityUpgradePrompt upgradePrompt;

	#endregion

	void OnEnable() {
		promptText.text = "What would you like to improve?";
		choiceOne.GetComponentInChildren<Text>().text = data.abilityOne.abilityName;
		choiceTwo.GetComponentInChildren<Text>().text = data.abilityTwo.abilityName;
		choiceThree.GetComponentInChildren<Text>().text = data.abilityThree.abilityName;
		choiceFour.gameObject.SetActive(false);



	}

	public override void ButtonNo() {
		gameObject.SetActive(false);
		GameController.instance.PauseGame(false);
	}

	public override void ChoiceOne() {
		upgradePrompt.data = data;
		upgradePrompt.ability = data.abilityOne;
		upgradePrompt.slot = AbilitySlot.First;

		gameObject.SetActive(false);
		upgradePrompt.gameObject.SetActive(true);
	}

	public override void ChoiceTwo() {
		upgradePrompt.data = data;
		upgradePrompt.ability = data.abilityTwo;
		upgradePrompt.slot = AbilitySlot.Second;

		gameObject.SetActive(false);
		upgradePrompt.gameObject.SetActive(true);
	}

	public override void ChoiceThree() {
		upgradePrompt.data = data;
		upgradePrompt.ability = data.abilityThree;
		upgradePrompt.slot = AbilitySlot.Third;

		gameObject.SetActive(false);
		upgradePrompt.gameObject.SetActive(true);
	}

	public override void ButtonYes() {
		throw new System.NotImplementedException();
	}

	public override void ChoiceFour() {
		throw new System.NotImplementedException();
	}
}