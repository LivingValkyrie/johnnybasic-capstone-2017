﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: NPCMainMenuPrompt
/// </summary>
public class NPCMainMenuPrompt : PromptFourChoice {
	#region Fields

	public PlayerData data;
	public BaseRoom room;
	public CraftPrompt craftPrompt;
	public NPCAbilityListPrompt listPrompt;
	public FollowMePrompt followPrompt;

	#endregion

	void OnEnable() {
		promptText.text = "How can i help?";

		choiceOne.GetComponentInChildren<Text>().text = "Follow Me";

		//turn off follow me for johnny basic
		if ( data.characterName == GameController.instance.progress.playerOneName ) {
			choiceOne.GetComponentInChildren<Text>().text = "About Me";
		}
	}

	public override void ButtonNo() {
		gameObject.SetActive(false);

		GameController.instance.PauseGame(false);
	}

	public override void ChoiceOne() {
		//print(data.characterName);
		followPrompt.data = data;
		followPrompt.gameObject.SetActive(true);
		gameObject.SetActive(false);
	}

	public override void ChoiceTwo() {
		//redecorate
		craftPrompt.data = data;
		craftPrompt.room = room;
		craftPrompt.repeatable = false;

		switch ( room.state ) {
			case 0:
				craftPrompt.promptText.text = "Uh, i shouldnt be here. the room hasn't been unlocked, which means i haven't either.";
				craftPrompt.buttonYes.gameObject.SetActive( false );
				break;
			case 1:
				craftPrompt.promptText.text = "Well, this room is really bland, want to help me improve it?";

				craftPrompt.recipe = data.firstRoomUpgrade;
				craftPrompt.buttonYes.gameObject.SetActive( true );
				break;
			case 2:
				craftPrompt.promptText.text = "this is nice, but i think it could be better...";
				craftPrompt.recipe = data.secondRoomUpgrade;
				craftPrompt.buttonYes.gameObject.SetActive( true );
				break;
			case 3:
				craftPrompt.promptText.text = "I appreciate all the help, now my room is perfect!";
				craftPrompt.recipe = null;
				craftPrompt.buttonYes.gameObject.SetActive( false );
				break;
			default:
				craftPrompt.promptText.text = "IDK what the hell you did with my room, but lets just leave it alone.";
				print( $"Room state for {room.gameObject.name} is out of range with a score of {room.state}, it should be 0-3." );
				craftPrompt.buttonYes.gameObject.SetActive( false );
				break;
		}

		craftPrompt.gameObject.SetActive(true);
		gameObject.SetActive(false);
	}

	public override void ChoiceThree() {
		//upgrade
		listPrompt.data = data;
		listPrompt.gameObject.SetActive(true);
		gameObject.SetActive(false);
	}

	public override void ChoiceFour() {
		//craft
		craftPrompt.data = data;
		craftPrompt.recipe = data.consumableRecipe;
		craftPrompt.repeatable = true;
		craftPrompt.gameObject.SetActive(true);
		gameObject.SetActive(false);
	}

	public override void ButtonYes() {}
}