﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: CraftPrompt
/// </summary>
public class CraftPrompt : Prompt {
	#region Fields

	public PlayerData data;
	public RecipeGUIItem[] recipeGuiItems;
	public BaseRoom room;
	public UpgradeRecipe recipe;
	public bool repeatable = true;

	public AbilitySlot abSlot;
	public ModSlot modSlot;

	#endregion

	void OnEnable() {
		//print(recipe);
		foreach (RecipeGUIItem item in recipeGuiItems) {
			item.gameObject.SetActive(false);
		}

		if (recipe != null) {
			switch (recipe.type) {
				case RecipeType.Consumable:
					promptText.text = $"{data.consumableRecipe.name} {data.GetConsumableCount()}";
					break;
				case RecipeType.RoomUpgrade:

					break;
				case RecipeType.SkillUpgrade:
					promptText.text = recipe.description;
					break;
			}

			//set up recipe items
			for (int i = 0; i < recipe.itemsRequired.Length && i < UpgradeRecipe.maxRecipeItems; i++) {
				RecipeEntry entry = recipe.itemsRequired[i];
				if (entry.item == null) {
					//print("default");
					continue;
				}

				//print("I :" +  i + " " + entry);
				recipeGuiItems[i].itemName.text = entry.item.itemName;
				recipeGuiItems[i].needed.text = entry.required.ToString();

				int value;
				GameController.instance.inventory.baseItems.TryGetInventoryItem(entry.item, out value);

				//print( "value " + value + " tryGet " + GameController.instance.inventory.baseItems.TryGetInventoryItem( entry.item, out value ));

				recipeGuiItems[i].have.text = value.ToString();
				recipeGuiItems[i].gameObject.SetActive(true);
			}
		} else {
			foreach (RecipeGUIItem guiItem in recipeGuiItems) {
				guiItem.enabled = false;
			}
		}

		//print( "craft prompt slot " + abSlot );
		//print( "craft prompt mod slot " + modSlot );

	}

	public override void ButtonYes() {
		//print("Check recipe then if successful increase consumable count.");
		if (GameController.instance.inventory.TestRecipe(recipe)) { //this line does the inventory check
			recipe.CraftRecipe(this, data, abSlot, modSlot);

			if ( repeatable ) {
				OnEnable();
			} else {
				ButtonNo();
			}
		} else {
			print("Failed to craft");
		}
		
	}

	public override void ButtonNo() {
		gameObject.SetActive(false);

		GameController.instance.PauseGame(false);
	}
}