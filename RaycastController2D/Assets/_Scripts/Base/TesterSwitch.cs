﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: TesterSwitch
/// </summary>
public class TesterSwitch : SimpleSwitch {
	#region Fields

	#endregion

	public override void OnActivate() {
		print("Found");
	}

	public override void OnDeactivate() {
		throw new System.NotImplementedException();
	}
}