﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: EnableObjectSwitch
/// </summary>
public class EnableObjectSwitch : SimpleSwitch {
	#region Fields

	public GameObject objToEnable;

	#endregion

	public override void OnActivate() {
		objToEnable.SetActive(true);
	}

	public override void OnDeactivate() {
		objToEnable.SetActive( false );
	}
}