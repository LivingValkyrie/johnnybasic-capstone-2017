﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: JunkPile
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class JunkPile : MonoBehaviour {
	#region Fields

	public Sprite[] junkSprites;
	public int itemsPerLevel = 50;

	#endregion

	void Start() {
		int temp = 0;

		foreach (var item in GameController.instance.inventory.baseItems) {
			temp += item.Value;
		}


		int toShow = (temp / itemsPerLevel);
		if (toShow >= junkSprites.Length) {
			//print("Toshow over max, setting to max - 1");
			toShow = junkSprites.Length - 1;
		}else if (toShow <= 0) {
			//print( "Toshow is zero, setting to max - 1" );
			toShow = 0;
		}

		//print( temp + " " + toShow );

		GetComponent<SpriteRenderer>().sprite = junkSprites[toShow];
	}

}