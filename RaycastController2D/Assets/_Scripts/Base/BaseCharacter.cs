﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: BaseCharacter
/// </summary>
[DisallowMultipleComponent]
public class BaseCharacter : SimpleSwitch {
	#region Fields

	//public BaseRoomID id;
	public BaseRoom room;
	public PlayerData npcData;

	public NPCMainMenuPrompt prompt;

	public bool debug = true;

	#endregion

	void Start() {
		if ( room.state <= 0 ) {
			gameObject.SetActive( false );
		}

		if (debug) {
			//Debug.LogWarning("in debug. npc will load as if the room is already unlocked.");
			gameObject.SetActive(true);
		}
	}

	public override void OnActivate() {
		prompt.data = npcData;
		prompt.room = room;
		prompt.gameObject.SetActive(true);
		GameController.instance.PauseGame(false);
	}

	public override void OnDeactivate() {
		Toggle(activator);
	}

}

