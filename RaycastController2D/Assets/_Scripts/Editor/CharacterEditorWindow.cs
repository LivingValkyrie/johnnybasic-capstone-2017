﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// @author Mike Dobson
/// </summary>
public class CharacterEditorWindow : EditorWindow {

    #region ToolVariables
    public List<PlayerData> characterList = new List<PlayerData>();
    public List<string> characterNameList = new List<string>();
    public string[] characterNameArray;

    //editor variables
    int currentChoice = 0;
    int lastChoice = 0;

    //character variables
    string windowName = "";
    Sprite sprite = null;
    AnimatorOverrideController animator = null;
    int health = 1;
    float defence = 0;
    string[] jumpStyles = new string[] { "Double Jump", "Wall Jump", "Air Dash" };
    int jumpStyleChoice = 0;
    bool doubleJump = false;
    bool wallJump = false;
    bool airDash = false;
    float engagementRadius = 1f;
    PlayerAbility abilityMain = null;
    PlayerAbility abilitySecondary = null;
    PlayerAbility abilityOne = null;
    PlayerAbility abilityTwo = null;
    PlayerAbility abilityThree = null;
    PlayerAbility limitBreak = null;
    float limitBreakGaugeSize = 100f;

    //foldouts
    bool characterData = false;
    bool companionData = false;
    bool abilitiesData = false;

    //error flags
    //bool nameFlag = false;
    //bool spriteFlag = false;

    #endregion

    [MenuItem("Custom Tools/Character Tool")]
    private static void initialization()
    {
        EditorWindow.GetWindow<CharacterEditorWindow>();
    }

    void Awake()
    {
        GetCharacters();
        populateUI();
    }

    void OnGUI()
    {
        //draw the dropdown menu for character choice
        GUIContent content = new GUIContent();
        currentChoice = EditorGUILayout.Popup(currentChoice, characterNameArray);

        EditorGUILayout.Space();

        if(sprite != null)
        {
            GUIStyle spriteStyle = new GUIStyle();
            spriteStyle.fixedHeight = 90;
            spriteStyle.fixedWidth = 90;

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            Texture2D tempTexture = AssetPreview.GetAssetPreview(sprite);
            GUILayout.Label(tempTexture, spriteStyle);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        sprite = EditorGUILayout.ObjectField(sprite, typeof(Sprite), false) as Sprite;
        EditorGUILayout.Space();
        EditorGUILayout.LabelField(windowName);
        EditorGUILayout.Space();
        content.tooltip = "The animator that will run this character animations";
        content.text = "Animator: ";
        animator = EditorGUILayout.ObjectField(content, animator, typeof(AnimatorOverrideController), false) as AnimatorOverrideController;

        //character data block
        characterData = EditorGUILayout.Foldout(characterData, "Basic Character Info: ");
        if(characterData)
        {
            content.tooltip = "The maximum health the character will have";
            content.text = "Health: ";
            health = EditorGUILayout.IntSlider(content, Mathf.Max(1, health), 1, 250);
            content.tooltip = "The percent of damage reduction the character will have";
            content.text = "Defense: ";
            defence = EditorGUILayout.Slider(content, Mathf.Max(1, defence), 1, 100);
            jumpStyleChoice = GUILayout.SelectionGrid(jumpStyleChoice, jumpStyles, 3);
            switch(jumpStyleChoice)
            {
                case 0:
                    doubleJump = true;
                    wallJump = false;
                    airDash = false;
                    break;
                case 1:
                    doubleJump = false;
                    wallJump = true;
                    airDash = false;
                    break;
                case 2:
                    doubleJump = false;
                    wallJump = false;
                    airDash = true;
                    break;
            }
        }

        //Companion specific information block
        EditorGUILayout.Space();
        companionData = EditorGUILayout.Foldout(companionData, "Companion Specific Info: ");
        if(companionData)
        {
            content.tooltip = "The radius at which the character will engage enemies in combat";
            content.text = "Engagement Radius: ";
            engagementRadius = EditorGUILayout.Slider(content, Mathf.Max(0, engagementRadius), 0, 30);
        }

        //Abilities block
        EditorGUILayout.Space();
        abilitiesData = EditorGUILayout.Foldout(abilitiesData, "Abilities: ");
        if(abilitiesData)
        {
            abilityMain = EditorGUILayout.ObjectField("Main Attack:", abilityMain, typeof(PlayerAbility), false) as PlayerAbility;
            abilitySecondary = EditorGUILayout.ObjectField("Secondary Attack:", abilitySecondary, typeof(PlayerAbility), false) as PlayerAbility;
            abilityOne = EditorGUILayout.ObjectField("Ability One:", abilityOne, typeof(PlayerAbility), false) as PlayerAbility;
            abilityTwo = EditorGUILayout.ObjectField("Ability Two:", abilityTwo, typeof(PlayerAbility), false) as PlayerAbility;
            abilityThree = EditorGUILayout.ObjectField("Ability Three:", abilityThree, typeof(PlayerAbility), false) as PlayerAbility;
            limitBreak = EditorGUILayout.ObjectField("Limit Break:", limitBreak, typeof(PlayerAbility), false) as PlayerAbility;
            limitBreakGaugeSize = EditorGUILayout.FloatField("Limit Breeak Gauge Size:", limitBreakGaugeSize);
        }

        //window management block
        if(currentChoice != lastChoice)
        {
            ResetFlags();
            populateUI();
            lastChoice = currentChoice;
        }

        if (GUILayout.Button("Save"))
        {
            saveCurrentCharacter();
        }
    }

    private void GetCharacters()
    {
        characterList.Clear();
        characterNameList.Clear();
        string[] guids = AssetDatabase.FindAssets("t:PlayerData");
        foreach(string guid in guids)
        {
            string tempString = AssetDatabase.GUIDToAssetPath(guid);
            PlayerData characterInst = AssetDatabase.LoadAssetAtPath(tempString, typeof(PlayerData)) as PlayerData;

            characterNameList.Add(characterInst.characterName);
            characterList.Add(characterInst);
        }
        characterNameArray = characterNameList.ToArray();
    }

    private void populateUI()
    {
        ResetFlags();

        windowName = characterList[currentChoice].characterName;
        sprite = characterList[currentChoice].mySprite;
        animator = characterList[currentChoice].animatorController;
        health = characterList[currentChoice].maxHealth;
        defence = characterList[currentChoice].defense;
        doubleJump = characterList[currentChoice].canMultiJump;
        wallJump = characterList[currentChoice].canWallJump;
        airDash = characterList[currentChoice].canAirDash;
        engagementRadius = characterList[currentChoice].engagementRadius;
        abilityMain = characterList[currentChoice].attackMain;
        abilitySecondary = characterList[currentChoice].attackSecondary;
        abilityOne = characterList[currentChoice].abilityOne;
        abilityTwo = characterList[currentChoice].abilityTwo;
        abilityThree = characterList[currentChoice].abilityThree;
        limitBreak = characterList[currentChoice].limitBreak;
        limitBreakGaugeSize = characterList[currentChoice].limitGaugeSize;

        if(doubleJump)
        {
            jumpStyleChoice = 0;
        }
        else if(wallJump)
        {
            jumpStyleChoice = 1;
        }
        else
        {
            jumpStyleChoice = 2;
        }
    }

    private void saveCurrentCharacter()
    {
        //reset and check flags
        ResetFlags();
        if(windowName == "")
        {
            //nameFlag = true;
            return;
        }
        if(sprite == null)
        {
            //spriteFlag = true;
            return;
        }

        //Save the data to local
        characterList[currentChoice].characterName = windowName;
        characterList[currentChoice].mySprite = sprite;
        characterList[currentChoice].animatorController = animator;
        characterList[currentChoice].maxHealth = health;
        characterList[currentChoice].defense = defence;
        characterList[currentChoice].canMultiJump = doubleJump;
        characterList[currentChoice].canAirDash = airDash;
        characterList[currentChoice].canWallJump = wallJump;
        characterList[currentChoice].engagementRadius = engagementRadius;
        characterList[currentChoice].attackMain = abilityMain;
        characterList[currentChoice].attackSecondary = abilitySecondary;
        characterList[currentChoice].abilityOne = abilityOne;
        characterList[currentChoice].abilityTwo = abilityTwo;
        characterList[currentChoice].abilityThree = abilityThree;
        characterList[currentChoice].limitBreak = limitBreak;
        characterList[currentChoice].limitGaugeSize = limitBreakGaugeSize;

        //force update to save meta
        EditorUtility.SetDirty(characterList[currentChoice]);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private void ResetFlags()
    {
        //spriteFlag = false;
        //nameFlag = false;
    }
}
