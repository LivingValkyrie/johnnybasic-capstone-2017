﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Sprites;

/// <summary>
/// @author Mike Dobson
/// </summary>
public class EnemyEditorWindow : EditorWindow {

    #region ToolVaraibles
    public List<EnemyData> enemyList = new List<EnemyData>();
    public List<string> enemyNameList = new List<string>();
    public string[] enemyNameArray;

    //editor variables
    int currentChoice = 0;
    int lastChoice = 0;

    //character variables
    string enemyName = " ";
    Sprite enemySprite = null;
    AnimatorOverrideController enemyAnimator = null;
    EnemyTypes enemyType = EnemyTypes.podCrawler;
    Color characterHue = new Color();
    [Tooltip("this is a test")]
    int enemyHealth = 1;
    float attackDistance = 1;
    float attackCooldown = 1;
    float moveSpeed = 1;
    float chaseSpeed = 1;
    bool stationaryPatrol = false;

    //Foldouts
    bool showAttackValues = false;
    bool showMovementValues = false;

    //error flags
    bool existsFlag = false;
    bool nameFlag = false;
    bool spriteFlag = false;

    #endregion

    [MenuItem("Custom Tools/Enemy Tool %e")]
	private static void initialization()
    {
        EditorWindow.GetWindow<EnemyEditorWindow>();
    }

    void Awake()
    {
        getEnemies();
    }

    void OnGUI()
    {
        GUIContent content = new GUIContent();
        currentChoice = EditorGUILayout.Popup(currentChoice, enemyNameArray);

        EditorGUILayout.Space();
        //Sprite render
        if(enemySprite != null)
        {
            GUIStyle spriteStyle = new GUIStyle();
            spriteStyle.fixedHeight = 90;
            spriteStyle.fixedWidth = 90;

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            Texture2D tempTexture = AssetPreview.GetAssetPreview(enemySprite);
            //Texture2D tempTexture = SpriteUtility.GetSpriteTexture(enemySprite, false);
            GUILayout.Label(tempTexture, spriteStyle);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }
        enemySprite = EditorGUILayout.ObjectField(enemySprite, typeof(Sprite), false) as Sprite;
        EditorGUILayout.Space();
        content.tooltip = "This is a test";
        content.text = "Name: ";
        enemyName = EditorGUILayout.TextField(content, enemyName);
        enemyAnimator = EditorGUILayout.ObjectField("Animator: ",enemyAnimator, typeof(AnimatorOverrideController), false) as AnimatorOverrideController;
        enemyType = (EnemyTypes)EditorGUILayout.EnumPopup("Enemy Type: ", enemyType);
        content.text = "Hue";
        content.tooltip = "This is the hue that will overlay the enemy";
        characterHue = EditorGUILayout.ColorField(content, characterHue);
        enemyHealth = EditorGUILayout.IntSlider("Health: ", Mathf.Max(1,enemyHealth), 1, 250);

        //Attack values block
        showAttackValues = EditorGUILayout.Foldout(showAttackValues, "Attack Info");
        if(showAttackValues)
        {
            attackDistance = EditorGUILayout.Slider("Attack Distance", Mathf.Max(0,attackDistance), 0, 20);
            attackCooldown = EditorGUILayout.FloatField("Attack Cooldown", Mathf.Max(0,attackCooldown));
        }

        //movement values block
        showMovementValues = EditorGUILayout.Foldout(showMovementValues, "Movement Info");
        if(showMovementValues)
        {
            EditorGUILayout.BeginHorizontal();
            moveSpeed = EditorGUILayout.FloatField("Patrol Movement Speed", Mathf.Max(0,moveSpeed));
            stationaryPatrol = EditorGUILayout.Toggle("Stationary Patrol", stationaryPatrol);
            EditorGUILayout.EndHorizontal();
            chaseSpeed = EditorGUILayout.FloatField("Chase Movement Speed", Mathf.Max(0,chaseSpeed));
        }
        EditorGUILayout.Space();

        //Create/save button space
        if(currentChoice == 0)
        {
            if(GUILayout.Button("Create"))
            {
                CreateNew();
            }
        }
        else
        {
            if(GUILayout.Button("Save"))
            {
                saveCurrentEnemy();
            }
        }

        //Flag management space
        if(existsFlag)
        {
            EditorGUILayout.HelpBox("That enemy already exists \n Please edit the existing enemy or change the current name", MessageType.Warning);
        }
        if(nameFlag)
        {
            EditorGUILayout.HelpBox("The enemy must have a name before saving",MessageType.Warning);
        }
        if(spriteFlag)
        {
            EditorGUILayout.HelpBox("The enemy must havce a sprite before saving", MessageType.Warning);
        }

        //window management space
        if(currentChoice != lastChoice)
        {
            ResetFlags();
            if(currentChoice == 0)
            {
                newEenemy();
            }
            else
            {
                populateUI();
            }
            lastChoice = currentChoice;
        }

    }

    private void getEnemies()
    {
        enemyList.Clear();
        enemyNameList.Clear();
        string[] guids = AssetDatabase.FindAssets("t:EnemyData");
        foreach(string guid in guids)
        {
            string tempString = AssetDatabase.GUIDToAssetPath(guid);
            EnemyData enemyInst = AssetDatabase.LoadAssetAtPath(tempString, typeof(EnemyData)) as EnemyData;

            enemyNameList.Add(enemyInst.characterName);
            enemyList.Add(enemyInst);
        }
        enemyNameList.Insert(0, "New");
        enemyNameArray = enemyNameList.ToArray();
    }

    private void populateUI()
    {
        ResetFlags();

        enemyName = enemyList[currentChoice - 1].characterName;
        enemySprite = enemyList[currentChoice - 1].mySprite;
        enemyAnimator = enemyList[currentChoice - 1].animatorController;
        enemyType = enemyList[currentChoice - 1].myType;
        characterHue = enemyList[currentChoice - 1].hue;
        enemyHealth = enemyList[currentChoice - 1].maxHealth;
        attackDistance = enemyList[currentChoice - 1].attackDistance;
        attackCooldown = enemyList[currentChoice - 1].attackCooldown;
        moveSpeed = enemyList[currentChoice - 1].moveSpeed;
        chaseSpeed = enemyList[currentChoice - 1].chaseSpeed;
        stationaryPatrol = enemyList[currentChoice - 1].stationaryPatrol;
    }

    private void newEenemy()
    {
        ResetFlags();

        enemyName = "";
        enemySprite = null;
        enemyAnimator = null;
        enemyType = EnemyTypes.podCrawler;
        characterHue = new Color();
        enemyHealth = 1;
        attackDistance = 1;
        attackCooldown = 1;
        moveSpeed = 1;
        chaseSpeed = 1;
        stationaryPatrol = false;
    }

    private void CreateNew()
    {
        ResetFlags();

        if(enemyName == "")
        {
            nameFlag = true;
            return;
        }
        if(enemySprite == null)
        {
            spriteFlag = true;
            return;
        }
        string[] assetString = AssetDatabase.FindAssets(enemyName);
        if(assetString.Length > 0)
        {
            existsFlag = true;
            return;
        }

        //Passed checks create new
        EnemyData meinEnemy = ScriptableObject.CreateInstance<EnemyData>();
        meinEnemy.characterName = enemyName;
        meinEnemy.mySprite = enemySprite;
        meinEnemy.animatorController = enemyAnimator;
        meinEnemy.myType = enemyType;
        meinEnemy.hue = characterHue;
        meinEnemy.maxHealth = enemyHealth;
        meinEnemy.attackDistance = attackDistance;
        meinEnemy.attackCooldown = attackCooldown;
        meinEnemy.moveSpeed = moveSpeed;
        meinEnemy.chaseSpeed = chaseSpeed;
        meinEnemy.stationaryPatrol = stationaryPatrol;

        //create asset
        AssetDatabase.CreateAsset(meinEnemy, "Assets/Prefabs/Character Data/Enemies/" + meinEnemy.characterName + ".asset");
        getEnemies();
        //goto index of new enemy
        for (int i = 0; i < enemyList.Count; i++)
        {
            if(enemyList[i].characterName == enemyName)
            {
                currentChoice = i + 1;
            }
        }
        AssetDatabase.Refresh();
    }

    private void saveCurrentEnemy()
    {
        //check flags
        ResetFlags();
        if(enemyName == "")
        {
            nameFlag = true;
            return;
        }
        if(enemySprite == null)
        {
            spriteFlag = true;
            return;
        }

        //Save the data to the local
        enemyList[currentChoice - 1].characterName = enemyName;
        enemyList[currentChoice - 1].mySprite = enemySprite;
        enemyList[currentChoice - 1].animatorController = enemyAnimator;
        enemyList[currentChoice - 1].myType = enemyType;
        enemyList[currentChoice - 1].hue = characterHue;
        enemyList[currentChoice - 1].maxHealth = enemyHealth;
        enemyList[currentChoice - 1].attackDistance = attackDistance;
        enemyList[currentChoice - 1].attackCooldown = attackCooldown;
        enemyList[currentChoice - 1].moveSpeed = moveSpeed;
        enemyList[currentChoice - 1].chaseSpeed = chaseSpeed;
        enemyList[currentChoice - 1].stationaryPatrol = stationaryPatrol;

        //force update to save meta
        EditorUtility.SetDirty(enemyList[currentChoice - 1]);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private void ResetFlags()
    {
        nameFlag = false;
        spriteFlag = false;
        existsFlag = false;
    }
}
