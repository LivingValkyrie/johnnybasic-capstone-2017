﻿using UnityEngine;
using System.Collections;
using UnityEditor;

/// <summary>
/// Author: Mike Dobson
/// Created: 11-14-16
/// 
/// Description: BehaviorInspector is the custom inspector that is used to display only the information that
/// is needed in order to make a behavior node functional.
/// </summary>
//[CustomEditor(typeof(BehaviorNode))]
//public class BehaviorInspector : Editor {

//    SerializedProperty typeProp;
//    SerializedProperty jumpEndProp;
//    SerializedProperty curveProp;
//    SerializedProperty jumpTimeProp;
//    SerializedProperty reqProp;
//    SerializedProperty waitProp;
//    SerializedProperty entryProp;

//    BehaviorType nodeType;

//    void OnEnable()
//    {
//        //Setup the SerializedProperties 
//        typeProp = serializedObject.FindProperty("thisType");
//        jumpEndProp = serializedObject.FindProperty("LandingPads");
//        curveProp = serializedObject.FindProperty("CurvePoints");
//        jumpTimeProp = serializedObject.FindProperty("JumpTimes");
//        reqProp = serializedObject.FindProperty("nodeRequirement");
//        waitProp = serializedObject.FindProperty("waitTime");
//        entryProp = serializedObject.FindProperty("leftEntry");
//    }

//    public override void OnInspectorGUI()
//    {
//        //================================= Required for inspector
//        serializedObject.Update();
//        //=================================

//        EditorGUILayout.PropertyField(typeProp);

//        BehaviorNode myTarget = (BehaviorNode)target;
//        nodeType = myTarget.thisType;

//        switch(nodeType)
//        {
//            case BehaviorType.StopContinue:
//            case BehaviorType.StopTurn:
//                EditorGUILayout.PropertyField(reqProp);
//                EditorGUILayout.PropertyField(waitProp);
//                break;
//            case BehaviorType.JumpZone:
//                EditorGUILayout.PropertyField(jumpEndProp);
//                EditorGUILayout.PropertyField(curveProp);
//                EditorGUILayout.PropertyField(jumpTimeProp);
//                EditorGUILayout.PropertyField(entryProp);
//                break;
//            case BehaviorType.LandingPad:
//                break;
//        }
       
//        //=========================================== Required for inspector
//        serializedObject.ApplyModifiedProperties();
//    }

//}
