using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_PlayerMod : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		PlayerMod data = (PlayerMod)obj;
		// Add your writer.Write calls here.
		writer.Write(data.characterName);
		writer.Write(data.maxHealthIncrease);
		writer.Write(data.defenseIncrease);
		writer.Write(data.armorIncrease);

	}
	
	public override object Read(ES2Reader reader)
	{
		PlayerMod data = new PlayerMod();
		data.characterName = reader.Read<System.String>();
		data.maxHealthIncrease = reader.Read<System.Int32>();
		data.defenseIncrease = reader.Read<System.Single>();
		data.armorIncrease = reader.Read<System.Single>();

		return data;
	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_PlayerMod():base(typeof(PlayerMod)){}
}