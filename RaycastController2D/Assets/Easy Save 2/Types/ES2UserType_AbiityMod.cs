using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_AbiityMod : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		AbilityMod data = (AbilityMod)obj;
		// Add your writer.Write calls here.
		writer.Write(data.atkDmg);
		writer.Write(data.atkSpd);
		writer.Write(data.atkRng);
		writer.Write(data.cooldown);
		writer.Write(data.limitGaugeModifier);

	}
	
	public override object Read(ES2Reader reader)
	{
		AbilityMod data = new AbilityMod();
		data.atkDmg = reader.Read<System.Int32>();
		data.atkSpd = reader.Read<System.Single>();
		data.atkRng = reader.Read<System.Single>();
		data.cooldown = reader.Read<System.Single>();
		data.limitGaugeModifier = reader.Read<System.Single>();

		return data;
	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_AbiityMod():base(typeof(AbilityMod)){}
}