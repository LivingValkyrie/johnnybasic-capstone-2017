using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_WaypointData : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		WaypointData data = (WaypointData)obj;
		// Add your writer.Write calls here.
		writer.Write(data.firstEnter);
		writer.Write(data.indestructible);
		writer.Write(data.waypointId);
		writer.Write(data.levelToLoad);
		writer.Write(data.isBroken);

	}
	
	public override object Read(ES2Reader reader)
	{
		WaypointData data = new WaypointData();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		WaypointData data = (WaypointData)c;
		// Add your reader.Read calls here to read the data into the object.
		data.firstEnter = reader.Read<System.Boolean>();
		data.indestructible = reader.Read<System.Boolean>();
		data.waypointId = reader.Read<System.String>();
		data.levelToLoad = reader.Read<System.String>();
		data.isBroken = reader.Read<System.Boolean>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_WaypointData():base(typeof(WaypointData)){}
}