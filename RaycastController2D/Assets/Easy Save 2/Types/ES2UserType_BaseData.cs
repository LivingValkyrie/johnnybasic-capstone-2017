using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_BaseData : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		BaseData data = (BaseData)obj;
		// Add your writer.Write calls here.
		writer.Write(data.johnnysRoom);
		writer.Write(data.abigalesRoom);
		writer.Write(data.mannysRoom);
		writer.Write(data.valoriesRoom);

	}
	
	public override object Read(ES2Reader reader)
	{
		BaseData data = new BaseData();
		data.johnnysRoom = reader.Read<System.Int32>();
		data.abigalesRoom = reader.Read<System.Int32>();
		data.mannysRoom = reader.Read<System.Int32>();
		data.valoriesRoom = reader.Read<System.Int32>();

		return data;
	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_BaseData():base(typeof(BaseData)){}
}