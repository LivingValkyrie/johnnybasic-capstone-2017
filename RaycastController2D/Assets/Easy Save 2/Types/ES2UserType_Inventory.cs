using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_Inventory : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		Inventory data = (Inventory)obj;
		// Add your writer.Write calls here.
		writer.Write(data.items);
		writer.Write(data.maxCapacity);
		writer.Write(data.baseItems);

	}
	
	public override object Read(ES2Reader reader)
	{
		Inventory data = GetOrCreate<Inventory>();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		Inventory data = (Inventory)c;
		// Add your reader.Read calls here to read the data into the object.
		data.items = reader.ReadDictionary<InventoryItem,System.Int32>();
		data.maxCapacity = reader.Read<System.Int32>();
		data.baseItems = reader.ReadDictionary<InventoryItem,System.Int32>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_Inventory():base(typeof(Inventory)){}
}