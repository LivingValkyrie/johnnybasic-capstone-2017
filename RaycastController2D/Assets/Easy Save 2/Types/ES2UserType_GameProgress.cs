using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_GameProgress : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		GameProgress data = (GameProgress)obj;
		// Add your writer.Write calls here.
		writer.Write(data.levelName);
		writer.Write(data.spawnPointId);
		writer.Write(data.playerOneName);
		writer.Write(data.playerTwoName);
		writer.Write(data.playerOneHealth);
		writer.Write(data.playerTwoHealth);
		writer.Write(data.playerOneLimit);
		writer.Write(data.playerTwoLimit);
		writer.Write(data.baseData);
		writer.Write(data.playerOneLastCast);
		writer.Write(data.playerOneSecondToLastCast);
		writer.Write(data.playerTwoLastCast);
		writer.Write(data.playerTwoSecondToLastCast);
		writer.Write(data.isCarrying);
		writer.Write(data.hasCompanion);
		writer.Write(data.currPotions);
		writer.Write(data.maxPotions);
		writer.Write(data.johnnyConsumable);
		writer.Write(data.abbyConsumable);
		writer.Write(data.mannyConsumable);
		writer.Write(data.valConsumable);
		writer.Write(data.johnnyAbilities);
		writer.Write(data.abbyAbilities);
		writer.Write(data.mannyAbilities);
		writer.Write(data.valAbilities);
		writer.Write(data.specialConditions);
		writer.Write(data.playerMods);

	}
	
	public override object Read(ES2Reader reader)
	{
		GameProgress data = new GameProgress();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		GameProgress data = (GameProgress)c;
		// Add your reader.Read calls here to read the data into the object.
		data.levelName = reader.Read<System.String>();
		data.spawnPointId = reader.Read<System.Int32>();
		data.playerOneName = reader.Read<System.String>();
		data.playerTwoName = reader.Read<System.String>();
		data.playerOneHealth = reader.Read<System.Single>();
		data.playerTwoHealth = reader.Read<System.Single>();
		data.playerOneLimit = reader.Read<System.Single>();
		data.playerTwoLimit = reader.Read<System.Single>();
		data.baseData = reader.Read<BaseData>();
		data.playerOneLastCast = reader.Read<AbilitySlot>();
		data.playerOneSecondToLastCast = reader.Read<AbilitySlot>();
		data.playerTwoLastCast = reader.Read<AbilitySlot>();
		data.playerTwoSecondToLastCast = reader.Read<AbilitySlot>();
		data.isCarrying = reader.Read<System.Boolean>();
		data.hasCompanion = reader.Read<System.Boolean>();
		data.currPotions = reader.Read<System.Int32>();
		data.maxPotions = reader.Read<System.Int32>();
		data.johnnyConsumable = reader.Read<System.Int32>();
		data.abbyConsumable = reader.Read<System.Int32>();
		data.mannyConsumable = reader.Read<System.Int32>();
		data.valConsumable = reader.Read<System.Int32>();
		data.johnnyAbilities = reader.Read<System.Int32>();
		data.abbyAbilities = reader.Read<System.Int32>();
		data.mannyAbilities = reader.Read<System.Int32>();
		data.valAbilities = reader.Read<System.Int32>();
		data.specialConditions = reader.Read<SpecialConditions>();
		data.playerMods = reader.ReadArray<PlayerMod>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_GameProgress():base(typeof(GameProgress)){}
}