using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_InventoryItem : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		InventoryItem data = (InventoryItem)obj;
		// Add your writer.Write calls here.
		writer.Write(data.itemName);
		writer.Write(data.icon);
		writer.Write(data.maxCapacity);
		writer.Write(data.rarity);
		writer.Write(data.description);

	}
	
	public override object Read(ES2Reader reader)
	{
		InventoryItem data = ScriptableObject.CreateInstance<InventoryItem>();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		InventoryItem data = (InventoryItem)c;
		// Add your reader.Read calls here to read the data into the object.
		data.itemName = reader.Read<System.String>();
		data.icon = reader.Read<UnityEngine.Sprite>();
		data.maxCapacity = reader.Read<System.Int32>();
		data.rarity = reader.Read<InventoryItem.Rarity>();
		data.description = reader.Read<System.String>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_InventoryItem():base(typeof(InventoryItem)){}
}