using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_SpecialConditions : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		SpecialConditions data = (SpecialConditions)obj;
		// Add your writer.Write calls here.
		writer.Write(data.secretOne);
		writer.Write(data.secretTwo);
		writer.Write(data.secretThree);
		writer.Write(data.dungeonOne);
		writer.Write(data.dungeonTwo);
		writer.Write(data.dungeonThree);

	}
	
	public override object Read(ES2Reader reader)
	{
		SpecialConditions data = new SpecialConditions();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		SpecialConditions data = (SpecialConditions)c;
		// Add your reader.Read calls here to read the data into the object.
		data.secretOne = reader.Read<System.Boolean>();
		data.secretTwo = reader.Read<System.Boolean>();
		data.secretThree = reader.Read<System.Boolean>();
		data.dungeonOne = reader.Read<System.Boolean>();
		data.dungeonTwo = reader.Read<System.Boolean>();
		data.dungeonThree = reader.Read<System.Boolean>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_SpecialConditions():base(typeof(SpecialConditions)){}
}