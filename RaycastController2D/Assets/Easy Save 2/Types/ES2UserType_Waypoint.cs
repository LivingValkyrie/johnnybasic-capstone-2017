using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_Waypoint : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		Waypoint data = (Waypoint)obj;
		// Add your writer.Write calls here.
		writer.Write(data.data);

	}
	
	public override object Read(ES2Reader reader)
	{
		Waypoint data = GetOrCreate<Waypoint>();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		Waypoint data = (Waypoint)c;
		// Add your reader.Read calls here to read the data into the object.
		data.data = reader.Read<WaypointData>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_Waypoint():base(typeof(Waypoint)){}
}