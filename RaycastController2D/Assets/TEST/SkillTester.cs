﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: SkillTester
/// </summary>
public class SkillTester : MonoBehaviour {
	#region Fields

	public Ability ability;

	#endregion
	
	void Start() {
		ability.Init();
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			ability.OnCast(Vector2.right, Vector3.zero, this.gameObject);
		}
	}
}