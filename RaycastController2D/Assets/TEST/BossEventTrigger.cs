﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: BossEventTrigger
/// </summary>
public class BossEventTrigger : MonoBehaviour {
	#region Fields

	/*
		destroy player and companion
		destroy camera follow
		enabble camera shaker

		invoke repeating camera shake, play sound effect, increase counter
		once counter is over 3
		roar sound
		fade to thank you
		 
	*/

	public AudioClip roar, step;

	#endregion
	
	void Start() {
		
	}

	void Update() {
		
	}

	void OnTriggerEnter2D(Collider2D other) {
		print("Called " + other.name);
		if (other.gameObject == GameController.instance.ActivePlayerGameObject) {
			GameController.instance.ActivePlayer.StopInput(true);
			//GameController.instance.GetComponent<AudioSource>().clip = GameController.instance.bossIntro;
			GameController.instance.GetComponent<AudioSource>().Stop();
			//GameController.instance.GetComponent<AudioSource>().volume = .2f;
			GameController.instance.PlaySoundEffect(roar);

			Camera.main.GetComponent<CameraShake>().ShakeMultiple();
		}
		//GameController.instance.PauseGame(false);
	}
}