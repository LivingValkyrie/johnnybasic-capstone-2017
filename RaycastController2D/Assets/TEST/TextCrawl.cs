﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: TextCrawl
/// </summary>
public class TextCrawl : MonoBehaviour {
	#region Fields

	public Text[] text;
	int currText = 0;
	public Animation fadeIn, fadeOut;

	#endregion

	void Start() {
		text[currText].gameObject.SetActive(true);
		//text[currText].GetComponent<Animation>().Play("fadeIn");
	}

	public void FadeToNext() {
		//StartCoroutine("FadeTextOut");
		text[currText].gameObject.SetActive( false );


		currText++;
		if (currText >= text.Length) {
			GameController.instance.PauseGame(false);
			GameController.instance.LoadLevel("Zone_0_1");
		} else {
			//StartCoroutine("FadeTextIn");
			text[currText].gameObject.SetActive( true );

		}
	}

	IEnumerator FadeTextIn() {
		text[currText].gameObject.SetActive(true);
		text[currText].GetComponent<Animation>().Play("fadeIn");
		yield return new WaitForSeconds(1);
	}

	IEnumerator FadeTextOut() {
		text[currText].GetComponent<Animation>().Play("fadeOut");
		yield return new WaitForSeconds(1);
		text[currText].gameObject.SetActive(false);
		yield return null;

		//currText++;
		//if (currText >= text.Length) {
		//	GameController.instance.PauseGame(false);
		//	GameController.instance.LoadLevel("Zone_0_1");
		//	yield return null;
		//} else {
		//	yield return new WaitForSeconds(1);
		//}
	}
}