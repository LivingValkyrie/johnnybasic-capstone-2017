﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: TouchDamge
/// </summary>
public class TouchDamage : MonoBehaviour {
	#region Fields

	public float power = 5;
	public CharacterStats.DamageType damageType = CharacterStats.DamageType.None;
    //Collider2D myChild;

    #endregion

    void Start() {
        //myChild = GetComponentInChildren<CircleCollider2D>();
    }

	void OnTriggerEnter2D(Collider2D other) {
        //todo fix this
   		if (other.gameObject == GameController.instance.ActivePlayerGameObject ||
		    other.gameObject == GameController.instance.InactivePlayerGameObject) {
            
            //print("hit " + this.name);
			other.gameObject.GetComponent<CharacterStats>().TakeDamage(power, damageType);
		}
	}
}