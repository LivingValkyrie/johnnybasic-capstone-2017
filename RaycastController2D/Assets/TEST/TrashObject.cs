﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: TrashObject
/// </summary>
public class TrashObject : NonPlayerControllerInput {
	#region Fields

	float minimum = 0.0f;
	float maximum = 1f;
	float duration = 1.5f;
	float startTime;
	SpriteRenderer sprite;

	#endregion

	void Start() {
		velocity = new Vector3(Random.Range(0, 10), Random.Range(0, 10), 0);
		sprite = GetComponent<SpriteRenderer>();
		startTime = Time.time;
		Destroy(gameObject, duration);
	}

	protected override void Update() {
		base.Update();

		float t = (Time.time - startTime) / duration;
		sprite.color = new Color(1f, 1f, 1f, Mathf.SmoothStep(maximum, minimum, t));
	}

	protected virtual void FixedUpdate() {
		Controller.Move(velocity * Time.deltaTime);
	}
}