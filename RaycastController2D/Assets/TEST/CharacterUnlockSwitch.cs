﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: CharacterUnlockSwitch
/// </summary>
[RequireComponent(typeof(DestroyAfterAnim))]
public class CharacterUnlockSwitch : SimpleSwitch {
	#region Fields

	public BaseData.Character character;

	#endregion

	void Start() {
		if (GameController.instance.progress.baseData.GetRoomState(character) > 0) {
			Destroy(gameObject);
		}
	}

	public override void OnActivate() {
		GameController.instance.progress.baseData.UnlockRoom(character);
		GetComponent<Animation>().Play();
		GetComponent<DestroyAfterAnim>().enabled = true;
	}

	public override void OnDeactivate() {}
}