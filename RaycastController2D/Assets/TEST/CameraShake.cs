﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {
	public static CameraShake Instance;

	private float _amplitude = 0.1f;

	private Vector3 initialPosition;
	private bool isShaking = false;
	BoundingBoxFollowCamera bbfc;

	// Use this for initialization
	void Start() {
		Instance = this;
		bbfc = GetComponent<BoundingBoxFollowCamera>();
	}

	public void Shake(float amplitude, float duration) {
		_amplitude = amplitude;
		isShaking = true;
		initialPosition = transform.localPosition;
		bbfc.enabled = false;
		CancelInvoke();
		Invoke("StopShaking", duration);
	}

	public void StopShaking() {
		isShaking = false;
		bbfc.enabled = true;
	}

	// Update is called once per frame
	void Update() {
		if (isShaking) {
			transform.localPosition = initialPosition + Random.insideUnitSphere * _amplitude;
		}
	}

	public void ShakeMultiple() {
		//print("called in cam");
		StartCoroutine("Shakes");
	}

	//public float firstDuration, secondDuration, thirdDuration;
	//public float firstMag, secondMag, thirdMag;
	//public float firstBreak, secondBreak, thirdBreak;

	public Vector3[] shakes;

	IEnumerator Shakes() {
		foreach (Vector3 v3 in shakes) {
			//print("Start shake");

			Shake(v3.x, v3.y);
			yield return new WaitForSeconds(v3.y);

			//print("shake break start");
			yield return new WaitForSeconds(v3.z);

			//print("shake over");
		}

		print("after foreach");
		GameController.instance.PauseGame(false);
		GameController.instance.LoadLevel("Menu_Thank_You");
	}
}