﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: ManualPlatformController
/// </summary>
public class ManualPlatformController : MonoBehaviour {
	#region Fields

	public PlatformController platform;
	public bool inputActive = true;

	#endregion

	void OnTriggerStay2D( Collider2D other ) {
		if (other.gameObject == GameController.instance.ActivePlayerGameObject) {
			if (inputActive) {
				//print( other.GetComponent<Player>().directionalInput.y * platform.speed );

				platform.velocity.y = other.GetComponent<Player>().directionalInput.y * platform.speed * Time.deltaTime;
				platform.canMove = true;
			} else {
				platform.canMove = false;
			}
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		if (other.gameObject == GameController.instance.ActivePlayerGameObject) {
			platform.velocity.y = 0;
			platform.canMove = false;
		}
	}
}