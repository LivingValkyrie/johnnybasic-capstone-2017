﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: DestroyAfterClip
/// </summary>
public class DestroyAfterClip : MonoBehaviour {
	#region Fields

	#endregion

	void Start() {
		Invoke("DestroyObject", GetComponent<AudioSource>().clip.length);
	}

	void DestroyObject() {
		Destroy(gameObject);
	}
}