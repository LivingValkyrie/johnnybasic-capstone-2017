﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: DestroyAfterAnim
/// </summary>
public class DestroyAfterAnim : MonoBehaviour {
	#region Fields

	public bool spawnObjOnDestroy = false;
	public GameObject objToSpawn;

	#endregion
	
	void Start() {
		Invoke("DestroyObject", GetComponent<Animation>().clip.length);
	}

	void DestroyObject() {
		if (spawnObjOnDestroy) {
			Instantiate(objToSpawn, transform.position, Quaternion.identity);
		}
		Destroy(gameObject);
	}
}