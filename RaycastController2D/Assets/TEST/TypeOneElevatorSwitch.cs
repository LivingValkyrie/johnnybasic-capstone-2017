﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: TypeOneElevatorSwitch
/// </summary>
public class TypeOneElevatorSwitch : PlatformSwitch {
	#region Fields

	public GravityObject obj;

	#endregion

	public override void OnActivate() {
		obj.enabled = false;
		base.OnActivate();
	}

	public override void OnDeactivate() {
		obj.enabled = true;
		base.OnDeactivate();
	}
}