﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PauseOnEnable
/// </summary>
public class PauseOnEnable : MonoBehaviour {
	#region Fields

	public bool pauseOnEnable = true;
	public bool unpauseOnDisable = true;

	#endregion

	void OnEnable() {
		if (pauseOnEnable) {
			GameController.instance.SetPause(true);
		}
	}

	void OnDisable() {
		if (unpauseOnDisable) {
			GameController.instance.SetPause(false);
		}
	}
}