﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: PlatformSwitch
/// </summary>
public class PlatformSwitch : SimpleSwitch {
	#region Fields

	public PlatformController platform;

	public Sprite activeSprite, inactiveSprite;

	#endregion

	public override void OnActivate() {
		platform.canMove = true;
		if (activeSprite && GetComponent<SpriteRenderer>()) {
			GetComponent<SpriteRenderer>().sprite = activeSprite;
		}
	}

	public override void OnDeactivate() {
		platform.canMove = false;
		if ( inactiveSprite && GetComponent<SpriteRenderer>() ) {
			GetComponent<SpriteRenderer>().sprite = inactiveSprite;
		}
	}
}