﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: ManualPlatformSwitch
/// </summary>
public class ManualPlatformSwitch : SimpleSwitch {
	#region Fields

	public ManualPlatformController mpc;

	#endregion

	public override void OnActivate() {
		mpc.inputActive = true;
	}

	public override void OnDeactivate() {
		mpc.inputActive = false;
	}
}