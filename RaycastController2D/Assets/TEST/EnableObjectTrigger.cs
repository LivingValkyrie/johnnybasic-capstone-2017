﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: EnableObjectTrigger
/// </summary>
public class EnableObjectTrigger : MonoBehaviour {
	#region Fields

	public GameObject toEnable;

	#endregion

	void OnTriggerEnter2D(Collider2D other) {
		//print("triggered");
		if (other.gameObject == GameController.instance.ActivePlayerGameObject) {
			toEnable.SetActive(true);
			Destroy(gameObject);
		}
	}
}