﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: AnimTester
/// </summary>
public class AnimTester : MonoBehaviour {
	#region Fields

	Animator anim;
	[Header("Testing buttons, \"speed\" is set to a/d")]
	public KeyCode jumping = KeyCode.W;
	public KeyCode hasLanded = KeyCode.Q;
	public KeyCode attack = KeyCode.R;
	public KeyCode runToFall = KeyCode.E;

	#endregion

	void Start() {
		anim = GetComponent<Animator>();
	}

	void Update() {
		anim.SetFloat("speed", Input.GetAxis("Horizontal"));

        if (Input.GetKeyDown(jumping))
        {
            anim.SetTrigger("jumping");
        }
        if (Input.GetKeyDown(hasLanded)) {
			anim.SetTrigger("hasLanded");
		}
		if (Input.GetKeyDown(attack)) {
			anim.SetTrigger("attack");
		}
		if (Input.GetKeyDown(runToFall)) {
			anim.SetTrigger("runToFall");
		}
	}

}